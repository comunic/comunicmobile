package org.communiquons.comunic.independentnotifications;

import org.json.JSONException;
import org.json.JSONObject;

public class PushNotification {
    private final String id;
    private final int timeCreate;
    private final int timeout;
    private final String title;
    private final String body;
    private final String image;

    public PushNotification(JSONObject input) throws JSONException {
        id = input.getString("id");
        timeCreate = input.getInt("time_create");
        timeout = input.optInt("timeout");
        title = input.optString("title");
        body = input.optString("body");
        image = input.optString("image");
    }

    public String getId() {
        return id;
    }

    public int getTimeCreate() {
        return timeCreate;
    }

    public int getTimeout() {
        return timeout;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getImage() {
        return image;
    }
}

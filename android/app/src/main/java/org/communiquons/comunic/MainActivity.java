package org.communiquons.comunic;


import androidx.annotation.NonNull;

import org.communiquons.comunic.independentnotifications.NotificationsChannel;
import org.communiquons.comunic.independentnotifications.NotificationsService;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {

    private static final String INDEPENDENT_NOTIFICATIONS_CHANNEL = "org.communiquons.comunic/independent-push-notifications-service";

    @Override
    protected void onStart() {
        super.onStart();
        NotificationsService.startService(this);
    }

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);

        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), INDEPENDENT_NOTIFICATIONS_CHANNEL)
                .setMethodCallHandler(new NotificationsChannel(this));

    }


}

package org.communiquons.comunic.independentnotifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import org.communiquons.comunic.MainActivity;
import org.communiquons.comunic.R;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class NotificationsService extends Service implements Runnable {
    private static final String TAG = NotificationsService.class.getSimpleName();

    public static final String SVC_CHANNEL_ID = "IndependentPushServiceChannel";
    public static final String NOTIFS_CHANNEL_ID = "NotificationsPushServiceChannel";
    private static final String INDEPENDENT_PUSH_NOTIFICATIONS_SERVICE = "independent-push-notifications-service";
    private static final String WS_URL_PREF_KEY = "ws_url";

    private static final int NOTIFS_ID = 10;

    private static final int CONNECT_TIMEOUT = 1000;
    private static final int RECONNECT_INTERVAL = 60000;
    private static final int PING_INTERVAL = 15000;

    private Thread thread;
    private boolean stop = false;
    private final Object lock = new Object();

    public static void configure(@NonNull String wsURL, @NonNull Context context) {
        SharedPreferences prefs = context.getSharedPreferences(INDEPENDENT_PUSH_NOTIFICATIONS_SERVICE, MODE_PRIVATE);

        if (prefs.getString(WS_URL_PREF_KEY, "").equals(wsURL))
            return;

        stopService(context);
        prefs.edit().putString(WS_URL_PREF_KEY, wsURL).apply();
        startService(context);
    }

    public static void disable(@NonNull Context context) {
        SharedPreferences prefs = context.getSharedPreferences(INDEPENDENT_PUSH_NOTIFICATIONS_SERVICE, MODE_PRIVATE);
        prefs.edit().remove(WS_URL_PREF_KEY).apply();

        stopService(context);
    }

    public static void stopService(@NonNull Context context) {
        Intent serviceIntent = new Intent(context, NotificationsService.class);
        context.stopService(serviceIntent);
    }

    public static void startService(@NonNull Context context) {
        SharedPreferences prefs = context.getSharedPreferences(INDEPENDENT_PUSH_NOTIFICATIONS_SERVICE, MODE_PRIVATE);

        if (!prefs.contains(WS_URL_PREF_KEY)) {
            Log.v(TAG, "Independent push notifications service not configured ! Skipping!");
            return;
        }

        Log.v(TAG, "Start independent push notifications service...");

        Intent intent = new Intent(context, NotificationsService.class);
        ContextCompat.startForegroundService(context, intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (thread != null) {
            System.out.println("The service has already been started. Skipping initialization...");
            return START_STICKY;
        }


        createServiceNotificationChannel();

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0,
                notificationIntent,
                android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0);

        Notification notification = new NotificationCompat.Builder(this, SVC_CHANNEL_ID)
                .setContentTitle("Comunic")
                .setContentText(getText(R.string.independent_push_notification_notification_text))
                .setSmallIcon(R.drawable.ic_notifications)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);

        initService();

        return START_STICKY;
    }

    private void createServiceNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    SVC_CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    private void createPushNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    NOTIFS_CHANNEL_ID,
                    "Independent Push Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "Destroying service");
        super.onDestroy();

        if (thread != null) {
            thread.interrupt();
            stop = true;
        }
    }

    private void initService() {
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        try {
            while (!stop) {
                try {
                    connect();
                } catch (InterruptedException e) {
                    Log.e(TAG, "Thread interrupted!");
                    return;
                } catch (Exception e) {
                    Log.e(TAG, "Failed to connect to push notifications service!");
                    e.printStackTrace();
                }


                Log.v(TAG, "Wait a little before reconnecting...");

                // Wait attempting new connection
                // noinspection BusyWait
                Thread.sleep(RECONNECT_INTERVAL);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e(TAG, "Stop thread");
        }
    }

    private void connect() throws Exception {
        String url = getSharedPreferences(INDEPENDENT_PUSH_NOTIFICATIONS_SERVICE, MODE_PRIVATE)
                .getString(WS_URL_PREF_KEY, null);

        Log.v(TAG, "Connect to " + url);

        WebSocket ws = new WebSocketFactory().createSocket(url, CONNECT_TIMEOUT);
        ws.setPingInterval(PING_INTERVAL);
        ws.addListener(new WebSocketAdapter() {
            @Override
            public void onConnected(WebSocket websocket, Map<String, List<String>> headers) {
                Log.v(TAG, "Connected to independent push notifications service!");
            }

            @Override
            public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) {
                Log.v(TAG, "Disconnected from independent push notifications websocket!");
                synchronized (lock) {
                    lock.notify();
                }
            }

            @Override
            public void onTextFrame(WebSocket websocket, WebSocketFrame frame) {
                handleTextFrame(frame);
            }

            @Override
            public void onError(WebSocket websocket, WebSocketException cause) {
                Log.e(TAG, "An error occured, closing WebSocket!");
                cause.printStackTrace();
                websocket.disconnect();
            }
        });

        ws.connect();


        // wait for connection to complete
        synchronized (lock) {
            lock.wait();
        }
    }

    private void handleTextFrame(WebSocketFrame frame) {
        if (!frame.isTextFrame())
            return;

        Log.v(TAG, "Notification: " + frame.getPayloadText());
        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(frame.getPayloadText());


            if (jsonObject.has("drop_id"))
                dropNotification(jsonObject.getString("drop_id"));

            else if (jsonObject.has("id"))
                sendNotification(new PushNotification(jsonObject));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dropNotification(String id) {
        new Handler(Looper.getMainLooper()).post(() -> {
            NotificationManagerCompat notifManager = NotificationManagerCompat.from(NotificationsService.this);
            notifManager.cancel(id, NOTIFS_ID);
        });
    }

    private void sendNotification(PushNotification n) {
        new Handler(Looper.getMainLooper()).post(() -> postNotification(n));
    }

    private void postNotification(PushNotification n) {

        createPushNotificationChannel();

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0
        );

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFS_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notifications)
                .setContentTitle(n.getTitle())
                .setContentText(n.getBody())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(n.getId(), NOTIFS_ID, builder.build());

    }
}

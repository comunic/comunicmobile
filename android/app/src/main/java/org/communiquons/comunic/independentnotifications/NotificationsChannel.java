package org.communiquons.comunic.independentnotifications;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.provider.Settings;

import androidx.annotation.NonNull;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class NotificationsChannel implements MethodChannel.MethodCallHandler {
    
    private final Context context;

    public NotificationsChannel(Activity context) {
        this.context = context.getApplicationContext();
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
        try {
            switch (call.method) {
                case "needPreConfiguration":
                    result.success(needPreConfiguration());
                    break;

                case "preConfigure":
                    preConfigure(result);
                    break;

                case "configure":
                    configure((String) call.arguments, result);
                    break;

                case "disable":
                    disable(result);
                    break;

                default:
                    result.notImplemented();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.error("Failed to execute native code!", e.getMessage(), null);
        }

    }

    private boolean needPreConfiguration() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            return !context.getSystemService(PowerManager.class).isIgnoringBatteryOptimizations(context.getPackageName());
        }
        return false;
    }

    /**
     * Pre-configure independent notifications service
     */
    private void preConfigure(@NonNull MethodChannel.Result result) {
        if (needPreConfiguration() && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // See https://stackoverflow.com/a/41853011
            Intent intent = new Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);
        }

        result.success(null);
    }

    /**
     * Configure Independent Push Notifications service
     *
     * @param wsURL The URL this service will have to connect to
     */
    private void configure(@NonNull String wsURL, @NonNull MethodChannel.Result result) {
        NotificationsService.configure(wsURL, context);
        result.success(null);
    }

    /**
     * Disable push notifications service
     */
    private void disable(@NonNull MethodChannel.Result result) {
        NotificationsService.disable(context);
        result.success(null);
    }
}

package org.communiquons.comunic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.communiquons.comunic.independentnotifications.NotificationsService;

public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = BootReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || !intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.e(TAG, "Invalid call to intent!");
            return;
        }

        Log.v(TAG, "Boot received, starting independent push notifications service if required...");

        NotificationsService.startService(context);
    }
}

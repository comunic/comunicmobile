# comunic

A Comunic client written with flutter

## Getting Started on Android

## Flavors

### Application appearance & Title
* `stable` : For main releases
* `beta` : For intermediate releases


## Build types

### Beta
No special configuration is required. Use the `lib/main_dev.dart` file as entrypoint to the application.

### Releases
To make release builds, the application needs to be signed. Create a file named `key.properties` in the android directory with the following content: 
```
storeFile=path to keystore file
storePassword=store password
keyAlias=key alias
keyPassword=key password
forezKeyAlias=key alias
forezKeyPassword=key password
```

Use the `lib/main_online.dart` as entry point to the application.

### Building
Now choose a flavor and build the application using :
```sh
# Generic command
flutter build apk --flavor BUILD_FLAVOR -t ENTRY_POINT

# Example
flutter build apk --flavor beta -t lib/main_online.dart
```
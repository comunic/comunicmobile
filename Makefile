beta_online_release:
	flutter build apk --flavor beta -t lib/main_online.dart

beta_dev_release:
	flutter build apk --flavor beta -t lib/main_dev.dart


beta_online_release_split_per_abi:
	flutter build apk --flavor beta -t lib/main_online.dart --target-platform android-arm,android-arm64,android-x64 --split-per-abi


stable_release_split_per_abi:
	flutter build apk --flavor stable -t lib/main_online.dart --target-platform android-arm,android-arm64,android-x64 --split-per-abi

stable_release:
	flutter build apk --flavor stable -t lib/main_online.dart

forez_release:
	flutter build apk --flavor forez -t lib/forez/main_forez_online.dart

forez_release_split_per_abi:
	flutter build apk --flavor forez -t lib/forez/main_forez_online.dart --target-platform android-arm,android-arm64,android-x64 --split-per-abi

.PHONY: beta_offline_release beta_online_release_split_per_abi stable_release_split_per_abi stable_release beta_dev_release

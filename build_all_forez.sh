#!/bin/sh
DEST="/home/pierre/Desktop"

make forez_release_split_per_abi && \
	mv build/app/outputs/flutter-apk/app-armeabi-v7a-forez-release.apk "$DEST" && \
	mv build/app/outputs/flutter-apk/app-arm64-v8a-forez-release.apk "$DEST" && \
	mv build/app/outputs/flutter-apk/app-x86_64-forez-release.apk "$DEST" && \
	mv build/app/outputs/mapping/forezRelease/mapping.txt "$DEST"
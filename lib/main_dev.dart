import 'dart:io';

import 'package:comunic/main.dart';
import 'package:comunic/models/config.dart';
import 'package:comunic/utils/flutter_utils.dart';

/// Dev (internal) build configuration for the project
///
/// @author Pierre HUBERT

/// Fix HTTPS issue
class MyHttpOverride extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (cert, host, port) {
        return host == "devweb.local"; // Forcefully trust local website
      };
  }
}

void main() {
  Config.set(Config(
    apiServerName: "192.168.1.9:3000",
    apiServerUri: "/",
    apiServerSecure: false,
    clientName: isWeb ? "FlutterWeb" : "ComunicFlutter",
  ));

  HttpOverrides.global = new MyHttpOverride();

  subMain();
}

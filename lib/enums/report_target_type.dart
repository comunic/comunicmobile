/// What kind of content that can be reported
enum ReportTargetType {
  Post,
  Comment,
  Conversation,
  ConversationMessage,
  User,
  Group
}

extension ReportTargetExt on ReportTargetType {
  String get apiId {
    switch (this) {
      case ReportTargetType.Post:
        return "post";
      case ReportTargetType.Comment:
        return "comment";
      case ReportTargetType.Conversation:
        return "conversation";
      case ReportTargetType.ConversationMessage:
        return "conversation_message";
      case ReportTargetType.User:
        return "user";
      case ReportTargetType.Group:
        return "group";
    }
  }
}

/// Posts targets types enum
///
/// @author Pierre HUBERT

enum PostTarget {
  USER_PAGE,
  GROUP_PAGE
}
import 'package:flutter/material.dart';

/// Post visibility level
///
/// @author Pierre HUBERT

enum PostVisibilityLevel { PUBLIC, FRIENDS, USER, GROUP_MEMBERS }

/// Post visibility levels mapping with material icons
const PostVisibilityLevelsMapIcons = {
  PostVisibilityLevel.USER: Icons.lock,
  PostVisibilityLevel.FRIENDS: Icons.group,
  PostVisibilityLevel.GROUP_MEMBERS: Icons.group,
  PostVisibilityLevel.PUBLIC: Icons.public
};

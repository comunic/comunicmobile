/// Different kinds of posts
///
/// @author Pierre HUBERT

enum PostKind { TEXT, IMAGE, WEB_LINK, PDF, COUNTDOWN, SURVEY, YOUTUBE }

/// User page visibility
///
/// @author Pierre HUBERT

enum UserPageVisibility { PRIVATE, PUBLIC, OPEN }

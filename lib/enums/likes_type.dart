/// Likes types
///
/// @author Pierre HUBERT

enum LikesType { USER, POST, COMMENT, GROUP }

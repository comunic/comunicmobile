/// Load error level
///
/// @author Pierre HUBERT

enum LoadErrorLevel {MINOR, MAJOR, NONE}
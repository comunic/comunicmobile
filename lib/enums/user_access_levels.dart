/// User access levels
///
/// @author Pierre HUBERT

enum UserAccessLevels { NONE, BASIC, INTERMEDIATE, FULL}
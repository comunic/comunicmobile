import 'package:comunic/enums/post_target.dart';
import 'package:comunic/helpers/posts_helper.dart';
import 'package:comunic/models/advanced_user_info.dart';
import 'package:comunic/ui/widgets/post_create_form_widget.dart';
import 'package:comunic/ui/widgets/posts_list_widget.dart';
import 'package:flutter/material.dart';

/// User posts
///
/// @author Pierre Hubert

class UserPostsSection extends StatefulWidget {
  final AdvancedUserInfo user;

  const UserPostsSection({
    Key? key,
    required this.user,
  })  : super(key: key);

  @override
  _UserPostsSectionState createState() => _UserPostsSectionState();
}

class _UserPostsSectionState extends State<UserPostsSection> {
  int? get _userID => widget.user.id;

  final _postsKey = GlobalKey<PostsListWidgetState>();

  @override
  Widget build(BuildContext context) => PostsListWidget(
        key: _postsKey,
        topWidgets: [
          widget.user.canPostTexts
              ? PostCreateFormWidget(
                  postTarget: PostTarget.USER_PAGE,
                  targetID: _userID!,
                  onCreated: _postCreated,
                )
              : Container()
        ],
        getPostsList: () => PostsHelper().getUserPosts(_userID),
        getOlder: (from) => PostsHelper().getUserPosts(_userID, from: from),
        showPostsTarget: false,
      );

  void _postCreated() {
    _postsKey.currentState!.loadPostsList(getOlder: false);
  }
}

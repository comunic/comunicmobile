import 'package:comunic/enums/report_target_type.dart';
import 'package:comunic/enums/user_page_visibility.dart';
import 'package:comunic/helpers/friends_helper.dart';
import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/models/advanced_user_info.dart';
import 'package:comunic/models/displayed_content.dart';
import 'package:comunic/models/friend_status.dart';
import 'package:comunic/models/report_target.dart';
import 'package:comunic/ui/dialogs/report_dialog.dart';
import 'package:comunic/ui/widgets/FrienshipStatusWidget.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/text_widget.dart';
import 'package:comunic/utils/clipboard_utils.dart';
import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
/// About current user
///
/// @author Pierre Hubert
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

class AboutUserSection extends StatefulWidget {
  final AdvancedUserInfo user;

  const AboutUserSection({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  _AboutUserSectionState createState() => _AboutUserSectionState();
}

class _AboutUserSectionState extends State<AboutUserSection> {
  late FriendStatus _friendStatus;

  final _screenKey = GlobalKey<AsyncScreenWidgetState>();

  Future<void> _init() async {
    if (!widget.user.isCurrentUser)
      _friendStatus = await FriendsHelper().getFriendshipStatus(widget.user.id);
  }

  void _toggleRefresh() => _screenKey.currentState!.refresh();

  @override
  Widget build(BuildContext context) => AsyncScreenWidget(
      key: _screenKey,
      onReload: _init,
      onBuild: _buildList,
      errorMessage: tr("Failed to load user information!")!);

  Widget _buildList() => ListView(
        children: [
          widget.user.isCurrentUser
              ? Container()
              : FriendshipStatusWidget(
                  status: _friendStatus, onFriendshipUpdated: _toggleRefresh),

          // Account URL
          widget.user.hasPersonalWebsite
              ? ListTile(
                  leading: Icon(Icons.link),
                  title: Text(tr("Personal Website")!),
                  subtitle: Text(widget.user.personalWebsite),
                  onTap: () => launchUrlString(widget.user.personalWebsite),
                )
              : Container(),

          // Account note
          widget.user.hasPublicNote
              ? ListTile(
                  leading: Icon(Icons.note),
                  title: Text(tr("Note")!),
                  subtitle: TextWidget(
                      content: DisplayedString(widget.user.publicNote)),
                )
              : Container(),

          // Virtual directory
          widget.user.hasVirtualDirectory
              ? ListTile(
                  leading: Icon(Icons.alternate_email),
                  title: Text(tr("Virtual directory")!),
                  subtitle: Text("@${widget.user.virtualDirectory}"))
              : Container(),

          // User email address
          widget.user.emailAddress == null
              ? Container()
              : ListTile(
                  leading: Icon(Icons.email_outlined),
                  title: Text(tr("Email address")!),
                  subtitle: Text(widget.user.emailAddress!),
                  onTap: () =>
                      copyToClipboard(context, widget.user.emailAddress!),
                ),

          // User location
          widget.user.location == null
              ? Container()
              : ListTile(
                  leading: Icon(Icons.location_on),
                  title: Text(tr("Location")!),
                  subtitle: Text(widget.user.location!),
                  onTap: () => copyToClipboard(context, widget.user.location!),
                ),

          // Number of friends
          widget.user.isFriendsListPublic
              ? ListTile(
                  leading: Icon(Icons.group),
                  title: Text(tr("Number of friends")!),
                  subtitle: Text(widget.user.numberFriends.toString()),
                )
              : Container(),

          // Member for
          ListTile(
            leading: Icon(Icons.access_time_rounded),
            title: Text(tr("Member for")!),
            subtitle:
                Text(diffTimeFromNowToStr(widget.user.accountCreationTime)!),
          ),

          // Account visibility
          ListTile(
            leading: Icon(Icons.remove_red_eye),
            title: Text(tr("Account visibility")!),
            subtitle: Text(widget.user.pageVisibility == UserPageVisibility.OPEN
                ? tr("Open page")!
                : (widget.user.pageVisibility == UserPageVisibility.PUBLIC
                    ? tr("Public page")!
                    : tr("Private page")!)),
          ),

          // Report user
          srvConfig!.isReportingEnabled &&
                  (!widget.user.isCurrentUser ||
                      srvConfig!.reportPolicy!.canUserReportHisOwnContent)
              ? ListTile(
                  textColor: Colors.red,
                  leading: Icon(Icons.flag, color: Colors.red),
                  title: Text(tr("Report abuse")!),
                  onTap: _reportAbuse,
                )
              : Container(),
        ],
      );

  /// Report user
  void _reportAbuse() => showReportDialog(
      ctx: context,
      target: ReportTarget(ReportTargetType.User, widget.user.id));
}

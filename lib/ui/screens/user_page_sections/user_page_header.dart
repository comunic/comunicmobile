import 'package:comunic/models/advanced_user_info.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/utils/account_utils.dart';
import 'package:comunic/utils/conversations_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// User header
///
/// @author Pierre Hubert

class UserPageHeader extends StatelessWidget {
  final AdvancedUserInfo user;
  final Color bgColor;

  const UserPageHeader({
    Key? key,
    required this.user,
    required this.bgColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        color: bgColor,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              InkWell(
                onTap: () => showImageFullScreen(context, user.accountImageURL),
                child: AccountImageWidget(user: user),
              ),
              Expanded(flex: 1, child: Text(" ${user.displayName}")),

              // Private conversation
              user.id == userID()
                  ? Container()
                  : IconButton(
                      icon: Icon(
                        Icons.chat,
                        color: DefaultTextStyle.of(context).style.color,
                      ),
                      onPressed: () {
                        openPrivateConversation(context, user.id);
                      }),
            ],
          ),
        ),
      );
}

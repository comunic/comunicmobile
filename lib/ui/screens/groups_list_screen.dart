import 'package:comunic/enums/report_target_type.dart';
import 'package:comunic/helpers/groups_helper.dart';
import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/lists/groups_list.dart';
import 'package:comunic/models/group.dart';
import 'package:comunic/models/report_target.dart';
import 'package:comunic/ui/dialogs/report_dialog.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/widgets/group_icon_widget.dart';
import 'package:comunic/ui/widgets/group_membership_widget.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Groups list screen
///
/// @author Pierre Hubert

class GroupsListScreen extends StatefulWidget {
  @override
  _GroupsListScreenState createState() => _GroupsListScreenState();
}

class _GroupsListScreenState extends SafeState<GroupsListScreen> {
  GroupsList? _groups;
  bool _error = false;

  final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();

    this._refreshList();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        // Error
        buildErrorCard(
          tr("Could not load the list of groups!"),
          hide: !_error,
          actions: [
            MaterialButton(
              child: Text(tr("Try again")!.toUpperCase()),
              onPressed: () => _refreshList(),
            ),
          ],
        ),

        // List of groups
        Expanded(
            child: RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: () => this._refreshList(),
          child: _groups == null
              ? Container()
              : Stack(children: [_buildGroupsList(), _buildCreateButton()]),
        ))
      ],
    );
  }

  Widget _buildGroupsList() => ListView(
        children: (_groups!.values.toList()
              ..sort((one, two) => two.id.compareTo(one.id)))
            .map((g) => ListTile(
                  leading: GroupIcon(group: g),
                  title: Text(g.displayName),
                  subtitle: GroupMembershipWidget(
                    group: g,
                    onUpdated: () => _refreshIndicatorKey.currentState!.show(),
                  ),
                  trailing: IntrinsicWidth(
                    child: Row(
                      children: [
                        // Remove membership
                        IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: () => _deleteGroup(g)),

                        // Report button
                        srvConfig!.isReportingEnabled && !g.isAtLeastMember
                            ? IconButton(
                                onPressed: () => _reportGroup(g),
                                icon: Icon(
                                  Icons.flag,
                                ),
                              )
                            : Container()
                      ],
                    ),
                  ),
                  onTap: () => MainController.of(context)!.openGroup(g.id),
                ))
            .toList(),
      );

  Widget _buildCreateButton() => Positioned(
        right: 15,
        bottom: 15,
        child: FloatingActionButton(
          heroTag: null,
          child: Icon(Icons.add),
          onPressed: _createGroup,
        ),
      );

  /// Refresh the list of groups
  Future<void> _refreshList() async {
    try {
      final list = await GroupsHelper().getListUser();
      final groups = await GroupsHelper().getListOrThrow(list, force: true);

      setState(() {
        _groups = groups;
        _error = false;
      });
    } catch (e) {
      print(e);

      setState(() {
        _error = true;
      });
    }
  }

  /// Remove a group from the list
  void _deleteGroup(Group g) async {
    if (!await showConfirmDialog(
        context: context,
        message: tr("Do you really want to delete this group membership ?")))
      return;

    if (!await GroupsHelper().removeMembership(g.id))
      showSimpleSnack(
          context, tr("Could not remove your membership to this group!")!);

    // Refresh the list of groups
    _refreshIndicatorKey.currentState!.show();
  }

  /// Report a group
  void _reportGroup(Group g) => showReportDialog(
      ctx: context, target: ReportTarget(ReportTargetType.Group, g.id));

  /// Add a group
  void _createGroup() async {
    try {
      final name = await askUserString(
        context: context,
        title: tr("Group name")!,
        message: tr("Name of the group to create")!,
        defaultValue: "",
        hint: tr("Name of the group")!,
        maxLength: 50,
      );

      if (name == null) return;

      final groupID = await GroupsHelper.create(name);

      MainController.of(context)!.openGroup(groupID);
    } catch (e, s) {
      print("Could not create a new group! $e\n$s");
      showSimpleSnack(context, tr("Could not create a new group!")!);
    }
  }
}

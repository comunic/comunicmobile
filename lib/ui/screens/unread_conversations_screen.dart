import 'package:comunic/helpers/conversations_helper.dart';
import 'package:comunic/helpers/events_helper.dart';
import 'package:comunic/helpers/groups_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/groups_list.dart';
import 'package:comunic/lists/unread_conversations_list.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/unread_conversation.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/conversation_image_widget.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Unread conversations screen
///
/// @author Pierre Hubert

class UnreadConversationsScreen extends StatefulWidget {
  @override
  _UnreadConversationsScreenState createState() =>
      _UnreadConversationsScreenState();
}

class _UnreadConversationsScreenState
    extends SafeState<UnreadConversationsScreen> {
  late UnreadConversationsList _list;
  UsersList? _users;
  GroupsList? _groups;

  final _key = GlobalKey<AsyncScreenWidgetState>();

  Future<void> _refresh() async {
    _list = await ConversationsHelper.getListUnread();
    _users = await UsersHelper().getListWithThrow(_list.usersID);
    _groups = await GroupsHelper().getList(_list.groupsID);
  }

  @override
  void initState() {
    super.initState();

    listen<NewNumberUnreadConversations>((e) => _key.currentState!.refresh());
  }

  @override
  Widget build(BuildContext context) {
    return AsyncScreenWidget(
      key: _key,
      onReload: _refresh,
      onBuild: _buildList,
      errorMessage: tr("Could not load the list of unread conversations!")!,
    );
  }

  Widget _buildList() {
    // Check for no unread conversation
    if (_list.isEmpty)
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            tr("You do not have any unread conversation yet...")!,
            textAlign: TextAlign.center,
          ),
        ),
      );

    return ListView.builder(
      itemBuilder: _tileBuilder,
      itemCount: _list.length,
    );
  }

  Widget _tileBuilder(BuildContext context, int index) {
    final UnreadConversation conv = _list[index];

    final message = _list[index].message;

    final singleUserConv = conv.conv.members!.length < 3;

    String? messageStr;
    if (message.hasFile)
      messageStr = tr("New file");
    else if (message.hasMessage)
      messageStr = singleUserConv
          ? message.message.content
          : tr("%1% : %2%", args: {
              "1": _users!.getUser(message.userID).fullName,
              "2": message.message.content,
            });
    else
      message.serverMessage!.getText(_users);

    return ListTile(
      leading: ConversationImageWidget(
        conversation: conv.conv,
        users: _users!,
        group: conv.conv.isGroupConversation
            ? _groups!.getGroup(conv.conv.groupID)
            : null,
      ),
      title: Text(ConversationsHelper.getConversationName(conv.conv, _users)),
      subtitle: RichText(
        text: TextSpan(style: Theme.of(context).textTheme.bodyText2, children: [
          TextSpan(
            text: messageStr,
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ]),
      ),
      trailing: Text(diffTimeFromNowToStr(conv.message.timeSent!)!),
      onTap: () =>
          MainController.of(context)!.openConversationById(conv.conv.id!),
    );
  }
}

import 'package:comunic/helpers/groups_helper.dart';
import 'package:comunic/helpers/search_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/groups_list.dart';
import 'package:comunic/lists/search_results_list.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/group.dart';
import 'package:comunic/models/search_result.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/ui/widgets/group_icon_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Search screen
///
/// @author Pierre Hubert

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  SearchResultsList? _searchResultsList;
  late UsersList _usersList;
  late GroupsList _groupsList;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
                icon: Icon(Icons.search),
                labelText: tr("Search a user, a group...")),
            onChanged: (s) => _newSearch(s),
          ),
          _searchResultsList == null
              ? Container()
              : Expanded(
                  child: ListView(
                    children: _searchResultsList!
                        .map((f) => f.kind == SearchResultKind.USER
                            ? _SearchResultUser(
                                user: _usersList.getUser(f.id),
                              )
                            : _SearchResultGroup(
                                group: _groupsList[f.id]!,
                              ))
                        .toList(),
                  ),
                )
        ],
      ),
    );
  }

  /// Perform a new search
  void _newSearch(String s) async {
    try {
      if (s.length < 2) return;

      final list = await SearchHelper().globalSearch(s);
      final users = await UsersHelper().getListWithThrow(list.usersId);
      final groups = await GroupsHelper().getListOrThrow(list.groupsId);

      setState(() {
        _searchResultsList = list;
        _usersList = users;
        _groupsList = groups;
      });
    } catch (e, stack) {
      print(e);
      print(stack);

      showSimpleSnack(context, tr("Could not peform search!")!);
    }
  }
}

class _SearchResultUser extends StatelessWidget {
  final User user;

  const _SearchResultUser({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: AccountImageWidget(
        user: user,
      ),
      title: Text(user.displayName),
      onTap: () => MainController.of(context)!.openUserPage(user.id),
    );
  }
}

class _SearchResultGroup extends StatelessWidget {
  final Group group;

  const _SearchResultGroup({Key? key, required this.group}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: GroupIcon(group: group),
      title: Text(group.displayName),
      subtitle: Text(tr("Group")!),
      onTap: () => MainController.of(context)!.openGroup(group.id),
    );
  }
}

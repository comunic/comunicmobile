import 'package:comunic/ui/screens/update_conversation_screen.dart';
import 'package:flutter/material.dart';

/// Create a new conversation route
///
/// @author Pierre HUBERT

class CreateConversationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => UpdateConversationScreen();
}

import 'package:comunic/helpers/friends_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/models/advanced_user_info.dart';
import 'package:comunic/models/friend_status.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/widgets/mobile_mode/user_page_mobile.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/ui/widgets/tablet_mode/user_page_tablet.dart';
import 'package:comunic/utils/account_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// User page route
///
/// @author Pierre HUBERT

enum _PageStatus { LOADING, ERROR, READY }

class UserPageScreen extends StatefulWidget {
  final int userID;

  const UserPageScreen({Key? key, required this.userID}) : super(key: key);

  @override
  _UserPageScreenState createState() => _UserPageScreenState();
}

class _UserPageScreenState extends SafeState<UserPageScreen> {
  // Helpers
  final usersHelper = UsersHelper();

  // Objects members
  _PageStatus _status = _PageStatus.LOADING;
  AdvancedUserInfo? _userInfo;
  FriendStatus? _frienshipStatus;
  final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  _setStatus(_PageStatus s) => setState(() => _status = s);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_userInfo?.id == widget.userID) return;
    _getUserInfo();
  }

  Future<void> _getUserInfo() async {
    _setStatus(_PageStatus.LOADING);

    try {
      final user = await usersHelper.getAdvancedInfo(widget.userID);
      final status = widget.userID == userID()
          ? null
          : await FriendsHelper().getFriendshipStatus(widget.userID);

      setState(() {
        _userInfo = user;
        _frienshipStatus = status;
      });

      _setStatus(_PageStatus.READY);
    } on GetUserAdvancedUserError catch (e) {
      _setStatus(_PageStatus.ERROR);

      if (e.cause == GetUserAdvancedInformationErrorCause.NOT_AUTHORIZED) {
        final controller = MainController.of(context)!;
        controller.popPage();
        controller.openUserAccessDeniedPage(widget.userID);
      }
    } catch (e, s) {
      print("Could not refresh user information! $e\n$s");
      _setStatus(_PageStatus.ERROR);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_status == _PageStatus.LOADING) return buildLoadingPage();

    if (_status == _PageStatus.ERROR) return _buildError();

    return Scaffold(
      body: RefreshIndicator(
        key: _refreshIndicatorKey,
        child: _buildBody(),
        onRefresh: _getUserInfo,
      ),
    );
  }

  /// Error card
  Widget _buildError() {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr("Error")!),
      ),
      body: Center(
          child:
              buildErrorCard(tr("Could not get user information!"), actions: [
        TextButton(
          onPressed: _getUserInfo,
          child: Text(
            tr("Retry")!.toUpperCase(),
            style: TextStyle(color: Colors.white),
          ),
        )
      ])),
    );
  }

  Widget _buildBody() {
    return isTablet(context)
        ? UserPageTablet(
            userInfo: _userInfo!,
            onNeedRefresh: () => _refreshIndicatorKey.currentState!.show(),
            friendshipStatus: _frienshipStatus,
          )
        : UserMobilePage(
            userInfo: _userInfo!,
            onNeedRefresh: () => _refreshIndicatorKey.currentState!.show(),
          );
  }
}

import 'package:comunic/helpers/posts_helper.dart';
import 'package:comunic/ui/widgets/posts_list_widget.dart';
import 'package:flutter/material.dart';

/// Newest posts screen
///
/// @author Pierre HUBERT

class NewestPostsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NewestPostsScreenState();
}

class _NewestPostsScreenState extends State<NewestPostsScreen> {
  // Helpers
  final PostsHelper _postsHelper = PostsHelper();


 @override
  Widget build(BuildContext context) {
    return PostsListWidget(
      getPostsList: _postsHelper.getLatest,
      getOlder: (f) => _postsHelper.getLatest(from: f - 1),
      showPostsTarget: true,
    );
  }

}

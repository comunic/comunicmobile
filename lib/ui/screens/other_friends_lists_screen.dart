import 'package:comunic/helpers/friends_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/ui/tiles/simple_user_tile.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/navigation_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Other friends list route (not intended to display current user friends list)
///
/// @author Pierre HUBERT

class OtherUserFriendsListScreen extends StatefulWidget {
  final int userID;
  final bool enableAppBar;

  const OtherUserFriendsListScreen({
    Key? key,
    required this.userID,
    this.enableAppBar = true,
  })  : super(key: key);

  @override
  _OtherUserFriendsListScreenState createState() =>
      _OtherUserFriendsListScreenState();
}

class _OtherUserFriendsListScreenState
    extends State<OtherUserFriendsListScreen> {
  final FriendsHelper friendsHelper = FriendsHelper();
  final UsersHelper usersHelper = UsersHelper();

  late Set<int> _friendsList;
  UsersList? _usersInfo;
  bool _error = false;

  String? get _routeName => tr("Friends of %name%",
      args: {"name": _usersInfo!.getUser(widget.userID).displayName});

  void setError(bool e) => setState(() => _error = e);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    load();
  }

  /// Load the list of friends of the user
  Future<void> load() async {
    setError(false);

    try {
      final friendsList = await friendsHelper.getOtherUserList(widget.userID);

      // We use [Set.toSet] here to avoid the current user to appear in the list
      final users = await usersHelper
          .getListWithThrow(friendsList.toSet()..add(widget.userID));

      setState(() {
        _friendsList = friendsList;
        _usersInfo = users;
      });
    } catch (e, st) {
      print(e);
      print(st);
      setError(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_error) return _buildError();

    if (_usersInfo == null) return buildCenteredProgressBar();

    return Scaffold(
      appBar: widget.enableAppBar
          ? AppBar(
              title: Text(_routeName!),
            )
          : null,
      body: ListView.builder(
        itemCount: _friendsList.length,
        itemBuilder: (c, i) => SimpleUserTile(
          user: _usersInfo!.getUser(_friendsList.elementAt(i)),
          onTap: (u) => openUserPage(
            context: context,
            userID: u.id,
          ),
        ),
      ),
    );
  }

  Widget _buildError() {
    return Scaffold(
      body: buildErrorCard(
        tr(
          "Could not get the list of friends of this user !",
        ),
        actions: [
          TextButton(
            child: Text(
              tr("Try again")!.toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
            onPressed: load,
          )
        ],
      ),
    );
  }
}

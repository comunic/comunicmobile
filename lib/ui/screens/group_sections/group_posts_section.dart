import 'package:comunic/enums/post_target.dart';
import 'package:comunic/helpers/posts_helper.dart';
import 'package:comunic/models/advanced_group_info.dart';
import 'package:comunic/ui/widgets/post_create_form_widget.dart';
import 'package:comunic/ui/widgets/posts_list_widget.dart';
/// Group posts section
///
/// @author Pierre Hubert
import 'package:flutter/material.dart';

class GroupPostsSection extends StatefulWidget {
  final AdvancedGroupInfo group;

  const GroupPostsSection({
    Key? key,
    required this.group,
  }) : super(key: key);

  @override
  _GroupPostsSectionState createState() => _GroupPostsSectionState();
}

class _GroupPostsSectionState extends State<GroupPostsSection> {
  final _postsKey = GlobalKey<PostsListWidgetState>();
  final _formKey = GlobalKey<PostCreateFormWidgetState>();

  /// Add create post target
  Widget _buildPostCreationArea() {
    if (!widget.group.canCreatePost) return Container();

    return PostCreateFormWidget(
      key: _formKey,
      postTarget: PostTarget.GROUP_PAGE,
      targetID: widget.group.id,
      onCreated: () => _postsKey.currentState!.loadPostsList(getOlder: false),
    );
  }

  /// Build the list of posts of the group
  @override
  Widget build(BuildContext context) {
    return PostsListWidget(
      key: _postsKey,
      topWidgets: <Widget>[
        _buildPostCreationArea(),
      ],
      getPostsList: () => PostsHelper().getGroupPosts(widget.group.id),
      showPostsTarget: false,
      userNamesClickable: true,
      getOlder: (from) =>
          PostsHelper().getGroupPosts(widget.group.id, from: from),
    );
  }
}

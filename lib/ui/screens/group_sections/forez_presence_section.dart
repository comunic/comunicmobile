import 'package:comunic/helpers/forez_presence_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/forez_presences_set.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/ui/routes/forez_presence_settings_route.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/forez_presence_calendar_widget.dart';
import 'package:flutter/material.dart';

/// Forez presence section
///
/// @author Pierre Hubert

class ForezPresenceSection extends StatefulWidget {
  final int groupID;

  const ForezPresenceSection({
    Key? key,
    required this.groupID,
  }) : super(key: key);

  @override
  _ForezPresenceSectionState createState() => _ForezPresenceSectionState();
}

class _ForezPresenceSectionState extends State<ForezPresenceSection> {
  PresenceSet? _presences;
  late UsersList _users;
  DateTime _currentDay = DateTime.now();

  List<int> get _currentListOfUsers => _presences!.getUsersAtDate(_currentDay);

  Future<void> _refresh() async {
    await ForezPresenceHelper.refreshCache(widget.groupID);

    _presences = await ForezPresenceHelper.getAll(widget.groupID);
    _users = await UsersHelper().getList(_presences!.usersID);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AsyncScreenWidget(
          onReload: _refresh,
          onBuild: _buildList,
          errorMessage: "Erreur lors du chargement des présences !",
        ),
        Positioned(
          right: 10,
          bottom: 10,
          child: FloatingActionButton(
            backgroundColor: Colors.green,
            onPressed: () => showPresenceSettingsRoute(context, widget.groupID),
            child: Icon(Icons.edit),
          ),
        )
      ],
    );
  }

  Widget _buildList() {
    final currentList = _currentListOfUsers;
    return ListView.builder(
      itemCount: currentList.length + 1,
      itemBuilder: (c, i) =>
          i == 0 ? _buildCalendar() : _buildUser(currentList[i - 1]),
    );
  }

  Widget _buildCalendar() => PresenceCalendarWidget(
        presenceSet: _presences!,
        mode: CalendarDisplayMode.MULTIPLE_USERS,
        selectedDay: _currentDay,
        onDayClicked: _selectDay,
      );

  void _selectDay(DateTime dt) => setState(() => _currentDay = dt);

  Widget _buildUser(int userID) {
    final user = _users.getUser(userID);
    return ListTile(
      leading: AccountImageWidget(user: user),
      title: Text(user.fullName),
    );
  }
}

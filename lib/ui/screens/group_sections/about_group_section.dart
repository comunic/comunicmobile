import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/models/advanced_group_info.dart';
import 'package:comunic/models/group.dart';
import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

/// About group section
///
/// @author Pierre Hubert

class AboutGroupSection extends StatelessWidget {
  final AdvancedGroupInfo group;
  final Function(Group) onReportGroup;

  const AboutGroupSection({
    Key? key,
    required this.group,
    required this.onReportGroup,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => ListView(
        children: [
          // URL, if any
          group.hasURL
              ? ListTile(
                  leading: Icon(Icons.link),
                  title: Text(tr("URL")!),
                  subtitle: Text(group.url),
                  onTap: () => launchUrlString(group.url),
                )
              : Container(),

          // Description, if any
          group.hasDescription
              ? ListTile(
                  leading: Icon(Icons.note),
                  title: Text(tr("Description")!),
                  subtitle: Text(group.description),
                )
              : Container(),

          // Time create
          ListTile(
            leading: Icon(Icons.access_time),
            title: Text("Created"),
            subtitle: Text(diffTimeFromNowToStr(group.timeCreate!)!),
          ),

          // Number of members
          ListTile(
            leading: Icon(Icons.group),
            title: Text(tr("Members")!),
            subtitle: Text(tr("%1% members",
                args: {"1": group.numberMembers.toString()})!),
          ),

          // Who can create posts
          ListTile(
            leading: Icon(Icons.add),
            title: Text(tr("Who can create posts")!),
            subtitle: Text(
                group.postCreationLevel == GroupPostCreationLevel.MEMBERS
                    ? tr("Every members")!
                    : tr("Only moderators and administrators")!),
          ),

          // Registration process
          ListTile(
            leading: Icon(Icons.login),
            title: Text(tr("Registration process")!),
            subtitle: Text(group.registrationLevel ==
                    GroupRegistrationLevel.CLOSED
                ? tr("On invitation only")!
                : (group.registrationLevel == GroupRegistrationLevel.MODERATED
                    ? tr("A moderator has to approve requests")!
                    : tr("Anyone can join the group without approval")!)),
          ),

          // Group visibility
          ListTile(
            leading: Icon(Icons.remove_red_eye),
            title: Text(tr("Visibility")!),
            subtitle: Text(group.visibilityLevel == GroupVisibilityLevel.SECRETE
                ? tr("Secrete group")!
                : (group.visibilityLevel == GroupVisibilityLevel.PRIVATE
                    ? tr("Private group")!
                    : tr("Open group")!)),
          ),

          // Group members visibility
          ListTile(
            leading: Icon(Icons.remove_red_eye),
            title: Text(tr("Members list visibility")!),
            subtitle: Text(
                group.isMembersListPublic! ? tr("Public")! : tr("Private")!),
          ),

          group.isForezGroup
              ? // Group members visibility
              ListTile(
                  leading: Icon(Icons.info_outline),
                  title: Text(tr("Forez group")!),
                  subtitle: Text(tr("Forez special features enabled")!),
                )
              : Container(),
          // Report group
          srvConfig!.isReportingEnabled
              ? ListTile(
                  textColor: Colors.red,
                  leading: Icon(Icons.flag, color: Colors.red),
                  title: Text(tr("Report abuse")!),
                  onTap: () => onReportGroup(group),
                )
              : Container(),
        ],
      );
}

import 'package:comunic/models/conversation.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:flutter/material.dart';

import '../conversation_screen.dart';

/// Group conversation section
///
/// @author Pierre Hubert

class GroupConversationSection extends StatelessWidget {
  final Conversation conv;

  const GroupConversationSection({
    Key? key,
    required this.conv,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Stack(
        children: [
          ConversationScreen(
            conversationID: conv.id!,
          ),
          Positioned(
            right: 1.0,
            child: Row(
              children: [
                conv.callCapabilities != CallCapabilities.NONE
                    ? IconButton(
                        icon: Icon(Icons.phone),
                        onPressed: () =>
                            MainController.of(context)!.startCall(conv.id!),
                      )
                    : Container(),
                IconButton(
                  icon: Icon(Icons.settings),
                  onPressed: () => MainController.of(context)!
                      .openConversationSettingsRoute(conv),
                ),
              ],
            ),
          )
        ],
      );
}

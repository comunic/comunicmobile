import 'package:comunic/helpers/friends_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/models/friend_status.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/widgets/FrienshipStatusWidget.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// User access denied screen
///
/// @author Pierre HUBERT

class UserAccessDeniedScreen extends StatefulWidget {
  final int userID;

  const UserAccessDeniedScreen({Key? key, required this.userID})
      : super(key: key);

  @override
  _UserAccessDeniedScreenState createState() => _UserAccessDeniedScreenState();
}

class _UserAccessDeniedScreenState extends SafeState<UserAccessDeniedScreen> {
  final UsersHelper usersHelper = UsersHelper();
  final FriendsHelper friendsHelper = FriendsHelper();

  final _key = GlobalKey<AsyncScreenWidgetState>();

  late FriendStatus _status;
  late User _user;

  Future<void> refresh() async {
    final status = await friendsHelper.getFriendshipStatus(widget.userID);
    final user = await usersHelper.getSingleWithThrow(widget.userID);

    // Check if the two users are friend now
    if (status.areFriend) {
      final controller = MainController.of(context)!;
      controller.popPage();
      controller.openUserPage(widget.userID);
    }

    _status = status;
    _user = user;
  }

  @override
  Widget build(BuildContext context) {
    return AsyncScreenWidget(
        key: _key,
        onReload: refresh,
        onBuild: _buildPage,
        errorMessage: tr("Could not load friendship information!")!);
  }

  Widget _buildPage() {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AccountImageWidget(
                user: _user,
                width: 75,
              ),
              Text(
                _user.displayName,
                style: TextStyle(fontSize: 25.0),
              ),
              Text(tr("This account is private.")!),
              FriendshipStatusWidget(
                status: _status,
                onFriendshipUpdated: () => _key.currentState!.refresh(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

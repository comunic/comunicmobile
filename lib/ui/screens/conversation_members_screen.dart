import 'package:comunic/helpers/conversations_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/conversation.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Conversation members screen
///
/// @author Pierre Hubert

class ConversationMembersScreen extends StatefulWidget {
  final int convID;

  const ConversationMembersScreen({
    Key? key,
    required this.convID,
  }) : super(key: key);

  @override
  _ConversationMembersScreenState createState() =>
      _ConversationMembersScreenState();
}

class _ConversationMembersScreenState extends State<ConversationMembersScreen> {
  late Conversation _conversation;
  late UsersList _members;

  Future<void> _refresh() async {
    _conversation =
        await ConversationsHelper().getSingle(widget.convID, force: true);
    _members = await UsersHelper().getListWithThrow(_conversation.membersID);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(tr("Conversation members")!)),
      body: AsyncScreenWidget(
        onReload: _refresh,
        onBuild: _buildMembersList,
        errorMessage:
            tr("Could not load the list of members of this conversation!")!,
      ),
    );
  }

  Widget _buildMembersList() => ListView.builder(
        itemBuilder: _buildItem,
        itemCount: _conversation.members!.length,
      );

  Widget _buildItem(BuildContext context, int index) {
    final member = _conversation.members![index];
    final user = _members.getUser(member.userID);
    return ListTile(
      leading: AccountImageWidget(user: user),
      title: Text(user.displayName),
      subtitle: Text(member.isAdmin ? tr("Admin")! : tr("Member")!),
    );
  }
}

import 'package:comunic/helpers/groups_helper.dart';
import 'package:comunic/models/group.dart';
import 'package:comunic/ui/widgets/group_icon_widget.dart';
import 'package:comunic/ui/widgets/group_membership_widget.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Group access denied screen
///
/// @author Pierre Hubert

class GroupAccessDeniedScreen extends StatefulWidget {
  final int groupID;
  final Function() onMembershipAcquired;

  const GroupAccessDeniedScreen({
    Key? key,
    required this.groupID,
    required this.onMembershipAcquired,
  }) : super(key: key);

  @override
  _GroupAccessDeniedScreenState createState() =>
      _GroupAccessDeniedScreenState();
}

class _GroupAccessDeniedScreenState extends SafeState<GroupAccessDeniedScreen> {
  Group? _group;

  bool error = false;

  @override
  void initState() {
    _refresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (error)
      return buildErrorCard(tr("Could not get basic group information!"),
          actions: [
            MaterialButton(
              child: Text(tr("Try again")!.toUpperCase()),
              onPressed: () => this._refresh(),
            )
          ]);

    if (_group == null) return buildCenteredProgressBar();

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(
              flex: 5,
            ),
            GroupIcon(group: _group!),
            Spacer(),
            Text(
              _group!.displayName,
              style: TextStyle(fontSize: 20),
            ),
            Spacer(),
            Text(
              tr("A registration is required to access this group page.")!,
              textAlign: TextAlign.center,
            ),
            Spacer(),
            GroupMembershipWidget(
              group: _group!,
              onUpdated: () => this._refresh(),
              onError: () => this._refresh(),
            ),
            Spacer(
              flex: 5,
            )
          ],
        ),
      ),
    );
  }

  /// Get basic information about the groups
  Future<void> _refresh() async {
    try {
      setState(() {
        error = false;
        _group = null;
      });

      // Get information about a single group
      final group = await GroupsHelper().getSingle(widget.groupID, force: true);

      if (group.isAtLeastMember) widget.onMembershipAcquired();

      setState(() => _group = group);
    } catch (e) {
      print(e);
      setState(() => error = true);
    }
  }
}

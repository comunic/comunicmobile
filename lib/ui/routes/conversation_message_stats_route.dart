import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/conversation.dart';
import 'package:comunic/models/conversation_message.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Conversation message statistics route
///
/// @author Pierre Hubert

class ConversationMessageStatsRoute extends StatefulWidget {
  final Conversation conv;
  final ConversationMessage message;

  const ConversationMessageStatsRoute({
    Key? key,
    required this.conv,
    required this.message,
  })  : super(key: key);

  @override
  _ConversationMessageStatsRouteState createState() =>
      _ConversationMessageStatsRouteState();
}

class _ConversationMessageStatsRouteState
    extends State<ConversationMessageStatsRoute> {
  late UsersList _users;

  Future<void> _init() async {
    _users = await UsersHelper()
        .getList(widget.conv.membersID..add(widget.message.userID));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr("Message statistics")!),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => MainController.of(context)!.popPage(),
        ),
      ),
      body: AsyncScreenWidget(
          onReload: _init,
          onBuild: _buildScreen,
          errorMessage: tr("Failed to load message information!")!),
    );
  }

  List<Widget> get _firstItems => [
        ListTile(
          leading: Icon(Icons.access_time_rounded),
          title: Text(tr("Created on")!),
          subtitle: Text(dateTimeToString(widget.message.date)),
        ),
        ListTile(
          leading: AccountImageWidget(
            user: _users.getUser(widget.message.userID),
          ),
          title: Text(_users.getUser(widget.message.userID).fullName),
          subtitle: Text(tr("Creator")!),
        ),
      ];

  Widget _buildScreen() => ListView.builder(
        itemCount: _firstItems.length + widget.conv.members!.length,
        itemBuilder: (c, i) {
          final firstItems = _firstItems;
          if (i < firstItems.length) return firstItems[i];

          final convMember = widget.conv.members![i - firstItems.length];

          if (convMember.userID == widget.message.userID) return Container();

          return ListTile(
            leading: AccountImageWidget(
              user: _users.getUser(convMember.userID),
            ),
            title: Text(_users.getUser(convMember.userID).fullName),
            subtitle: Text(convMember.lastMessageSeen < widget.message.id!
                ? tr("Message not seen yet")!
                : tr("Message seen")!),
          );
        },
      );
}

import 'package:comunic/helpers/conversations_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/conversation.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/screens/conversation_screen.dart';
import 'package:comunic/ui/widgets/comunic_back_button_widget.dart';
import 'package:comunic/ui/widgets/conversation_image_widget.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Single conversation route
///
/// @author Pierre HUBERT

class ConversationRoute extends StatefulWidget {
  final int conversationID;

  const ConversationRoute({
    Key? key,
    required this.conversationID,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ConversationRouteState();
}

class _ConversationRouteState extends SafeState<ConversationRoute> {
  final ConversationsHelper _conversationsHelper = ConversationsHelper();
  Conversation? _conversation;
  UsersList? _users;
  String? _conversationName;
  bool _error = false;

  setError(bool err) => setState(() => _error = err);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _loadConversation();
  }

  Future<void> _loadConversation() async {
    setError(false);

    try {
      _conversation = await _conversationsHelper
          .getSingle(widget.conversationID, force: true);

      _users = await UsersHelper().getList(_conversation!.membersID);

      final conversationName =
          ConversationsHelper.getConversationName(_conversation!, _users);

      if (!this.mounted) return null;

      setState(() => _conversationName = conversationName);
    } catch (e, s) {
      print("Failed to get conversation name! $e => $s");
      setError(true);
    }
  }

  void _openSettings() =>
      MainController.of(context)!.openConversationSettingsRoute(_conversation!);

  Widget _buildRouteBody() {
    //Handle errors
    if (_error)
      return buildErrorCard(
        tr("Could not get conversation information!"),
        actions: <Widget>[
          TextButton(
            onPressed: _loadConversation,
            child: Text(
              tr("Try again")!.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      );

    if (_conversationName == null || _conversation == null)
      return buildCenteredProgressBar();

    return ConversationScreen(
      conversationID: widget.conversationID,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: isTablet(context)
            ? (_conversation == null || _users == null
                ? null
                : ConversationImageWidget(
                    conversation: _conversation!, users: _users!))
            : ComunicBackButton(),
        title: Text(
          _conversationName == null ? tr("Loading")! : _conversationName!,
        ),
        actions: <Widget>[
          // Start call (if possible)
          _conversation == null ||
                  _conversation!.callCapabilities == CallCapabilities.NONE
              ? Container()
              : IconButton(
                  icon: Icon(Icons.phone),
                  onPressed: () => MainController.of(context)!
                      .startCall(widget.conversationID),
                ),

          // Edit conversation settings
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: _openSettings,
          )
        ],
      ),
      body: _buildRouteBody(),
    );
  }
}

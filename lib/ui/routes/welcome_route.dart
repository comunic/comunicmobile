import 'package:comunic/ui/routes/create_account_route.dart';
import 'package:comunic/ui/routes/login_route.dart';
import 'package:comunic/ui/widgets/login_scaffold.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Login route
///
/// @author Pierre HUBERT

class WelcomeRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WelcomeRouteState();
}

class _WelcomeRouteState extends State<WelcomeRoute> {
  void _openLoginPage() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (c) => LoginRoute()));
  }

  void _openCreateAccountPage() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (c) => CreateAccountRoute()));
  }

  /// Build login form
  Widget _buildLoginForm() {
    return Material(
      color: Colors.transparent,
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.login, color: Colors.white),
            title: Text(tr("Login")!),
            onTap: _openLoginPage,
          ),
          ListTile(
            leading: Icon(Icons.person_add_alt_1, color: Colors.white),
            title: Text(tr("Join the network")!),
            onTap: _openCreateAccountPage,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) => LoginScaffold(
        child: null,
        noStyleChild: _buildLoginForm(),
      );
}

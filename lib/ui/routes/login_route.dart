import 'package:comunic/ui/routes/forgot_password_route.dart';
import 'package:comunic/ui/widgets/init_widget.dart';
import 'package:comunic/ui/widgets/login_route_container.dart';
import 'package:comunic/ui/widgets/login_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Login route
///
/// @author Pierre Hubert

class LoginRoute extends StatefulWidget {
  @override
  _LoginRouteState createState() => _LoginRouteState();
}

class _LoginRouteState extends State<LoginRoute> {
  void _openResetPasswordPage() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (c) => ForgotPasswordRoute()));
  }

  @override
  Widget build(BuildContext context) {
    return LoginRouteContainer(
      child: Scaffold(
        appBar: AppBar(
          title: Text(tr("Login to Comunic")!),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 370),
              child: Column(
                children: [
                  LoginWidget(
                    onSignedIn: () {
                      final nav = Navigator.of(context);
                      nav.pop();
                      nav.pushReplacement(MaterialPageRoute(
                          builder: (b) => InitializeWidget()));
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.help),
                    title: Text(tr("Password forgotten")!),
                    onTap: _openResetPasswordPage,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

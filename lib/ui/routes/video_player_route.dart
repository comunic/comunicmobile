import 'package:chewie/chewie.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/comunic_back_button_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

/// Video player route
///
/// @author Pierre Hubert

class VideoPlayerRoute extends StatefulWidget {
  final String url;

  const VideoPlayerRoute({
    Key? key,
    required this.url,
  }) : super(key: key);

  @override
  _VideoPlayerRouteState createState() => _VideoPlayerRouteState();
}

class _VideoPlayerRouteState extends State<VideoPlayerRoute> {
  VideoPlayerController? _videoPlayerController;
  ChewieController? _chewieController;

  Future<void> _initialize() async {
    _videoPlayerController = VideoPlayerController.network(widget.url);

    await _videoPlayerController!.initialize();

    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController!,
      looping: false,
      allowFullScreen: true,
      allowMuting: true,
      allowedScreenSleep: false,
    );
  }

  @override
  void dispose() {
    if (_videoPlayerController != null) _videoPlayerController!.dispose();
    if (_chewieController != null) _chewieController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: ComunicBackButton(),
        title: Text("Video"),
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() => AsyncScreenWidget(
        onReload: _initialize,
        onBuild: _showBody,
        errorMessage: tr("Failed to initialize video!")!,
      );

  Widget _showBody() => Chewie(controller: _chewieController!);
}

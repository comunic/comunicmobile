import 'package:comunic/helpers/posts_helper.dart';
import 'package:comunic/lists/posts_list.dart';
import 'package:comunic/ui/widgets/posts_list_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Single post route
///
/// @author Pierre HUBERT

class SinglePostRoute extends StatelessWidget {
  final int postID;

  const SinglePostRoute({
    Key? key,
    required this.postID,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr("Post")!),
      ),
      body: _SinglePostRouteBody(postID: postID),
    );
  }
}

class _SinglePostRouteBody extends StatefulWidget {
  final int postID;

  const _SinglePostRouteBody({
    Key? key,
    required this.postID,
  })  : super(key: key);

  @override
  __SinglePostRouteBodyState createState() => __SinglePostRouteBodyState();
}

class __SinglePostRouteBodyState extends State<_SinglePostRouteBody> {
  Future<PostsList?> _getPost() async {
    try {
      return PostsList()..add(await PostsHelper().getSingle(widget.postID));
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return PostsListWidget(
      getPostsList: _getPost,
      showPostsTarget: true,
    );
  }
}

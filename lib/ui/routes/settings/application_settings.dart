import 'package:comunic/helpers/preferences_helper.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/settings/header_spacer_section.dart';
import 'package:comunic/utils/flutter_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';

/// Application settings
///
/// @author Pierre Hubert

class ApplicationSettings extends StatefulWidget {
  @override
  _ApplicationSettingsState createState() => _ApplicationSettingsState();
}

class _ApplicationSettingsState extends State<ApplicationSettings> {
  PreferencesHelper? _preferencesHelper;

  Future<void> _refresh() async {
    _preferencesHelper = await PreferencesHelper.getInstance();
  }

  @override
  Widget build(BuildContext context) => AsyncScreenWidget(
      onReload: _refresh,
      onBuild: _buildSections,
      errorMessage: tr("Could not load settings!")!);

  Widget _buildSections() {
    return SettingsList(
      sections: [
        HeadSpacerSection(),
        _buildAppearanceSection(),
        _buildDebugSection()
      ],
    );
  }

  /// Appearance section
  SettingsSection _buildAppearanceSection() => SettingsSection(
        title: tr("Appearance"),
        tiles: [
          _PreferencesSettingsTile(
            preferencesKey: PreferencesKeyList.ENABLE_DARK_THEME,
            title: tr("Enable dark theme"),
            subtitle: null,
            onChange: _updatedSettings,
            helper: _preferencesHelper,
          ),
        ],
      );

  /// Debug section
  SettingsSection _buildDebugSection() => SettingsSection(
        title: tr("Debug features"),
        tiles: [
          // Force mobile mode
          _PreferencesSettingsTile(
            preferencesKey: PreferencesKeyList.FORCE_MOBILE_MODE,
            title: tr("Force mobile mode"),
            subtitle: isIOS
                ? null
                : tr(
                    "Force the smartphone mode of the application to be used, even when tablet mode could be used."),
            onChange: _updatedSettings,
            helper: _preferencesHelper,
          ),

          // Show performances overlay
          _PreferencesSettingsTile(
            preferencesKey: PreferencesKeyList.SHOW_PERFORMANCE_OVERLAY,
            title: tr("Show performances overlay"),
            subtitle: null,
            onChange: _updatedSettings,
            helper: _preferencesHelper,
          ),
        ],
      );

  /// Apply new settings
  _updatedSettings() {
    setState(() {});
    applyNewThemeSettings(context);
  }
}

class _PreferencesSettingsTile extends SettingsTile {
  final PreferencesKeyList preferencesKey;
  final String? title;
  final String? subtitle;
  final Function onChange;
  final PreferencesHelper? helper;

  _PreferencesSettingsTile({
    required this.preferencesKey,
    required this.title,
    required this.subtitle,
    required this.onChange,
    required this.helper,
  });

  @override
  Widget build(BuildContext context) {
    return SettingsTile.switchTile(
      title: title,
      subtitle: subtitle,
      onToggle: _doChange,
      switchValue: helper!.getBool(preferencesKey),
      titleMaxLines: 2,
      subtitleMaxLines: 4,
    );
  }

  void _doChange(bool value) async {
    await helper!.setBool(preferencesKey, value);
    onChange();
  }
}

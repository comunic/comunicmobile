import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/ui/widgets/copy_icon.dart';
import 'package:comunic/utils/flutter_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';
import 'package:url_launcher/url_launcher_string.dart';

/// About application settings
///
/// @author Pierre Hubert

class AboutApplicationSettings extends StatelessWidget {
  @override
  Widget build(BuildContext context) => SettingsList(
        sections: [
          _buildGeneralSection(context),
        ],
      );

  /// General section
  SettingsSection _buildGeneralSection(BuildContext context) => SettingsSection(
        tiles: [
          SettingsTile(
            title: tr("Privacy policy"),
            onPressed: (c) => launchUrlString(srvConfig!.privacyPolicyURL),
          ),
          SettingsTile(
            title: tr("Terms of Use"),
            onPressed: (c) => launchUrlString(srvConfig!.termsURL),
          ),
          SettingsTile(
            title: tr("Contact us"),
            subtitle: srvConfig!.contactEmail,
            trailing: CopyIcon(srvConfig!.contactEmail),
          ),
          SettingsTile(
            title: tr("About this application"),
            subtitle: isIOS ? null : tr("Learn more about us"),
            onPressed: (_) => showAboutAppDialog(context),
          )
        ],
      );
}

import 'package:comunic/helpers/push_notifications_helper.dart';
import 'package:comunic/helpers/settings_helper.dart';
import 'package:comunic/models/notifications_settings.dart';
import 'package:comunic/ui/routes/push_notifications_route.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/settings/header_spacer_section.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/log_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';

/// Notifications settings
///
/// @author Pierre Hubert

class NotificationsSettingsScreen extends StatefulWidget {
  @override
  _NotificationsSettingsScreenState createState() =>
      _NotificationsSettingsScreenState();
}

class _NotificationsSettingsScreenState
    extends State<NotificationsSettingsScreen> {
  final key = GlobalKey<AsyncScreenWidgetState>();

  late NotificationsSettings _settings;
  PushNotificationsStatus? _pushNotificationsStatus;

  Future<void> _loadSettings() async {
    _settings = await SettingsHelper.getNotificationsSettings();
    _pushNotificationsStatus = await PushNotificationsHelper.getLocalStatus();
  }

  @override
  Widget build(BuildContext context) => AsyncScreenWidget(
        key: key,
        onReload: _loadSettings,
        onBuild: _buildScreen,
        errorMessage: tr("Failed to load notifications settings!")!,
      );

  Widget _buildScreen() => SettingsList(sections: [
        HeadSpacerSection(),
        SettingsSection(
          title: tr("Push notifications"),
          tiles: [
            SettingsTile(
              title: tr("Push notifications status"),
              subtitle: !PushNotificationsHelper.arePushNotificationsAvailable
                  ? tr("Unavailable")
                  : (_pushNotificationsStatus ==
                          PushNotificationsStatus.DISABLED
                      ? tr("Disabled")
                      : (_pushNotificationsStatus ==
                              PushNotificationsStatus.FIREBASE
                          ? tr("Use Google services")
                          : tr("Use independent service"))),
              onPressed: (c) =>
                  showInitialPushNotificationsConfiguration(context),
            ),
            SettingsTile.switchTile(
              title: tr("Allow conversations notification"),
              onToggle: (v) {
                _settings.allowConversations = v;
                _updatedSettings();
              },
              switchValue: _settings.allowConversations,
              subtitle:
                  tr("Receive notifications for the conversations you follow."),
              titleMaxLines: 2,
              subtitleMaxLines: 2,
            )
          ],
        )
      ]);

  void _updatedSettings() async {
    try {
      await SettingsHelper.setNotificationsSettings(_settings);
      setState(() {});
    } catch (e, s) {
      logError(e, s);
      snack(context, tr("Failed to update settings!")!);
    }
  }
}

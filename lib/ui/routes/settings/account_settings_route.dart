import 'package:comunic/ui/routes/settings/about_settings.dart';
import 'package:comunic/ui/routes/settings/account_image_settings.dart';
import 'package:comunic/ui/routes/settings/account_privacy_settings.dart';
import 'package:comunic/ui/routes/settings/account_security_settings.dart';
import 'package:comunic/ui/routes/settings/application_settings.dart';
import 'package:comunic/ui/routes/settings/custom_emojies_account_settings.dart';
import 'package:comunic/ui/routes/settings/general_account_settings.dart';
import 'package:comunic/ui/routes/settings/notifications_settings.dart';
import 'package:comunic/ui/routes/tour_route.dart';
import 'package:comunic/ui/widgets/settings/header_spacer_section.dart';
import 'package:comunic/utils/flutter_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';

enum _MainMenuActions { SHOW_TOUR }

/// Account settings route
///
/// @author Pierre HUBERT

class _SettingsSection {
  final String title;
  final String subtitle;
  final IconData icon;
  final Widget Function() onBuild;

  const _SettingsSection({
    required this.title,
    required this.subtitle,
    required this.icon,
    required this.onBuild,
  });
}

class AccountSettingsRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr("Settings")!),
        actions: [
          PopupMenuButton<_MainMenuActions>(
              onSelected: (v) => _doPopupMenuAction(context, v),
              itemBuilder: (c) => [
                    PopupMenuItem(
                      value: _MainMenuActions.SHOW_TOUR,
                      child: Text(tr("See the tour again")!),
                    ),
                  ]),
        ],
      ),
      body: _AccountSettingsBody(),
    );
  }

  void _doPopupMenuAction(BuildContext context, _MainMenuActions value) async {
    switch (value) {
      case _MainMenuActions.SHOW_TOUR:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (c) => TourRoute()));
        break;
    }
  }
}

class _AccountSettingsBody extends StatefulWidget {
  @override
  __AccountSettingsBodyState createState() => __AccountSettingsBodyState();
}

class __AccountSettingsBodyState extends State<_AccountSettingsBody> {
  late _SettingsSection _currentSection;

  @override
  void initState() {
    super.initState();

    /// Choose first section by default
    _currentSection = _sections[0];
  }

  /// The list of settings sections
  List<_SettingsSection> get _sections => [
        _SettingsSection(
          title: tr("General settings")!,
          subtitle: tr("Configure the main settings of your account")!,
          icon: Icons.settings,
          onBuild: () => GeneralAccountSettingsScreen(),
        ),

        // Emoticons
        _SettingsSection(
          title: tr("Custom emojis")!,
          subtitle: tr("Set your own emoticon shorcuts")!,
          icon: Icons.insert_emoticon,
          onBuild: () => CustomEmojisAccountSettings(),
        ),

        // Account image
        _SettingsSection(
          title: tr("Account image")!,
          subtitle: tr("Customize your account image")!,
          icon: Icons.account_circle,
          onBuild: () => AccountImageSettingsScreen(),
        ),

        // Notifications settings
        _SettingsSection(
          title: tr("Notifications")!,
          subtitle: tr("Choose the notifications you receive on your phone")!,
          icon: Icons.notifications,
          onBuild: () => NotificationsSettingsScreen(),
        ),

        // Security settings
        _SettingsSection(
          title: tr("Security")!,
          subtitle: tr("Manage security options of your account")!,
          icon: Icons.lock,
          onBuild: () => AccountSecuritySettingsScreen(),
        ),

        // Privacy settings
        _SettingsSection(
          title: tr("Privacy")!,
          subtitle: tr("Here you can make actions to protect your privacy")!,
          icon: Icons.security,
          onBuild: () => AccountPrivacySettings(),
        ),

        // Application settings
        _SettingsSection(
          title: tr("Application settings")!,
          subtitle: tr("Manage local application settings")!,
          icon: Icons.smartphone,
          onBuild: () => ApplicationSettings(),
        ),

        // About settings
        _SettingsSection(
          title: tr("About this application")!,
          subtitle: tr("Learn more about us")!,
          icon: Icons.info,
          onBuild: () => AboutApplicationSettings(),
        ),
      ];

  @override
  Widget build(BuildContext context) =>
      isTablet(context) ? _buildTabletMode() : _buildMobileMode();

  /// Tablet mode
  Widget _buildTabletMode() => Row(
        children: <Widget>[
          Container(width: 400, child: _buildTabletLeftPane()),
          Expanded(child: _currentSection.onBuild())
        ],
      );

  Widget _buildTabletLeftPane() => ListView.builder(
        itemBuilder: (c, i) {
          final section = _sections[i];

          return Container(
            color: section.title == _currentSection.title
                ? Colors.black.withAlpha(50)
                : null,
            child: ListTile(
              title: Text(section.title),
              subtitle: Text(section.subtitle),
              leading: Icon(section.icon),
              onTap: () => _openSectionTablet(section),
            ),
          );
        },
        itemCount: _sections.length,
      );

  void _openSectionTablet(_SettingsSection s) =>
      setState(() => _currentSection = s);

  /// Mobile mode
  Widget _buildMobileMode() => SettingsList(
        sections: [
          HeadSpacerSection(),
          SettingsSection(
            title: tr("Settings"),
            tiles: _sections
                .map((f) => SettingsTile(
                      title: f.title,
                      leading: Icon(f.icon),
                      subtitle: isIOS ? null : f.subtitle,
                      onPressed: (_) => _openSectionsAsNewRoute(f),
                      subtitleMaxLines: 3,
                    ))
                .toList(),
          )
        ],
      );

  /// Push a new route with the settings section
  _openSectionsAsNewRoute(_SettingsSection f) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (c) => Scaffold(
        appBar: AppBar(
          title: Text(f.title),
        ),
        body: f.onBuild(),
      ),
    ));
  }
}

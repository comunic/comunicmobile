import 'package:comunic/ui/screens/update_conversation_screen.dart';
import 'package:flutter/material.dart';

/// Update a conversation route
///
/// @author Pierre HUBERT

class UpdateConversationRoute extends StatefulWidget {
  final int conversationID;

  const UpdateConversationRoute({Key? key, required this.conversationID})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _UpdateConversationRoute();
}

class _UpdateConversationRoute extends State<UpdateConversationRoute> {
  @override
  Widget build(BuildContext context) =>
      UpdateConversationScreen(convID: widget.conversationID);
}

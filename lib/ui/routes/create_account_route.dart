import 'package:comunic/ui/routes/login_route.dart';
import 'package:comunic/ui/widgets/create_account_widget.dart';
import 'package:comunic/ui/widgets/login_route_container.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Create account route
///
/// @author Pierre HUBERT

class CreateAccountRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LoginRouteContainer(
      child: Scaffold(
        appBar: AppBar(
          title: Text(tr("Create an account")!),
        ),
        body: _CreateAccountRouteBody(),
      ),
    );
  }
}

class _CreateAccountRouteBody extends StatefulWidget {
  @override
  __CreateAccountRouteBodyState createState() =>
      __CreateAccountRouteBodyState();
}

class __CreateAccountRouteBodyState extends State<_CreateAccountRouteBody> {
  @override
  Widget build(BuildContext context) =>
      CreateAccountWidget(onCreated: _accountCreated);

  void _accountCreated() async {
    await showCupertinoDialog(
      context: context,
      builder: (c) => CupertinoAlertDialog(
        title: Text(tr("Account created")!),
        content: Text(tr(
            "Your account has been successfully created. You can now login to start to use it.")!),
        actions: <Widget>[
          CupertinoButton(
              child: Text(tr("Login")!), onPressed: () => Navigator.of(c).pop())
        ],
      ),
    );

    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (c) => LoginRoute()));
  }
}

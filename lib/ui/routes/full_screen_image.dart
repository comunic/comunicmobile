import 'package:cached_network_image/cached_network_image.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:url_launcher/url_launcher_string.dart';

/// Full screen image details
///
/// @author Pierre Hubert

class FullScreenImageRoute extends StatefulWidget {
  final String url;
  final String? cacheKey;

  FullScreenImageRoute({required this.url, this.cacheKey});

  @override
  _FullScreenImageRouteState createState() => _FullScreenImageRouteState();
}

class _FullScreenImageRouteState extends State<FullScreenImageRoute> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr("Image")!),
        actions: [
          IconButton(
              icon: Icon(Icons.launch), onPressed: () => launchUrlString(widget.url))
        ],
      ),
      body: PhotoView(
          loadingBuilder: (c, i) => Center(child: CircularProgressIndicator()),
          imageProvider: CachedNetworkImageProvider(
            widget.url,
            cacheKey: widget.cacheKey,
          )),
    );
  }
}

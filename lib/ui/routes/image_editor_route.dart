import 'dart:io';

import 'package:comunic/models/api_request.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/log_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';

import '../../models/api_request.dart';
import '../../utils/files_utils.dart';

/// Image cropper route
///
/// @author Pierre Hubert

/// Attempt to crop image
///
/// Return original image in case of error / if the user did not crop the image
Future<BytesFile> showImageCropper(BuildContext context, BytesFile source,
    {CropAspectRatio? aspectRatio}) async {
  File? file;
  File? cropped;

  try {
    file = await generateTemporaryFile();
    await file.writeAsBytes(source.bytes!);

    var cropped = await ImageCropper().cropImage(
      sourcePath: file.absolute.path,
      compressFormat: ImageCompressFormat.png,
      aspectRatio: aspectRatio,
      uiSettings: [
        AndroidUiSettings(
          toolbarColor: Colors.black,
          toolbarTitle: tr("Crop Photo"),
          toolbarWidgetColor: Colors.white,
        ),
        IOSUiSettings(title: tr("Crop Photo"))
      ],
    );

    if (cropped == null) return source;

    return BytesFile("cropped.png", await cropped.readAsBytes());
  } catch (e, s) {
    logError(e, s);
    snack(context, tr("Failed to execute image cropper!")!);
    return source;
  } finally {
    if (file != null && await file.exists()) file.delete();
    if (cropped != null && await cropped.exists()) cropped.delete();
  }
}

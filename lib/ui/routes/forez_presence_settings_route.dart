import 'package:comunic/helpers/forez_presence_helper.dart';
import 'package:comunic/lists/forez_presences_set.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/forez_presence_calendar_widget.dart';
import 'package:comunic/utils/account_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/log_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Forez presence settings route
///
/// On this route, users can change there presence settings
///
/// @author Pierre Hubert

/// Show presence settings route
Future<void> showPresenceSettingsRoute(
        BuildContext context, int? groupID) async =>
    await Navigator.push(context,
        MaterialPageRoute(builder: (c) => PresenceSettings(groupID: groupID!)));

class PresenceSettings extends StatefulWidget {
  final int groupID;

  const PresenceSettings({
    Key? key,
    required this.groupID,
  }) : super(key: key);

  @override
  _PresenceSettingsState createState() => _PresenceSettingsState();
}

class _PresenceSettingsState extends State<PresenceSettings> {
  late PresenceSet _currPresences;

  Future<void> _load() async {
    await ForezPresenceHelper.refreshCache(widget.groupID);
    _currPresences =
        await ForezPresenceHelper.getForUser(widget.groupID, userID());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr("Presence settings")!),
      ),
      body: AsyncScreenWidget(
        onReload: _load,
        onBuild: _buildScreen,
        errorMessage: tr("Failed to load data!")!,
      ),
    );
  }

  Widget _buildScreen() => ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              tr("Please click on the day you will be in the plain, so that everyone gets informed ! ;)")!,
              textAlign: TextAlign.center,
            ),
          ),
          PresenceCalendarWidget(
            presenceSet: _currPresences,
            onDayClicked: _toggleDay,
          ),
        ],
      );

  void _toggleDay(DateTime dt) async {
    try {
      // Update current list
      _currPresences.toggleDate(dt, userID());

      // Update on server
      if (_currPresences.containsDate(dt))
        await ForezPresenceHelper.addDay(widget.groupID, dt);
      else
        await ForezPresenceHelper.delDay(widget.groupID, dt);
    } catch (e, s) {
      logError(e, s);
      _currPresences.toggleDate(dt, userID());
      snack(context, tr("Failed to update information!")!);
    }
  }
}

import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/routes/main_route/page_info.dart';
import 'package:comunic/ui/screens/notifications_screen.dart';
import 'package:comunic/ui/widgets/banner_widget.dart';
import 'package:comunic/ui/widgets/mobile_mode/mobile_appbar_widget.dart';
import 'package:flutter/material.dart';

/// Main route of the application
///
/// @author Pierre HUBERT

class SmartphoneMainRoute extends StatefulWidget implements MainRoute {
  const SmartphoneMainRoute({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MainRouteState();
}

/// Private implementation of HomeController
class _MainRouteState extends MainController {
  @override
  PageInfo get defaultPage =>
      PageInfo(type: PageType.NOTIFICATIONS_PAGE, child: NotificationsScreen());

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueAccent,
      child: SafeArea(
        // Avoid OS areas
        child: WillPopScope(
          onWillPop: willPop,
          child: Scaffold(
            appBar: currentPage.hideNavBar
                ? null
                : ComunicMobileAppBar(currentPage: currentPage),
            body: SafeArea(
                key: currentPage.key,
                child: Column(
                  children: [
                    BannerWidget(),
                    Expanded(child: currentPage.child)
                  ],
                )),
          ),
        ),
      ),
    );
  }
}

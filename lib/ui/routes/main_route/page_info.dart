import 'package:flutter/cupertino.dart';

/// Single page information
///
/// @author Pierre HUBERT

enum PageType {
  USER_PAGE,
  GROUP_PAGE,
  CONVERSATION_PAGE,
  NOTIFICATIONS_PAGE,
  CONVERSATIONS_LIST_PAGE,
  FRIENDS_LIST_PAGE,
  LATEST_POSTS_PAGE,
  OTHER_PAGE
}

class PageInfo {
  final PageType type;
  final Widget child;
  final int? id;
  final bool hideNavBar;
  final bool canShowAsDialog;

  /// Unique identification of this child
  final key = UniqueKey();

  PageInfo({
    this.type = PageType.OTHER_PAGE,
    required this.child,
    this.id,
    this.hideNavBar = false,
    this.canShowAsDialog = false,
  });
}

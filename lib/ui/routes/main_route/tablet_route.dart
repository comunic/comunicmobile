import 'package:comunic/models/conversation.dart';
import 'package:comunic/ui/dialogs/screen_dialog.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/routes/main_route/page_info.dart';
import 'package:comunic/ui/screens/newest_posts.dart';
import 'package:comunic/ui/widgets/banner_widget.dart';
import 'package:comunic/ui/widgets/tablet_mode/calls/calls_area.dart';
import 'package:comunic/ui/widgets/tablet_mode/conversations/conversations_area_widget.dart';
import 'package:comunic/ui/widgets/tablet_mode/current_user_panel.dart';
import 'package:comunic/ui/widgets/tablet_mode/global_search_field.dart';
import 'package:comunic/ui/widgets/tablet_mode/memberships_panel.dart';
import 'package:comunic/ui/widgets/tablet_mode/tablet_appbar_widget.dart';
import 'package:flutter/material.dart';

/// Main tablet route
///
/// @author Pierre Hubert

const _SideBarSize = 300.0;

class TabletRoute extends StatefulWidget implements MainRoute {
  const TabletRoute({Key? key}) : super(key: key);

  @override
  _TabletRouteState createState() => _TabletRouteState();
}

class _TabletRouteState extends MainController {
  final _conversationsKey = GlobalKey<ConversationsAreaWidgetState>();
  final _callsKey = GlobalKey<CallsAreaState>();

  @override
  PageInfo get defaultPage => PageInfo(child: NewestPostsScreen());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: willPop,
      child: Scaffold(
        appBar: _buildAppBar() as PreferredSizeWidget?,
        body: _buildBody(),
      ),
    );
  }

  Widget _buildAppBar() => ComunicTabletAppBarWidget();

  Widget _buildBody() => Stack(
        children: [
          Row(children: <Widget>[_buildLeftPane(), _buildRightPane()]),
          Positioned(
            right: 0,
            bottom: 0,
            child: ConversationsAreaWidget(key: _conversationsKey),
          ),
          CallsArea(key: _callsKey),
        ],
      );

  Widget _buildLeftPane() => Theme(
        data: Theme.of(context).copyWith(
          textTheme: TextTheme(bodyText2: TextStyle(color: Colors.white)),
          iconTheme: IconThemeData(color: Colors.white),
        ),
        child: Container(
          width: _SideBarSize,
          color: Color(0xFF222D32),
          child: Column(
            children: <Widget>[
              CurrentUserPanel(),
              Container(height: 10),
              GlobalSearchField(),
              Container(height: 10),
              Expanded(child: MembershipsPanel(currentPage: currentPage))
            ],
          ),
        ),
      );

  Widget _buildRightPane() => Container(
        key: currentPage.key,
        width: MediaQuery.of(context).size.width - _SideBarSize,
        child: Column(
          children: [
            BannerWidget(),
            Expanded(child: currentPage.child),
          ],
        ),
      );

  @override
  void pushPage(PageInfo page) {
    if (page.canShowAsDialog == true)
      showScreenDialog(context, page.child);
    else
      super.pushPage(page);
  }

  @override
  void openConversation(Conversation conv, {fullScreen: false}) {
    if (fullScreen)
      super.openConversation(conv, fullScreen: fullScreen);
    else
      openConversationById(conv.id, fullScreen: false);
  }

  @override
  void openConversationById(int? convID, {fullScreen = false}) {
    if (!fullScreen) {
      popUntilMainRoute();
      _conversationsKey.currentState!.openConversations(convID);
    } else
      super.openConversationById(convID!, fullScreen: fullScreen);
  }

  @override
  void startCall(int convID) => _callsKey.currentState!.openCall(convID);
}

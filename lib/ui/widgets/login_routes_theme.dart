import 'package:comunic/models/config.dart';
import 'package:flutter/material.dart';

/// Login forms base theme
///
/// @author Pierre Hubert

class LoginRoutesTheme extends StatelessWidget {
  final Widget child;

  const LoginRoutesTheme({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) => Theme(
        data: Theme.of(context).copyWith(
            scaffoldBackgroundColor: Config.get()!.splashBackgroundColor,
            appBarTheme: AppBarTheme(
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
            iconTheme: IconThemeData(color: Colors.white),
            inputDecorationTheme: InputDecorationTheme(
              fillColor: Colors.white,
              hintStyle: TextStyle(color: Colors.white),
              labelStyle: TextStyle(color: Colors.white),
              prefixStyle: TextStyle(color: Colors.white),
              hoverColor: Colors.white,
              errorStyle: TextStyle(color: Colors.deepOrangeAccent),
            ),
            checkboxTheme: CheckboxThemeData(
              fillColor: MaterialStateProperty.all(Colors.white),
              checkColor:
                  MaterialStateProperty.all(Config.get()!.splashBackgroundColor),
            ),
            dialogBackgroundColor: Config.get()!.splashBackgroundColor,
            dialogTheme: DialogTheme(
              contentTextStyle: TextStyle(
                color: Colors.white,
              ),
            ),
            buttonTheme: ButtonThemeData(buttonColor: Colors.white),
            primaryColor: Colors.white,
            backgroundColor: Config.get()!.splashBackgroundColor,
            disabledColor: Colors.grey,
            highlightColor: Colors.white12,
            hintColor: Colors.white,
            textTheme: TextTheme(subtitle1: TextStyle(color: Colors.white)),
            radioTheme: RadioThemeData(
              fillColor: MaterialStateProperty.all(Colors.white),
            ),
            colorScheme: ColorScheme(
              primary: Colors.white,
              secondary: Colors.white,
              surface: Config.get()!.splashBackgroundColor,
              background: Config.get()!.splashBackgroundColor,
              error: Colors.redAccent,
              onPrimary: Config.get()!.splashBackgroundColor,
              onSecondary: Config.get()!.splashBackgroundColor,
              onSurface: Colors.white,
              onBackground: Colors.white,
              onError: Colors.redAccent,
              brightness: Brightness.dark,
            ).copyWith(secondary: Colors.white)),
        child: child,
      );
}

import 'package:flutter/material.dart';

/// Display an icon button with a badge
///
/// Based from Medium article called "Notification Badge in Flutter"
///
/// @author Pierre Hubert

class IconButtonWithBadge extends StatelessWidget {
  final Widget icon;
  final void Function() onPressed;
  final int number;
  final bool active;

  const IconButtonWithBadge({
    Key? key,
    required this.icon,
    required this.onPressed,
    this.number = 0,
    this.active = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: active ? Color(0x55000000) : null,
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          IconButton(icon: icon, onPressed: onPressed),
          number != 0
              ? Positioned(
                  right: 6,
                  top: 6,
                  child: Container(
                    padding: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(minWidth: 14, minHeight: 14),
                    child: Text(
                      "$number",
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                  ))
              : Container()
        ],
      ),
    );
  }
}

import 'package:comunic/utils/navigation_utils.dart';
import 'package:flutter/material.dart';

class ComunicBackButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BackButton(
      onPressed: () => popPage(context),
    );
  }
}

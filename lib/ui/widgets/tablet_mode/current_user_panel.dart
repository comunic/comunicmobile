import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/account_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Current user panel
///
/// @author Pierre HUBERT

class CurrentUserPanel extends StatefulWidget {
  @override
  _CurrentUserPanelState createState() => _CurrentUserPanelState();
}

class _CurrentUserPanelState extends SafeState<CurrentUserPanel> {
  User? _user;

  Future<void> _refresh() async {
    try {
      final user = await UsersHelper().getSingleWithThrow(userID());

      setState(() => _user = user);
    } catch (e, s) {
      print("Could not load user panel! $e\n$s");

      setTimeout(5, _refresh);
    }
  }

  @override
  void initState() {
    super.initState();
    _refresh();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      child: Center(child: _buildContent()),
    );
  }

  Widget _buildContent() {
    if (_user == null) return buildCenteredProgressBar();

    return ListTile(
      onTap: () => MainController.of(context)!.openCurrentUserPage(),
      leading: AccountImageWidget(
        user: _user!,
        width: 50,
      ),
      title: Text(_user!.displayName),
    );
  }
}

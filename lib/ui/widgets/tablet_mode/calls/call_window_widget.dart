import 'package:comunic/ui/screens/call_screen.dart';
import 'package:comunic/ui/widgets/custom_app_bar_size.dart';
import 'package:comunic/ui/widgets/tablet_mode/calls/calls_area.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Call window widget
///
/// @author Pierre HUBERT

const _WindowSize = Size(450, 200);

class CallWindowWidget extends StatefulWidget {
  final int convID;
  final void Function() onClose;

  const CallWindowWidget({
    Key? key,
    required this.convID,
    required this.onClose,
  }) : super(key: key);

  @override
  _CallWindowWidgetState createState() => _CallWindowWidgetState();
}

class _CallWindowWidgetState extends State<CallWindowWidget> {
  double? _left, _top;

  var _fullScreen = false;

  final _callScreenKey = GlobalKey();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // Initialize window coordinates
    _top = 10;
    _left = MediaQuery.of(context).size.width - 10 - _WindowSize.width;
  }

  @override
  Widget build(BuildContext context) {
    if (_fullScreen) return _buildScreen();

    return Positioned(
      width: _WindowSize.width,
      height: _WindowSize.height,
      left: _left,
      top: _top,
      child: Draggable(
        child: Card(
          child: _buildScreen(),
        ),
        feedback: Container(
          width: _WindowSize.width,
          height: _WindowSize.height,
          color: Colors.red,
        ),
        onDragEnd: _moveEnd,
      ),
    );
  }

  Widget _buildScreen() => CallScreen(
        key: _callScreenKey,
        buildCustomAppBar: (convName) => AppBarWrapper(
            height: 30,
            appBar: AppBar(
              backgroundColor: Colors.black,
              title: Text(convName == null ? tr("Loading...")! : convName),
              actions: <Widget>[
                // Go full screen
                IconButton(
                  iconSize: 16,
                  icon: Icon(
                      _fullScreen ? Icons.fullscreen_exit : Icons.fullscreen),
                  onPressed: _toggleFullScreen,
                ),

                // Close call
                IconButton(
                  iconSize: 16,
                  icon: Icon(Icons.close),
                  onPressed: widget.onClose,
                )
              ],
            )),
        convID: widget.convID,
        floatingButtons: _fullScreen,
        onClose: widget.onClose,
      );

  /// Compute new window position
  void _moveEnd(DraggableDetails details) {
    // Determine the limits of containing stack
    RenderBox renderBox = context
        .findAncestorStateOfType<CallsAreaState>()!
        .context
        .findRenderObject() as RenderBox;
    final size = renderBox.size;
    final offset = renderBox.localToGlobal(Offset.zero);

    // Determine new window position
    _top = details.offset.dy - offset.dy;
    _left = details.offset.dx - offset.dx;

    // Force the window to appear completely on the screen
    if (_top! + _WindowSize.height >= size.height)
      _top = size.height - _WindowSize.height;
    if (_left! + _WindowSize.width >= size.width)
      _left = size.width - _WindowSize.width;

    if (_top! < 0) _top = 0;
    if (_left! < 0) _left = 0;

    setState(() {});
  }

  void _toggleFullScreen() => setState(() => _fullScreen = !_fullScreen);
}

import 'package:comunic/ui/widgets/tablet_mode/calls/call_window_widget.dart';
import 'package:flutter/material.dart';

/// Tablets mode calls area
///
/// @author Pierre Hubert

class CallsArea extends StatefulWidget {
  const CallsArea({Key? key}) : super(key: key);

  @override
  CallsAreaState createState() => CallsAreaState();
}

class CallsAreaState extends State<CallsArea> {
  final _openCalls = Map<int, Key>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: _openCalls
          .map((convID, key) => MapEntry(
              convID,
              CallWindowWidget(
                key: key,
                convID: convID,
                onClose: () => closeCall(convID),
              )))
          .values
          .toList(),
    );
  }

  /// Open a new call
  void openCall(int convID) {
    if (!_openCalls.containsKey(convID)) {
      setState(() {
        _openCalls[convID] = UniqueKey();
      });
    }
  }

  /// Close a call
  void closeCall(int convID) {
    if (_openCalls.containsKey(convID))
      setState(() => _openCalls.remove(convID));
  }
}

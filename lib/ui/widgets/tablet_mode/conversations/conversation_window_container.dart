import 'package:comunic/ui/widgets/custom_app_bar_size.dart';
import 'package:flutter/material.dart';

/// Conversation window
///
/// Simple container for a conversation
///
/// @author Pierre Hubert

class ConversationWindowContainer extends StatelessWidget {
  final Color? appBarBgColor;
  final Widget? icon;
  final Widget title;
  final void Function() onClose;
  final void Function() onToggleCollapse;
  final bool isCollapsed;
  final Widget body;
  final List<Widget>? action;

  const ConversationWindowContainer({
    Key? key,
    this.appBarBgColor,
    this.icon,
    required this.title,
    required this.onClose,
    required this.body,
    required this.onToggleCollapse,
    required this.isCollapsed,
    this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        width: 300,
        height: !isCollapsed ? 400 : 40,
        child: Scaffold(
          appBar: AppBarWrapper(
              height: 40,
              appBar: AppBar(
                backgroundColor: appBarBgColor,
                leading: icon,
                title: GestureDetector(child: title, onTap: onToggleCollapse),
                actions: action ?? []
                  ..add(
                    IconButton(icon: Icon(Icons.close), onPressed: onClose),
                  ),
                toolbarTextStyle:
                    TextTheme(headline6: TextStyle(fontSize: 15)).bodyText2,
                titleTextStyle:
                    TextTheme(headline6: TextStyle(fontSize: 15)).headline6,
              )),
          body: Visibility(
            child: body,
            visible: !isCollapsed,
            maintainState: true,
          ),
        ),
      ),
    );
  }
}

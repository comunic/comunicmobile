import 'package:comunic/ui/widgets/tablet_mode/conversations/conversation_window.dart';
import 'package:comunic/ui/widgets/tablet_mode/conversations/open_conversation_button_widget.dart';
import 'package:flutter/material.dart';

/// Conversations area widget
///
/// This widget allow floating conversations in tablet mode
///
/// @author Pierre

class ConversationsAreaWidget extends StatefulWidget {
  const ConversationsAreaWidget({Key? key}) : super(key: key);

  @override
  ConversationsAreaWidgetState createState() => ConversationsAreaWidgetState();
}

class ConversationsAreaWidgetState extends State<ConversationsAreaWidget> {
  final _openConversations = Map<int?, UniqueKey>();

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: _openConversations.map(_conversationWindow).values.toList()
        ..add(_buildOpenButton()),
    );
  }

  /// Add a button to open new conversations
  Widget _buildOpenButton() => OpenConversationButton();

  /// Open a new conversation
  void openConversations(int? convID) {
    if (!_openConversations.containsKey(convID))
      setState(() => _openConversations[convID] = UniqueKey());
  }

  MapEntry<int?, Widget> _conversationWindow(int? convID, UniqueKey key) =>
      MapEntry(
        convID,
        ConversationWindow(
          key: key,
          convID: convID!,
          onClose: () => setState(() => _openConversations.remove(convID)),
        ),
      );
}

import 'package:comunic/ui/screens/conversations_list_screen.dart';
import 'package:flutter/material.dart';

/// This buttons open a menu
/// to choose a conversation to open
///
/// @author Pierre Hubert

class OpenConversationButton extends StatefulWidget {
  @override
  _OpenConversationButtonState createState() => _OpenConversationButtonState();
}

class _OpenConversationButtonState extends State<OpenConversationButton> {
  bool _showConversationsList = false;

  void _setShowConversationsList(bool s) =>
      setState(() => _showConversationsList = s);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          _showConversationsList ? _buildConversationsList() : Container(),
          FloatingActionButton(
            heroTag: null,
            onPressed: () => _setShowConversationsList(!_showConversationsList),
            child: Icon(_showConversationsList ? Icons.close : Icons.message),
          )
        ],
      ),
    );
  }

  Widget _buildConversationsList() => Container(
      width: 300,
      height: 500,
      child: Card(
        child: ConversationsListScreen(
          useSmallFAB: true,
          onOpen: () => _setShowConversationsList(false),
        ),
      ));
}

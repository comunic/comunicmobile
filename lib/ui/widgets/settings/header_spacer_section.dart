import 'package:flutter/material.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';

// ignore: must_be_immutable
class HeadSpacerSection extends CustomSection {
  HeadSpacerSection() : super(child: Container(height: 10));
}

import 'package:comunic/ui/dialogs/multi_choices_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';

/// Multiple choices settings tile
///
/// @author Pierre Hubert

class MultiChoicesSettingsTile<T> extends SettingsTile {
  final String title;
  final List<MultiChoiceEntry<T>> choices;
  final T currentValue;
  final Function(T) onChanged;
  final Widget? leading;
  final Widget? trailing;

  MultiChoicesSettingsTile({
    Key? key,
    required this.title,
    required this.choices,
    required this.currentValue,
    required this.onChanged,
    this.leading,
    this.trailing,
  }) : assert(currentValue != null);

  @override
  Widget build(BuildContext context) {
    return SettingsTile(
      leading: leading,
      trailing: trailing,
      title: title,
      subtitle: choices.firstWhere((f) => f.id == currentValue).title,
      onPressed: (_) => _changeValue(context),
    );
  }

  /// When the user request to change the current value
  void _changeValue(BuildContext context) async {
    final newChoice = await showMultiChoicesDialog(
      context: context,
      choices: choices,
      defaultChoice: currentValue,
      title: title,
    );
    if (newChoice == null) return;
    onChanged(newChoice);
  }
}

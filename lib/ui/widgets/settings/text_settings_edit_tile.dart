import 'package:comunic/ui/dialogs/single_input_dialog.dart';
import 'package:comunic/utils/flutter_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/string_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';

/// Text edit settings tile
///
/// @author Pierre HUBERT

bool _defaultCheck(String s) => s.isNotEmpty;

class TextEditSettingsTile extends SettingsTile {
  final String title;
  final String currValue;
  final void Function(String)? onChanged;
  final bool Function(String) checkInput;
  final bool allowEmptyValues;
  final int maxLines;
  final int? maxLength;

  bool get readOnly => onChanged == null;

  TextEditSettingsTile({
    Key? key,
    required this.title,
    required this.currValue,
    required this.onChanged,
    this.checkInput = _defaultCheck,
    this.allowEmptyValues = false,
    this.maxLength,
    this.maxLines = 1,
  });

  @override
  Widget build(BuildContext context) {
    return SettingsTile(
      title: title,
      subtitle: isIOS ? reduceString(currValue, 20) : currValue,
      onPressed: readOnly ? null : (_) => _changeValue(context),
    );
  }

  void _changeValue(BuildContext context) async {
    final value = await showDialog<String>(
      context: context,
      builder: (b) => SingleInputDialog(
        title: title,
        icon: null,
        initialValue: currValue,
        label: title,
        checkInput: checkInput,
        errorMessage: tr("Invalid value!")!,
        canBeEmpty: allowEmptyValues,
        maxLines: maxLines,
        maxLength: maxLength,
      ),
    );

    if (value == null) return;

    onChanged!(value);
  }
}

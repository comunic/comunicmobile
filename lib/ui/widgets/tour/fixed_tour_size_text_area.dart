/// Fixed tour size text area
///
/// @author Pierre Hubert
import 'package:flutter/material.dart';

class FixedTourSizeTextArea extends StatelessWidget {
  final String? text;

  const FixedTourSizeTextArea(this.text);

  @override
  Widget build(BuildContext context) => ConstrainedBox(
    constraints: BoxConstraints(maxWidth: 300),
    child: Text(text!, textAlign: TextAlign.justify),
  );
}

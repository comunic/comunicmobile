import 'package:comunic/ui/routes/push_notifications_route.dart';
import 'package:comunic/ui/widgets/tour/presentation_pane.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Tour notifications pane
///
/// Invite the user to configure push notifications for its device
///
/// @author Pierre Hubert

class TourNotificationsPane extends PresentationPane {
  TourNotificationsPane({
    Key? key,
    required
        GlobalKey<PushNotificationsConfigurationWidgetState>
            pushNotificationsKey,
    required Function() onConfigured,
    required Function() onChanged,
    required bool visible,
  })  : super(
          icon: Icons.notifications,
          title: tr("Push notifications")!,
          child: (c) => PushNotificationsConfigurationWidget(
            key: pushNotificationsKey,
            onConfigured: onConfigured,
            onChanged: onChanged,
          ),
          canGoNext: pushNotificationsKey.currentState?.canSubmit ?? false,
          onTapNext: (c) => pushNotificationsKey.currentState!.submit(),
          visible: visible,
        );
}

import 'package:comunic/models/config.dart';
import 'package:comunic/ui/widgets/tour/fixed_tour_size_text_area.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// First tour pane
///
/// @author Pierre Hubert

class FirstTourPane extends StatelessWidget {
  final String? msgOne;
  final String? msgTwo;

  const FirstTourPane({Key? key, this.msgOne, this.msgTwo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Spacer(flex: 3),
        Text(
          config().appName,
          style: TextStyle(fontSize: 25),
        ),
        Spacer(flex: 2),
        FixedTourSizeTextArea(msgOne ??
            tr("Welcome to Comunic, the social network that respect your privacy!")),
        Spacer(flex: 1),
        FixedTourSizeTextArea(msgTwo ??
            tr("Let's configure a few things and present you some features of the network...")),
        Spacer(flex: 3),
      ],
    );
  }
}

/// Application presentation tour pane
///
/// @author Pierre Hubert
import 'package:comunic/ui/widgets/tour/fixed_tour_size_text_area.dart';
import 'package:flutter/material.dart';

class PresentationPane extends StatelessWidget {
  final IconData? icon;
  final Widget? iconWidget;
  final String title;
  final String? text;
  final Function(BuildContext)? child;
  final String? actionTitle;
  final Function(BuildContext)? onActionTap;
  final bool canGoNext;
  final Future<bool> Function(BuildContext)? onTapNext;
  final bool visible;

  const PresentationPane({
    Key? key,
    this.icon,
    this.iconWidget,
    required this.title,
    this.text,
    this.child,
    this.actionTitle,
    this.onActionTap,
    this.canGoNext = true,
    this.onTapNext,
    this.visible = true,
  })  : assert(icon != null || iconWidget != null),
        assert(text != null || child != null),
        super(key: key);

  bool get _hasAction => actionTitle != null && onActionTap != null;

  @override
  Widget build(BuildContext context) {
    if (text != null)
      return Column(
        children: <Widget>[
          Spacer(flex: 3),
          icon != null
              ? Icon(icon, color: Colors.white, size: 50)
              : iconWidget!,
          Spacer(flex: 1),
          Text(
            title,
            style: TextStyle(fontSize: 20),
          ),
          Spacer(flex: 1),
          FixedTourSizeTextArea(text),
          Spacer(flex: 1),
          _hasAction
              ? OutlinedButton(
                  onPressed: () => onActionTap!(context),
                  child: Text(
                    actionTitle!,
                    style: TextStyle(color: Colors.white),
                  ),
                )
              : Opacity(
                  opacity: 0,
                  child: OutlinedButton(
                    onPressed: null,
                    child: Text(""),
                  ),
                ),
          Spacer(flex: 3),
        ],
      );

    return Column(
      children: <Widget>[
        Spacer(flex: 1),
        icon != null ? Icon(icon, color: Colors.white, size: 50) : iconWidget!,
        Spacer(flex: 1),
        Text(
          title,
          style: TextStyle(fontSize: 20),
        ),
        Spacer(flex: 1),
        ConstrainedBox(
            constraints: BoxConstraints(maxHeight: 300),
            child: SingleChildScrollView(child: child!(context))),
        Spacer(flex: 1),
      ],
    );
  }
}

import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Last tour pane
///
/// @author Pierre Hubert

class LastTourPane extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        tr("The application is yours")!,
        style: TextStyle(fontSize: 25),
      ),
    );
  }
}

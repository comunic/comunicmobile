import 'package:comunic/models/user.dart';
import 'package:comunic/ui/routes/settings/account_image_settings.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/ui/widgets/tour/presentation_pane.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

class AccountImageTourPane extends PresentationPane {
  final Function(BuildContext) onUpdated;

  AccountImageTourPane({
    required User user,
    required this.onUpdated,
  }) : super(
            iconWidget: AccountImageWidget(user: user, width: 50),
            title: tr("Account image")!,
            text:
                "${tr("Account images allow to quickly recognize people.")}\n\n${tr("You can decide to define one now!")}",
            actionTitle: tr("Upload an account image"),
            onActionTap: (ctx) async {
              await uploadNewAccountImage(ctx);
              onUpdated(ctx);
            });
}

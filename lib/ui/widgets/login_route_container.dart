import 'package:comunic/models/config.dart';
import 'package:comunic/ui/widgets/login_routes_theme.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Login route container
///
/// Contains both theme and route centering
///
/// @author Pierre Hubert

class LoginRouteContainer extends StatelessWidget {
  final Widget child;

  const LoginRouteContainer({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!isTablet(context)) return LoginRoutesTheme(child: child);

    return Container(
      color: Config.get()!.splashBackgroundColor,
      child: Padding(
        padding: EdgeInsets.only(top: 10),
        child: Center(
          child: LoginRoutesTheme(
            child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 370),
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}

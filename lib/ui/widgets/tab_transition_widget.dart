import 'package:flutter/material.dart';

class TabTransitionWidget extends StatefulWidget {
  final Widget child;

  const TabTransitionWidget(this.child, {Key? key})
      : super(key: key);

  @override
  _TabTransitionWidgetState createState() => _TabTransitionWidgetState();
}

class _TabTransitionWidgetState extends State<TabTransitionWidget> {
  var _show = false;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(microseconds: 10)).then((value) {
      if (mounted) setState(() => _show = true);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!_show)
      return Center(child: CircularProgressIndicator());
    else
      return widget.child;
  }
}

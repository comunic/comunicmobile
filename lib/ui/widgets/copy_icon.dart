import 'package:clipboard/clipboard.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Icon used to copy content in clipboard
///
/// @author Pierre Hubert

class CopyIcon extends StatelessWidget {
  final String value;

  const CopyIcon(this.value);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.content_copy),
      onPressed: () {
        FlutterClipboard.copy(value);
        snack(
            context, tr("'%c%' was copied to clipboard", args: {"c": value})!);
      },
    );
  }
}

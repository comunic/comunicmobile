import 'package:cached_network_image/cached_network_image.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/conversation.dart';
import 'package:comunic/models/group.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:flutter/material.dart';

/// Conversation image widget
///
/// @author Pierre Hubert

class ConversationImageWidget extends StatelessWidget {
  final Conversation conversation;
  final UsersList users;
  final double size;
  final Group? group;
  final bool? noUserImage;

  const ConversationImageWidget({
    Key? key,
    required this.conversation,
    required this.users,
    this.group,
    this.size = 30,
    this.noUserImage,
  })  : assert(size > 0),
        super(key: key);

  @override
  Widget build(BuildContext context) => Material(
        child: _buildIcon(),
        color: Colors.transparent,
        borderRadius: BorderRadius.all(
          Radius.circular(18.0),
        ),
        clipBehavior: Clip.hardEdge,
      );

  Widget _buildIcon() {
    if (conversation.logoURL != null)
      return CachedNetworkImage(
        imageUrl: conversation.logoURL!,
        width: size,
      );

    if (group != null) {
      return CachedNetworkImage(
        imageUrl: group!.iconURL,
        width: size,
      );
    }

    if (noUserImage == true) return Container(width: size);

    if (conversation.members!.length < 2)
      return Icon(
        Icons.lock,
        size: size,
      );

    if (conversation.members!.length == 2)
      return AccountImageWidget(
        width: size,
        user: users.getUser(conversation.otherMembersID.first),
      );

    return MultipleAccountImagesWidget(
      users:
          conversation.otherMembersID.map((id) => users.getUser(id)).toList(),
      size: size,
    );
  }
}

class MultipleAccountImagesWidget extends StatelessWidget {
  final List<User> users;
  final double size;

  const MultipleAccountImagesWidget({
    Key? key,
    required this.users,
    required this.size,
  })  : assert(size > 0),
        super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        width: size,
        height: size,
        child: _buildContent(),
      );

  Widget _buildContent() {
    if (users.length == 2) return _buildFirstRow();

    return Column(
      children: [_buildFirstRow(), _buildSecondRow()],
    );
  }

  Widget _buildFirstRow() => Row(
        children: [
          AccountImageWidget(
            user: users[0],
            width: size / 2,
          ),
          AccountImageWidget(
            user: users[1],
            width: size / 2,
          )
        ],
      );

  Widget _buildSecondRow() => Row(
        children: [
          AccountImageWidget(
            user: users[2],
            width: size / 2,
          ),
          users.length > 3
              ? AccountImageWidget(
                  user: users[3],
                  width: size / 2,
                )
              : Container()
        ],
      );
}

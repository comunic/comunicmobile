import 'package:comunic/helpers/account_helper.dart';
import 'package:comunic/models/authentication_details.dart';
import 'package:comunic/utils/input_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Login widget
///
/// Used to authenticate user
///
/// @author Pierre Hubert

class LoginWidget extends StatefulWidget {
  final void Function() onSignedIn;

  const LoginWidget({Key? key, required this.onSignedIn}) : super(key: key);

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  final _emailEditingController = TextEditingController();
  final _passwordEditingController = TextEditingController();
  bool _loading = false;
  AuthResult? _authResult;

  String get _currEmail => _emailEditingController.text;

  String get _currPassword => _passwordEditingController.text;

  bool get _canSubmit => validateEmail(_currEmail) && _currPassword.length >= 3;

  /// Build error card
  Widget? _buildErrorCard() {
    if (_authResult == null) return null;

    //Determine the right message
    final message = (_authResult == AuthResult.INVALID_CREDENTIALS
        ? tr("Invalid credentials!")
        : (_authResult == AuthResult.TOO_MANY_ATTEMPTS
            ? tr(
                "Too many unsuccessful login attempts! Please try again later...")
            : tr("A network error occurred!")));

    return buildErrorCard(message);
  }

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Container(
            child: _buildErrorCard(),
          ),
          //Email address
          TextField(
            controller: _emailEditingController,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                labelText: tr("Email address"),
                alignLabelWithHint: true,
                errorText: _currEmail.length > 0 && !validateEmail(_currEmail)
                    ? tr("Invalid email address!")
                    : null),
            onChanged: (s) => setState(() {}),
          ),

          //Password
          TextField(
            controller: _passwordEditingController,
            obscureText: true,
            decoration: InputDecoration(
              labelText: tr("Password"),
              alignLabelWithHint: true,
            ),
            onChanged: (s) => setState(() {}),
            onSubmitted: _canSubmit ? (s) => _submitForm(context) : null,
          ),

          Container(
            padding: EdgeInsets.all(8.0),
            child: _loading
                ? CircularProgressIndicator()
                : ElevatedButton(
                    child: Text(tr("Sign in")!),
                    onPressed: _canSubmit ? () => _submitForm(context) : null,
                  ),
          ),
        ],
      );

  /// Call this whenever the user request to perform login
  Future<void> _submitForm(BuildContext context) async {
    setState(() {
      _loading = true;
    });

    final loginResult = await AccountHelper().signIn(
        AuthenticationDetails(email: _currEmail, password: _currPassword));

    if (loginResult == AuthResult.SUCCESS)
      widget.onSignedIn();
    else
      setState(() {
        _authResult = loginResult;
        _loading = false;
      });
  }
}

import 'package:flutter/material.dart';

/// Here is a widget used to contain a post
///
/// @author Pierre Hubert

class PostContainer extends StatelessWidget {
  final Widget child;

  const PostContainer({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: ConstrainedBox(
        constraints: BoxConstraints.loose(Size.fromWidth(500)),
        child: child,
      ),
    );
  }
}

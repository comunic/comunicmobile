import 'package:cached_network_image/cached_network_image.dart';
import 'package:comunic/models/user.dart';
import 'package:flutter/material.dart';

/// Account image widget
///
/// This widget is intended to only show a user account image
///
/// @author Pierre HUBERT

class AccountImageWidget extends StatelessWidget {
  final User user;
  final double width;

  const AccountImageWidget({
    Key? key,
    required this.user,
    this.width = 35.0,
  })  : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: CachedNetworkImage(
        imageUrl: user.accountImageURL,
        width: width,
        height: width,
        fit: BoxFit.cover,
        placeholder: (c, s) => Container(
          color: Colors.grey,
          width: width,
          height: width,
          child: CircularProgressIndicator(
            strokeWidth: 7.0,
          ),
        ),
        errorWidget: (c, s, o) => Container(
          color: Colors.red,
          width: width,
          height: width,
          child: Icon(
            Icons.error,
            color: Colors.white,
          ),
        ),
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(18.0),
      ),
      clipBehavior: Clip.hardEdge,
    );
  }
}

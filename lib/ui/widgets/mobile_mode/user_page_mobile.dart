import 'package:comunic/models/advanced_user_info.dart';
import 'package:comunic/ui/screens/friends_list_screen.dart';
import 'package:comunic/ui/screens/other_friends_lists_screen.dart';
import 'package:comunic/ui/screens/user_page_sections/about_user_section.dart';
import 'package:comunic/ui/screens/user_page_sections/user_page_header.dart';
import 'package:comunic/ui/screens/user_page_sections/user_posts_section.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Mobile mode of user page
///
/// @author Pierre Hubert

class UserMobilePage extends StatefulWidget {
  final AdvancedUserInfo userInfo;
  final void Function() onNeedRefresh;

  const UserMobilePage({
    Key? key,
    required this.userInfo,
    required this.onNeedRefresh,
  }) : super(key: key);

  @override
  _UserMobilePageState createState() => _UserMobilePageState();
}

class _UserMobilePageState extends State<UserMobilePage>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  List<UserPageTab> get _tabs => [
        // User posts
        UserPageTab(
          label: tr("Posts")!,
          onBuild: (c) => UserPostsSection(
            user: widget.userInfo,
          ),
        ),

        // About user
        UserPageTab(
          label: tr("About")!,
          onBuild: (c) => AboutUserSection(user: widget.userInfo),
        ),

        // User friends
        UserPageTab(
            label: tr("Friends")!,
            onBuild: (c) => widget.userInfo.isCurrentUser
                ? FriendsListScreen(
                    showAppBar: false,
                  )
                : OtherUserFriendsListScreen(
                    userID: widget.userInfo.id,
                    enableAppBar: false,
                  ),
            visible: widget.userInfo.isFriendsListPublic ||
                widget.userInfo.isCurrentUser),
      ].where((element) => element.visible).toList();

  @override
  void initState() {
    super.initState();

    _tabController = TabController(length: _tabs.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Column(children: [
        _buildHeader(),
        _buildBody(),
      ]);

  Widget _buildHeader() => Material(
        color: Colors.blue.shade700,
        textStyle: TextStyle(color: Colors.white),
        child: Column(
          children: [
            UserPageHeader(
              user: widget.userInfo,
              bgColor: Colors.transparent,
            ),
            TabBar(
              controller: _tabController,
              tabs: _tabs.map((e) => e.tab).toList(),
            ),
          ],
        ),
      );

  Widget _buildBody() => Expanded(
        child: TabBarView(
          controller: _tabController,
          children: _tabs.map((e) => e.onBuild(context)).toList(),
        ),
      );
}

class UserPageTab {
  final String label;
  final WidgetBuilder onBuild;
  final bool visible;

  UserPageTab({
    required this.label,
    required this.onBuild,
    this.visible = true,
  });

  Tab get tab => Tab(text: label);
}

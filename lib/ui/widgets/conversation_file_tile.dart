import 'package:cached_network_image/cached_network_image.dart';
/// Chat file tile
///
/// @author Pierre Hubert
import 'package:comunic/models/conversation_message.dart';
import 'package:comunic/ui/dialogs/audio_player_dialog.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/routes/video_player_route.dart';
import 'package:comunic/ui/widgets/network_image_widget.dart';
import 'package:filesize/filesize.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

const _AreaWidth = 150.0;
const _AreaHeight = 100.0;

class ConversationFileWidget extends StatefulWidget {
  final int messageID;
  final ConversationMessageFile file;

  const ConversationFileWidget({
    Key? key,
    required this.messageID,
    required this.file,
  }) : super(key: key);

  @override
  _ConversationFileWidgetState createState() => _ConversationFileWidgetState();
}

class _ConversationFileWidgetState extends State<ConversationFileWidget> {
  ConversationMessageFile get file => widget.file;

  String get _thumbCacheKey => "conv-msg-thumb-${widget.messageID}";

  String get _fileCacheKey => "conv-msg-file-${widget.messageID}";

  @override
  Widget build(BuildContext context) => Stack(
        children: [
          !file.hasThumbnail ||
                  file.fileType == ConversationMessageFileType.IMAGE
              ? Container(
                  width: 0,
                )
              : Opacity(
                  opacity: 0.8,
                  child: CachedNetworkImage(
                    imageUrl: file.thumbnail!,
                    cacheKey: _thumbCacheKey,
                    width: _AreaWidth,
                    height: _AreaHeight,
                    fit: BoxFit.cover,
                  ),
                ),
          Container(
            width: _AreaWidth,
            height: _AreaHeight,
            child: _buildContent(),
          )
        ],
      );

  Widget _buildContent() {
    switch (file.fileType) {
      // Images
      case ConversationMessageFileType.IMAGE:
        return Center(
          child: NetworkImageWidget(
            url: file.url!,
            cacheKey: _fileCacheKey,
            thumbnailURL: file.thumbnail,
            thumbnailCacheKey: _thumbCacheKey,
            allowFullScreen: true,
          ),
        );

      // We open it in the browser
      default:
        return Container(
          child: Center(
            child: MaterialButton(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spacer(flex: 2),
                  Icon(file.icon, color: Colors.white),
                  Spacer(),
                  Text(
                    file.name!.length < 23
                        ? file.name!
                        : file.name!.substring(0, 20) + "...",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                  Spacer(),
                  Text(
                    filesize(file.size),
                    style: TextStyle(fontSize: 10, color: Colors.white),
                  ),
                  Spacer(flex: 2),
                ],
              ),
              onPressed: _openFile,
            ),
          ),
        );
    }
  }

  void _openFile() {
    switch (file.fileType) {
      case ConversationMessageFileType.AUDIO:
        showAudioPlayerDialog(context, file.url);
        break;

      case ConversationMessageFileType.VIDEO:
        MainController.of(context)!.push(
          VideoPlayerRoute(url: file.url!),
          hideNavBar: true,
          canShowAsDialog: true,
        );
        break;

      default:
        launchUrlString(file.url!);
    }
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:comunic/models/group.dart';
import 'package:flutter/material.dart';

/// Custom group icon
///
/// @author Pierre Hubert
class GroupIcon extends StatelessWidget {
  final Group group;
  final double width;

  const GroupIcon({
    Key? key,
    required this.group,
    this.width = 50,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: CachedNetworkImage(
        imageUrl: group.iconURL,
        width: width,
        height: width,
        placeholder: (c, s) => Container(
          color: Colors.grey,
          width: width,
          height: width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircularProgressIndicator(
              strokeWidth: 4.0,
            ),
          ),
        ),
        errorWidget: (c, s, o) => Container(
          color: Colors.red,
          width: width,
          height: width,
          child: Icon(
            Icons.error,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

import 'dart:async';

import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Countdown widget
///
/// @author Pierre HUBERT

class CountdownWidget extends StatefulWidget {
  final int startTime;
  final int endTime;

  const CountdownWidget({
    Key? key,
    required this.startTime,
    required this.endTime,
  })  : super(key: key);

  @override
  _CountdownWidgetState createState() => _CountdownWidgetState();
}

class _CountdownWidgetState extends State<CountdownWidget> {
  int get remainingTime => widget.endTime - time();

  int get totalDuration => (widget.endTime - widget.startTime).abs();

  String? get remainingTimeStr {
    final remaining = Duration(seconds: remainingTime.abs());

    return tr(
      "%days% Days %hours% Hours %minutes% Minutes %seconds% Seconds",
      args: {
        "days": remaining.inDays.toString(),
        "hours": (remaining.inHours % 24).toString(),
        "minutes": (remaining.inMinutes % 60).toString(),
        "seconds": (remaining.inSeconds % 60).toString(),
      },
    );
  }

  @override
  void initState() {
    super.initState();

    Timer.periodic(Duration(seconds: 1), _timerCb);
  }

  void _timerCb(Timer timer) {
    if (!mounted) {
      timer.cancel();
      return;
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(remainingTimeStr!),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: LinearProgressIndicator(
              value: remainingTime <= 0
                  ? 1.0
                  : 1 - (remainingTime / totalDuration),
              backgroundColor: Theme.of(context).colorScheme.secondary,
              valueColor: AlwaysStoppedAnimation<Color>(
                  Theme.of(context).backgroundColor),
            ),
          )
        ],
      ),
    );
  }
}

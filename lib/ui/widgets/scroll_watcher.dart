import 'package:flutter/material.dart';

/// Scroll watcher
///
/// @author Pierre HUBERT

typedef OnReachBottomCallback = void Function();

class ScrollWatcher extends ScrollController {

  // Callbacks
  OnReachBottomCallback? onReachBottom;

  ScrollWatcher({this.onReachBottom}) {
    addListener(_updatePosition);
  }



  void _updatePosition() {

    // Refresh bottom position
    if(position.pixels.floor() == position.maxScrollExtent.floor())
      onReachBottom!();

  }
}
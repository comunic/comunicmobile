import 'package:comunic/helpers/events_helper.dart';
import 'package:comunic/helpers/notifications_helper.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/log_utils.dart';
import 'package:flutter/material.dart';

/// Status widget
///
/// Store for its children information about the number of unread conversations
/// & the number of unread conversations
///
/// @author Pierre Hubert

class StatusWidget extends StatefulWidget {
  final Widget Function(BuildContext) child;

  const StatusWidget({
    Key? key,
    required this.child,
  })  : super(key: key);

  @override
  StatusWidgetState createState() => StatusWidgetState();
}

class StatusWidgetState extends SafeState<StatusWidget> {
  int? unreadNotifications = 0;
  int? unreadConversations = 0;

  Future<void> init() async {
    try {
      final res = await NotificationsHelper().countUnread();
      unreadNotifications = res.notifications;
      unreadConversations = res.conversations;

      EventsHelper.emit(NewNumberNotifsEvent(unreadNotifications));
      EventsHelper.emit(NewNumberUnreadConversations(unreadConversations));
    } catch (e, s) {
      logError(e, s);
      print("Failed to initialize StatusWidget!");
    }
  }

  @override
  void initState() {
    super.initState();
    init();

    listen<NewNumberNotifsEvent>((e) => unreadNotifications = e.newNum);
    listen<NewNumberUnreadConversations>((e) => unreadConversations = e.newNum);
  }

  /// Find an ancestor of this object
  static StatusWidgetState? of(BuildContext c) =>
      c.findAncestorStateOfType<StatusWidgetState>();

  @override
  Widget build(BuildContext context) => widget.child(context);
}

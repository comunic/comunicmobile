import 'dart:async';

import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/models/server_config.dart';
import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

bool _bannerDismissed = false;

class BannerWidget extends StatefulWidget {
  const BannerWidget({Key? key}) : super(key: key);

  @override
  _BannerWidgetState createState() => _BannerWidgetState();
}

class _BannerWidgetState extends State<BannerWidget> {
  Timer? _timer;

  bool get _shouldShowBanner => showBanner && !_bannerDismissed;

  void _hideBanner() {
    if (this.mounted) setState(() => _bannerDismissed = true);
  }

  void _openLink() {
    launchUrlString(srvConfig!.banner!.link!);
  }

  @override
  void initState() {
    super.initState();

    if (_shouldShowBanner && srvConfig!.banner!.expire != null) {
      _timer = Timer(
          Duration(seconds: srvConfig!.banner!.expire! - time()), _hideBanner);
    }
  }

  @override
  void dispose() {
    if (_timer != null) _timer!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!_shouldShowBanner) return Container();

    final banner = srvConfig!.banner!;
    return Card(
      color: banner.nature == BannerNature.Information
          ? Colors.blue
          : banner.nature == BannerNature.Success
              ? Colors.green
              : Colors.red,
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Row(
          children: [
            BannerButton(
              icon: Icon(
                banner.nature == BannerNature.Information
                    ? Icons.info
                    : banner.nature == BannerNature.Success
                        ? Icons.check
                        : Icons.warning,
              ),
            ),
            Expanded(
              child: Text(
                banner.message.containsKey(shortLang)
                    ? banner.message[shortLang]!
                    : banner.message["en"]!,
                style: TextStyle(color: Colors.white),
              ),
            ),
            banner.link == null
                ? Container()
                : BannerButton(
                    onPressed: _openLink,
                    icon: Icon(Icons.open_in_new),
                  ),
            BannerButton(
              onPressed: _hideBanner,
              icon: Icon(Icons.close),
            )
          ],
        ),
      ),
    );
  }
}

class BannerButton extends StatelessWidget {
  final Function()? onPressed;
  final Widget? icon;

  const BannerButton({this.onPressed, this.icon, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: onPressed,
      icon: icon!,
      color: Colors.white,
      disabledColor: Colors.white,
      padding: EdgeInsets.all(1.0),
    );
  }
}

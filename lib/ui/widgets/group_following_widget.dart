import 'package:comunic/helpers/groups_helper.dart';
import 'package:comunic/models/group.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Group following status widget
///
/// @author Pierre Hubert

class GroupFollowingWidget extends StatefulWidget {
  final Group group;
  final Function() onUpdated;
  final Color? inactiveColor;
  final Color? activeColor;

  const GroupFollowingWidget({
    Key? key,
    required this.group,
    required this.onUpdated,
    this.activeColor,
    this.inactiveColor,
  }) : super(key: key);

  @override
  _GroupFollowingWidgetState createState() => _GroupFollowingWidgetState();
}

class _GroupFollowingWidgetState extends SafeState<GroupFollowingWidget> {
  Group get _group => widget.group;

  @override
  Widget build(BuildContext context) {
    if (!_group.isAtLeastMember) return Container();

    return InkWell(
      child: RichText(
        text: TextSpan(children: [
          WidgetSpan(
              child: Icon(
            Icons.notifications,
            size: 16.0,
            color: _group.following ? widget.activeColor : widget.inactiveColor,
          )),
          TextSpan(
              text:
                  "  " + (_group.following ? tr("Following")! : tr("Follow")!)),
        ]),
      ),
      onTap: () => _toggleFollowing(),
    );
  }

  /// Toggle following status
  void _toggleFollowing() async {
    if (!await GroupsHelper().setFollowing(_group.id, !_group.following)) {
      showSimpleSnack(context, tr("Could not update following status!")!);
      return;
    }

    setState(() {
      _group.following = !_group.following;

      this.widget.onUpdated();
    });
  }
}

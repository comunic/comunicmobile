import 'package:comunic/utils/input_utils.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

/// Text rich content widget
///
/// This widget is used to parse user content (URLs, references,
/// layout content...)
///
/// @author Pierre HUBERT

class TextRichContentWidget extends StatelessWidget {
  final TextAlign? textAlign;
  final TextStyle? style;
  final String text;

  TextRichContentWidget(
    this.text, {
    this.textAlign,
    this.style,
  });

  /// Parse the text and return it as a list of span elements
  static List<TextSpan> _parse(String text, TextStyle? style) {
    if (style == null) style = TextStyle();

    List<TextSpan> list = [];
    String currString = "";

    text.split("\n").forEach((f) {
      text.split(" ").forEach((s) {
        if (validateUrl(s)) {
          if (currString.length > 0)
            //"Flush" previous text
            list.add(TextSpan(style: style, text: currString + " "));

          // Add link
          list.add(TextSpan(
              style: style!.copyWith(color: Colors.indigo),
              text: s,
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  launchUrlString(s);
                }));

          currString = "";
        } else
          currString += s;

        currString += " ";
      });

      currString += "\n";
    });

    list.add(TextSpan(
        style: style, text: currString.substring(0, currString.length - 2)));

    return list;
  }

  @override
  Widget build(BuildContext context) {
    return RichText(
        textAlign: textAlign!, text: TextSpan(children: _parse(text, style)));
  }
}

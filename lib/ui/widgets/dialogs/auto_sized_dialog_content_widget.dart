import 'package:flutter/material.dart';

/// Widget that can be used to build dialog content
///
/// @author Pierre Hubert
class AutoSizeDialogContentWidget extends StatelessWidget {
  final Widget child;

  const AutoSizeDialogContentWidget({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
        constraints:
            BoxConstraints(maxHeight: MediaQuery.of(context).size.height - 50),
        child: SingleChildScrollView(
          child: child,
        ));
  }
}

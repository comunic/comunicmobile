import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Render a cancel button for an alert dialog
///
/// @author Pierre HUBERT

class CancelDialogButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: () => Navigator.of(context).pop(),
      child: Text(tr("Cancel")!.toUpperCase()),
      textColor: Colors.red,
    );
  }
}

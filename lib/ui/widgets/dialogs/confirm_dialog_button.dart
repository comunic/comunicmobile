import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Confirm dialog button
///
/// @author Pierre HUBERT

class ConfirmDialogButton<T> extends StatelessWidget {
  final bool enabled;
  final T value;

  const ConfirmDialogButton({
    Key? key,
    this.enabled = true,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: enabled ? () => Navigator.of(context).pop(value) : null,
      child: Text(tr("Confirm")!.toUpperCase()),
      textColor: Colors.green,
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Network image widget
///
/// Widget useful to show remote image
///
/// @author Pierre HUBERT

class NetworkImageWidget extends StatelessWidget {
  final String url;
  final String? cacheKey;
  final String? thumbnailURL;
  final String? thumbnailCacheKey;
  final bool allowFullScreen;
  final bool roundedEdges;
  final double? width;
  final double? height;
  final double? loadingHeight;

  const NetworkImageWidget({
    Key? key,
    required this.url,
    this.cacheKey,
    this.thumbnailURL,
    this.thumbnailCacheKey,
    this.allowFullScreen = false,
    this.width,
    this.height,
    this.roundedEdges = true,
    this.loadingHeight,
  }) : super(key: key);

  double? get _loadingHeight => loadingHeight != null ? loadingHeight : height;

  double? get _loadingWidth => width;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: allowFullScreen
            ? () {
                showImageFullScreen(context, url, cacheKey: cacheKey);
              }
            : null,
        child: CachedNetworkImage(
          imageUrl: thumbnailURL ?? url,
          cacheKey: thumbnailURL != null ? thumbnailCacheKey : cacheKey,
          width: width,
          height: height,
          fit: BoxFit.cover,
          placeholder: (c, s) => Container(
            width: _loadingWidth,
            height: _loadingHeight,
            color: Colors.lightBlueAccent,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
          errorWidget: (c, s, o) => Container(
            width: _loadingWidth,
            height: _loadingHeight,
            color: Colors.red,
            child: Center(
              child: Icon(
                Icons.error,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
      borderRadius:
          roundedEdges ? BorderRadius.all(Radius.circular(8.0)) : null,
      clipBehavior: Clip.hardEdge,
    );
  }
}

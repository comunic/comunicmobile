import 'package:comunic/models/config.dart';
import 'package:comunic/ui/widgets/banner_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Custom "Scaffold" for signed out users
///
/// @author Pierre Hubert

class LoginScaffold extends StatelessWidget {
  final Widget? child;
  final Widget? noStyleChild;

  const LoginScaffold({Key? key, required this.child, this.noStyleChild})
      : assert(child != null || noStyleChild != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    final availableHeight =
        mediaQuery.size.height - mediaQuery.viewInsets.bottom;
    final minHeight = 450.0;

    final contentHeight =
        availableHeight < minHeight ? minHeight : availableHeight;

    return Theme(
      data: Theme.of(context).copyWith(
        hintColor: Colors.white,
        inputDecorationTheme: InputDecorationTheme(
          errorStyle: TextStyle(color: Colors.white),
          hintStyle: TextStyle(color: Colors.white),
          labelStyle: TextStyle(color: Colors.white),
          focusColor: Colors.white,
          fillColor: Colors.white,
          hoverColor: Colors.white,
        ),
        scaffoldBackgroundColor: Config.get()!.splashBackgroundColor,
        textTheme: TextTheme(
          bodyText2: TextStyle(color: Colors.white),
          button: TextStyle(color: Colors.white),
        ),
      ),
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              BannerWidget(),
              Expanded(
                child: SingleChildScrollView(
                  child: Center(
                    child: Container(
                      height: contentHeight,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: 300),
                        child: Column(
                          children: <Widget>[
                            Spacer(flex: 2),
                            Text(config().appName,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 50)),
                            Spacer(flex: 1),
                            Text(
                              tr(config().appQuickDescription ??
                                  tr("Free social network that respect your privacy"))!,
                              textAlign: TextAlign.center,
                            ),
                            Spacer(flex: 3),
                            child != null
                                ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Material(
                                      child: child,
                                      color: Colors.indigo.shade500,
                                    ),
                                  )
                                : Container(),
                            noStyleChild ?? Container(),
                            Spacer(flex: 3),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

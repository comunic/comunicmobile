import 'package:flutter/material.dart';

/// Customize the size of the AppBar
///
/// @author Pierre Hubert

/// Reduce the size of the appbar
class AppBarWrapper extends StatelessWidget implements PreferredSizeWidget {
  final Widget appBar;
  final double height;

  const AppBarWrapper({required this.height, required this.appBar})
      : super();

  @override
  Widget build(BuildContext context) => appBar;

  @override
  Size get preferredSize => Size.fromHeight(height);
}

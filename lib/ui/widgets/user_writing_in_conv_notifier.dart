import 'package:comunic/helpers/events_helper.dart';
import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/log_utils.dart';
import 'package:flutter/material.dart';

/// User writing a message in a conversation notifier
///
/// @author Pierre Hubert

class UserWritingInConvNotifier extends StatefulWidget {
  final int convID;

  const UserWritingInConvNotifier({Key? key, required this.convID})
      : super(key: key);

  @override
  _UserWritingInConvNotifierState createState() =>
      _UserWritingInConvNotifierState();
}

class _UserWritingInConvNotifierState
    extends SafeState<UserWritingInConvNotifier> {
  final _usersInfo = UsersList();

  final _list = [];

  @override
  void initState() {
    this.listen<WritingMessageInConversationEvent>((ev) async {
      try {
        if (ev.convID != widget.convID) return;

        if (!_usersInfo.hasUser(ev.userID))
          _usersInfo.add(await UsersHelper().getSingleWithThrow(ev.userID));

        _handleEvent(ev.userID);
      } catch (e, s) {
        logError(e, s);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.only(left: 5, right: 5),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            writingText!,
            style: TextStyle(fontSize: 10),
            textAlign: TextAlign.justify,
          ),
        ),
      );

  String? get writingText {
    if (_list.isEmpty) return "";

    final users = _list.toSet().map((e) => _usersInfo.getUser(e)).toList();

    if (users.length == 1)
      return tr("%1% is writing...", args: {"1": users.first.fullName});

    return tr("%1% and %2% are writing...", args: {
      "1": users.first.fullName,
      "2": users.sublist(1).map((e) => e.fullName).join(", ")
    });
  }

  void _handleEvent(int? userID) async {
    setState(() => this._list.add(userID));

    await Future.delayed(
        Duration(seconds: srvConfig!.conversationsPolicy.writingEventLifetime));

    setState(() => this._list.removeAt(0));
  }
}

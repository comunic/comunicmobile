import 'package:comunic/helpers/likes_helper.dart';
import 'package:comunic/models/like_element.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Like widget
///
/// @author Pierre Hubert

/// Updated linking callback
/// int : new number of likes
/// bool : new user liking status
typedef UpdatedLikingCallBack = Function(int, bool);

class LikeWidget extends StatefulWidget {
  final LikeElement likeElement;
  final double? buttonIconSize;
  final Color? activeColor;
  final Color? inativeColor;

  const LikeWidget(
      {Key? key,
      required this.likeElement,
      this.buttonIconSize = 15.0,
      this.activeColor,
      this.inativeColor})
      : super(key: key);

  @override
  _LikeWidgetState createState() => _LikeWidgetState();
}

class _LikeWidgetState extends SafeState<LikeWidget> {
  LikeElement get elem => widget.likeElement;

  String? get _likeString {
    switch (elem.likes) {
      case 0:
        return tr("Like");

      case 1:
        return tr("1 Like");

      default:
        return tr("%num% likes", args: {"num": elem.likes.toString()});
    }
  }

  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
      child: IntrinsicWidth(
        child: InkWell(
          onTap: () => _toggleLike(),
          child: Row(
            children: <Widget>[
              Icon(
                Icons.thumb_up,
                color: elem.userLike
                    ? (widget.activeColor ?? Colors.blue)
                    : widget.inativeColor,
                size: widget.buttonIconSize,
              ),
              SizedBox(
                width: 8.0,
              ),
              Text(_likeString!),
            ],
          ),
        ),
      ),
    );
  }

  /// Toggle like status
  void _toggleLike() async {
    // As like are not really important, we ignore failures
    try {
      await LikesHelper()
          .setLiking(type: elem.likeType, like: !elem.userLike, id: elem.id);
      setState(() {
        elem.userLike = !elem.userLike;

        elem.likes += elem.userLike ? 1 : -1;
      });
    } catch (e, stack) {
      print("$e\n$stack");
    }
  }
}

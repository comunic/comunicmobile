import 'package:comunic/helpers/friends_helper.dart';
import 'package:comunic/models/friend_status.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Friendship status widget
///
/// @author Pierre HUBERT

const WhiteTextColorStyle = TextStyle(color: Colors.white);

class FriendshipStatusWidget extends StatefulWidget {
  final FriendStatus status;
  final void Function() onFriendshipUpdated;

  const FriendshipStatusWidget({
    Key? key,
    required this.status,
    required this.onFriendshipUpdated,
  })  : super(key: key);

  @override
  _FriendshipStatusWidgetState createState() => _FriendshipStatusWidgetState();
}

class _FriendshipStatusWidgetState extends State<FriendshipStatusWidget> {
  final FriendsHelper _friendsHelper = FriendsHelper();

  bool _sentRequest = false;

  int get friendID => widget.status.userID;

  void setSentRequest(bool sent) => setState(() => _sentRequest = sent);

  @override
  void didUpdateWidget(FriendshipStatusWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    setSentRequest(false);
  }

  @override
  Widget build(BuildContext context) {
    if (_sentRequest) return Container();

    // No request sent yet
    if (widget.status.noRequestExchanged) {
      return ElevatedButton(
        child: Text(tr("Send request")!.toUpperCase()),
        onPressed: () =>
            executeRequest(() => _friendsHelper.sendRequest(friendID)),
      );
    }

    // Already sent a friendship request
    if (widget.status.sentRequest) {
      return ElevatedButton(
        child: Text(
          tr("Cancel request")!.toUpperCase(),
          style: WhiteTextColorStyle,
        ),
        style:
            ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red)),
        onPressed: () =>
            executeRequest(() => _friendsHelper.cancelRequest(friendID)),
      );
    }

    // Received a friendship request
    if (widget.status.receivedRequest) {
      return Column(
        children: <Widget>[
          ElevatedButton(
            child: Text(
              tr("Accept request")!.toUpperCase(),
              style: WhiteTextColorStyle,
            ),
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.green)),
            onPressed: () => executeRequest(
                () => _friendsHelper.respondRequest(friendID, true)),
          ),
          ElevatedButton(
            child: Text(
              tr("Reject request")!.toUpperCase(),
              style: WhiteTextColorStyle,
            ),
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.red)),
            onPressed: () => executeRequest(
                () => _friendsHelper.respondRequest(friendID, false)),
          )
        ],
      );
    }

    // The two users are friends, offers to follow him
    return ElevatedButton(
      child: Text((widget.status.following ? tr("Following") : tr("Follow"))!
          .toUpperCase()),
      onPressed: () => executeRequest(() =>
          _friendsHelper.setFollowing(friendID, !widget.status.following)),
    );
  }

  /// Send friendship request
  Future<void> executeRequest(Future<bool> Function() func) async {
    setSentRequest(true);

    if (!await func())
      showSimpleSnack(context, tr("Could not update your membership!")!);

    widget.onFriendshipUpdated();
  }
}

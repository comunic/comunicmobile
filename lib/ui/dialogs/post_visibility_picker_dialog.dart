import 'package:comunic/enums/post_visibility_level.dart';
import 'package:comunic/ui/tiles/post_visibility_level_tile.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Post utilities
///
/// @author Pierre HUBERT

/// Show post visibility level picker and return selected visibility level
Future<PostVisibilityLevel> showPostVisibilityPickerDialog({
  required BuildContext context,
  required PostVisibilityLevel initialLevel,
  required bool isGroup,
}) async {
  final newLevel = await showDialog<PostVisibilityLevel>(
    context: context,
    builder: (c) => _PostVisibilityPickerWidget(
      isGroup: isGroup,
    ),
  );

  return newLevel == null ? initialLevel : newLevel;
}

class _PostVisibilityPickerWidget extends StatelessWidget {
  final bool isGroup;

  const _PostVisibilityPickerWidget({Key? key, required this.isGroup})
      : super(key: key);

  @override
  Widget build(BuildContext c) => AlertDialog(
        title: Text(tr("Select new post visibility level")!),

        // Show all options
        content: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: 200),
          child: Column(
            children: <Widget>[
              // Public
              PostVisibilityLevelTile(
                level: PostVisibilityLevel.PUBLIC,
                title: tr("Public")!,
                onSelect: (o) => Navigator.pop(c, o),
              ),

              // Friends only (User page only)
              PostVisibilityLevelTile(
                level: PostVisibilityLevel.FRIENDS,
                title: tr("Friends only")!,
                onSelect: (o) => Navigator.pop(c, o),
                visible: !isGroup,
              ),

              // User only (User page only)
              PostVisibilityLevelTile(
                level: PostVisibilityLevel.USER,
                title: tr("Me only")!,
                onSelect: (o) => Navigator.pop(c, o),
                visible: !isGroup,
              ),

              // Group members only (Group page only)
              PostVisibilityLevelTile(
                level: PostVisibilityLevel.GROUP_MEMBERS,
                title: tr("Group members only")!,
                onSelect: (o) => Navigator.pop(c, o),
                visible: isGroup,
              ),
            ],
          ),
        ),

        // Dialog actions
        actions: <Widget>[
          // Cancel
          TextButton(
            child: Text(tr("Cancel")!.toUpperCase()),
            onPressed: () => Navigator.pop(c, null),
          ),
        ],
      );
}

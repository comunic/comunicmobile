import 'package:comunic/helpers/settings_helper.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/input_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Ask the user to enter his password
///
/// @author Pierre Hubert

enum _Status { NONE, CHECKING, ERROR }

Future<String?> showUserPasswordDialog(BuildContext context) async {
  return await showDialog(
      context: context, builder: (c) => _InputUserPasswordDialog());
}

class _InputUserPasswordDialog extends StatefulWidget {
  @override
  __InputUserPasswordDialogState createState() =>
      __InputUserPasswordDialogState();
}

class __InputUserPasswordDialogState
    extends SafeState<_InputUserPasswordDialog> {
  final _controller = TextEditingController();
  var _status = _Status.NONE;

  String get _currPass => _controller.text;

  bool get _canSubmit =>
      legacyValidatePassword(_controller.text) && _status != _Status.CHECKING;

  void _setStatus(_Status s) => setState(() => _status = s);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(tr("Password required")!),
      content: _buildContent(),
      actions: <Widget>[
        MaterialButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(tr("Cancel")!.toUpperCase()),
        ),
        MaterialButton(
          onPressed: _canSubmit ? _checkPassword : null,
          child: Text(tr("Submit")!.toUpperCase()),
        ),
      ],
    );
  }

  Widget _buildContent() {
    if (_status == _Status.CHECKING)
      return IntrinsicHeight(
        child: buildCenteredProgressBar(),
      );

    return TextField(
      controller: _controller,
      obscureText: true,
      onChanged: (s) => _setStatus(_Status.NONE),
      decoration: InputDecoration(
        alignLabelWithHint: true,
        labelText: tr("Your current password"),
        errorText: _status == _Status.ERROR ? tr("Invalid password!") : null,
      ),
    );
  }

  void _checkPassword() async {
    try {
      _setStatus(_Status.CHECKING);

      await SettingsHelper.checkUserPassword(_currPass);

      Navigator.of(context).pop(_currPass);
    } catch (e, stack) {
      print("Could not validate user pasword! $e\n$stack");
      _setStatus(_Status.ERROR);
    }
  }
}

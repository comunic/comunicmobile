import 'package:comunic/ui/dialogs/single_input_dialog.dart';
import 'package:comunic/utils/input_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Ask the user to enter an URL
///
/// @author Pierre Hubert

/// Ask the user to enter an URL
Future<String?> showInputURLDialog({
  required BuildContext context,
  required String? title,
  String? initialURL,
}) async {
  return await showDialog(
    context: context,
    builder: (c) => SingleInputDialog(
      title: title!,
      icon: Icons.link,
      initialValue: initialURL,
      label: "http://...",
      checkInput: (s) => validateUrl(s),
      errorMessage: tr("Invalid URL!")!,
    ),
  );
}

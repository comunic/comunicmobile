import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

/// Deprecation dialog
///
/// @author Pierre Hubert

/// Show a dialog to warn the user this version of the application is deprecated
Future<void> showDeprecationDialog(BuildContext context) async {
  await showDialog(context: context, builder: (c) => _DeprecationDialog());
}

class _DeprecationDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(tr("Deprecated application version")!),
      content: Text(tr(
          "This version of the Comunic application is deprecated. You might still be able to use it, but some features may not work. We recommend you to update to the latest version of the application.")!),
      actions: [
        MaterialButton(
          onPressed: () =>
              launchUrlString(ServerConfigurationHelper.config!.playStoreURL),
          child: Text(tr("Go to the Play Store")!),
        ),
        MaterialButton(
          onPressed: () => launchUrlString(
              ServerConfigurationHelper.config!.androidDirectDownloadURL),
          child: Text(tr("Download update outside Play Store")!),
        ),
        MaterialButton(
          onPressed: () => Navigator.pop(context),
          child: Text(tr("Use the old application anyway")!),
        )
      ],
    );
  }
}

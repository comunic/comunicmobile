import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Dialog to use to ask the user to enter a value
///
/// @author Pierre Hubert

class SingleInputDialog extends StatefulWidget {
  final String title;
  final IconData? icon;
  final String? initialValue;
  final String label;
  final bool Function(String) checkInput;
  final String errorMessage;
  final bool canBeEmpty;
  final int maxLines;
  final int? maxLength;

  const SingleInputDialog({
    Key? key,
    required this.title,
    required this.icon,
    required this.initialValue,
    required this.label,
    required this.checkInput,
    required this.errorMessage,
    this.canBeEmpty = false,
    this.maxLines = 1,
    this.maxLength,
  }) : super(key: key);

  @override
  __InputURLDialogState createState() => __InputURLDialogState();
}

class __InputURLDialogState extends State<SingleInputDialog> {
  TextEditingController? _controller;

  bool get _isValid =>
      (_controller!.text.isEmpty && widget.canBeEmpty) ||
      (_controller!.text.isNotEmpty && widget.checkInput(_controller!.text));

  @override
  void initState() {
    super.initState();
    _controller = new TextEditingController(text: widget.initialValue);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title),
      content: TextField(
        controller: _controller,
        onChanged: (s) => setState(() {}),
        maxLines: widget.maxLines,
        maxLength: widget.maxLength,
        decoration: InputDecoration(
          icon: widget.icon == null ? null : Icon(widget.icon),
          alignLabelWithHint: true,
          labelText: widget.label,
          errorText: _controller!.text.isNotEmpty && !_isValid
              ? widget.errorMessage
              : null,
        ),
      ),
      actions: <Widget>[
        // Cancel
        MaterialButton(
          child: Text(tr("Cancel")!.toUpperCase()),
          onPressed: () => Navigator.of(context).pop(),
          textColor: Colors.red,
        ),

        // Confirm
        MaterialButton(
          child: Text(tr("Confirm")!.toUpperCase()),
          onPressed: _isValid
              ? () => Navigator.of(context).pop(_controller!.text)
              : null,
        ),
      ],
    );
  }
}

import 'package:comunic/helpers/report_helper.dart';
import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/models/report_target.dart';
import 'package:comunic/models/server_config.dart';
import 'package:comunic/ui/dialogs/alert_dialog.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/widgets/comunic_back_button_widget.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

enum _Status { ChooseCause, GiveComment, Sending }

/// Show a dialog to report some content
Future<void> showReportDialog({
  required BuildContext ctx,
  required ReportTarget target,
}) async {
  MainController.of(ctx)!.push(
    _ReportDialog(target: target),
    canShowAsDialog: true,
    hideNavBar: true,
  );
}

class _ReportDialog extends StatefulWidget {
  final ReportTarget target;

  const _ReportDialog({Key? key, required this.target}) : super(key: key);

  @override
  State<_ReportDialog> createState() => _ReportDialogState();
}

class _ReportDialogState extends SafeState<_ReportDialog> {
  var _cause = srvConfig!.reportPolicy!.causes.length - 1;
  var _status = _Status.ChooseCause;
  final _commentController = TextEditingController();

  List<ReportCause> get _causes => srvConfig!.reportPolicy!.causes;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: ComunicBackButton(),
          title: Text(tr("Report abuse")!),
          actions: _status == _Status.Sending
              ? []
              : [
                  IconButton(
                    onPressed: _goNextStep,
                    icon: Icon(Icons.check),
                  ),
                ]),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: _buildContent(),
      ),
    );
  }

  Widget _buildContent() {
    if (_status == _Status.Sending)
      return Center(child: CircularProgressIndicator());

    if (_status == _Status.ChooseCause)
      return Column(
        children: [
          Text(tr("Please choose the reason of your report:")!),
          SizedBox(height: 15),
          Expanded(
            child: ListView.builder(
              itemBuilder: (c, i) => RadioListTile(
                  dense: false,
                  title: Text(_causes[i].localeLabel),
                  value: i,
                  groupValue: _cause,
                  onChanged: (_i) => setState(() => _cause = i)),
              itemCount: _causes.length,
            ),
          ),
        ],
      );

    if (_status == _Status.GiveComment) {
      return Column(
        children: [
          Text("You can optionally describe the reason of your report:"),
          SizedBox(height: 15),
          TextField(
            controller: _commentController,
            decoration: InputDecoration(hintText: tr("Reason of report")),
            minLines: 5,
            maxLines: 5,
            maxLength: srvConfig!.reportPolicy!.maxCommentLength,
          )
        ],
      );
    }

    throw Exception("Unknown status!");
  }

  void _goNextStep() async {
    if (_status == _Status.ChooseCause) {
      setState(() => _status = _Status.GiveComment);
      return;
    }

    // Send report
    try {
      setState(() => _status = _Status.Sending);

      final result = await ReportHelper.sendReport(
        cause: _causes[_cause],
        target: widget.target,
        comment: _commentController.value.text,
      );

      // In case of success
      if (result == ReportResult.Success) {
        await alert(context,
            tr("Report successfully saved. Thank you for your contribution!"));
        MainController.of(context)!.popPage();
      } else if (result == ReportResult.ErrorAlreadyReported) {
        await alert(
            context, tr("You have already sent a report for this resource!"));
        MainController.of(context)!.popPage();
      } else {
        await alert(context, tr("Failed to send report!"));
      }
    } catch (e, s) {
      print("$e $s");
      alert(context, tr("Failed to send report!"));
    } finally {
      setState(() => _status = _Status.GiveComment);
    }
  }
}

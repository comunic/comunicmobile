import 'package:comunic/models/new_post.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Create a new survey dialog
///
/// @author Pierre Hubert

/// Show a dialog to prompt information about a new survey to create
Future<NewSurvey?> showNewSurveyDialog(
    {required BuildContext context, NewSurvey? initialSurvey}) async {
  return await showDialog<NewSurvey>(
      context: context,
      builder: (c) => _SurveyDialog(
            initialSurvey: initialSurvey,
          ));
}

class _SurveyDialog extends StatefulWidget {
  final NewSurvey? initialSurvey;

  const _SurveyDialog({Key? key, this.initialSurvey}) : super(key: key);

  @override
  __SurveyDialogState createState() => __SurveyDialogState();
}

class __SurveyDialogState extends State<_SurveyDialog> {
  TextEditingController? _questionController;
  final _choiceController = TextEditingController();
  var _choices = Set<String>();
  var _allowNewChoices = false;

  bool get canConfirm =>
      _questionController!.text.length > 2 && _choices.length > 1;

  NewSurvey get newSurvey => NewSurvey(
      question: _questionController!.text,
      answers: _choices,
      allowNewChoicesCreation: _allowNewChoices);

  @override
  void initState() {
    super.initState();
    _questionController = TextEditingController(
      text: widget.initialSurvey == null ? "" : widget.initialSurvey!.question,
    );
    if (widget.initialSurvey != null) {
      _choices = widget.initialSurvey!.answers;
      _allowNewChoices = widget.initialSurvey!.allowNewChoicesCreation;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(tr("New survey")!),
      content: _buildBody(),
      actions: <Widget>[
        // Cancel
        MaterialButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(tr("Cancel")!.toUpperCase()),
        ),

        // Confirm
        MaterialButton(
          onPressed:
              canConfirm ? () => Navigator.of(context).pop(newSurvey) : null,
          child: Text(tr("Confirm")!.toUpperCase()),
        ),
      ],
    );
  }

  Widget _buildBody() {
    return ConstrainedBox(
      constraints:
          BoxConstraints(maxHeight: MediaQuery.of(context).size.height - 50),
      child: SingleChildScrollView(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              // Survey question
              TextField(
                controller: _questionController,
                onChanged: (s) => setState(() {}),
                decoration: InputDecoration(
                    alignLabelWithHint: true, labelText: tr("Question")),
              ),

              Container(height: 30),
              Text(tr("Current choices:")!),
              _buildCurrentChoices(),

              Container(
                height: 10,
              ),

              // Add choice form
              TextField(
                controller: _choiceController,
                onChanged: (s) => setState(() {}),
                onSubmitted: (s) => _addChoice(),
                decoration: InputDecoration(
                    hintText: tr("New choice..."),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.add),
                      onPressed: () => _choiceController.text.length > 0
                          ? _addChoice()
                          : null,
                    )),
              ),

              // Allow users to create new choices
              ListTile(
                leading: Switch.adaptive(
                  value: _allowNewChoices,
                  onChanged: (v) => setState(() => _allowNewChoices = v),
                ),
                title: Text(tr("Allow users to create new choices")!),
              )
            ]),
      ),
    );
  }

  Widget _buildCurrentChoices() {
    if (_choices.length == 0) return Text(tr("No choice yet.")!);

    return Column(
      children: _choices
          .map((f) => ListTile(
              title: Text(f),
              dense: true,
              trailing: IconButton(
                icon: Icon(Icons.delete),
                iconSize: 15,
                onPressed: () => setState(() => _choices.remove(f)),
              )))
          .toList(),
    );
  }

  void _addChoice() {
    if (_choiceController.text.length == 0) return;
    setState(() {
      _choices.add(_choiceController.text);
      _choiceController.clear();
    });
  }
}

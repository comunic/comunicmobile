import 'package:comunic/ui/dialogs/single_input_dialog.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Add YouTube link dialog
///
/// @author Pierre Hubert

/// Ask the user to input a YouTube ID
Future<String?> showInputYouTubeIDDialog(
    BuildContext context, String? initialID) async {
  final value = await showDialog<String>(
      context: context,
      builder: (b) => SingleInputDialog(
            title: tr("Input YouTube URL")!,
            icon: Icons.ondemand_video,
            initialValue: initialID == null
                ? null
                : "https://www.youtube.com/watch/?v=" + initialID,
            label: tr("https://www.youtube.com/watch/?v=")!,
            checkInput: (s) => RegExp(r'watch\/\?v=[\w\-\_]+').hasMatch(s),
            errorMessage: tr("Invalid YouTube link!")!,
          ));

  if (value == null) return null;

  return value.split("v=")[1].split("&")[0].split("#")[0];
}

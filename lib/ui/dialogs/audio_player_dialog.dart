import 'package:chewie_audio/chewie_audio.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

/// Audio player dialog
///
/// @author Pierre Hubert

/// Show audio player dialog
Future<void> showAudioPlayerDialog(BuildContext context, String? url) async {
  showDialog(context: context, builder: (c) => _AudioPlayerDialog(url: url!));
}

class _AudioPlayerDialog extends StatefulWidget {
  final String url;

  const _AudioPlayerDialog({
    Key? key,
    required this.url,
  })  : super(key: key);

  @override
  __AudioPlayerDialogState createState() => __AudioPlayerDialogState();
}

class __AudioPlayerDialogState extends State<_AudioPlayerDialog> {
  VideoPlayerController? _videoPlayerController;
  ChewieAudioController? _chewieAudioController;

  Future<void> _initialize() async {
    _videoPlayerController = VideoPlayerController.network(widget.url);

    await _videoPlayerController!.initialize();

    _chewieAudioController = ChewieAudioController(
      videoPlayerController: _videoPlayerController!,
      autoPlay: true,
      looping: false,
    );
  }

  void _closeDialog() {
    Navigator.pop(context);
  }

  @override
  void dispose() {
    if (_videoPlayerController != null) _videoPlayerController!.dispose();
    if (_chewieAudioController != null) _chewieAudioController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(tr("Audio Player")!),
      content: _buildContent(),
      actions: [
        MaterialButton(
          onPressed: _closeDialog,
          child: Text(tr("Close")!.toUpperCase()),
        )
      ],
    );
  }

  Widget _buildContent() => ConstrainedBox(
        constraints: BoxConstraints(maxHeight: 50, maxWidth: 500),
        child: AsyncScreenWidget(
          onReload: _initialize,
          onBuild: _buildReadyContent,
          errorMessage: tr("Failed to initialize audio player!")!,
        ),
      );

  Widget _buildReadyContent() =>
      ChewieAudio(controller: _chewieAudioController!);
}

import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

/// Color picker dialog
///
/// @author Pierre Hubert

Future<Color?> showColorPickerDialog(
        BuildContext context, Color? initialColor) async =>
    await showDialog(
      context: context,
      builder: (c) => _ColorPickerDialog(initialColor: initialColor),
    );

class _ColorPickerDialog extends StatefulWidget {
  final Color? initialColor;

  const _ColorPickerDialog({Key? key, required this.initialColor})
      : super(key: key);

  @override
  __ColorPickerDialogState createState() => __ColorPickerDialogState();
}

class __ColorPickerDialogState extends State<_ColorPickerDialog> {
  Color? _newColor;

  @override
  void initState() {
    _newColor = widget.initialColor;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: MaterialPicker(
        pickerColor: _newColor ?? Colors.blue.shade900,
        onColorChanged: (c) => setState(() => _newColor = c),
      ),
      actions: [
        MaterialButton(
          onPressed: () => Navigator.pop(context, widget.initialColor),
          child: Text(tr("Cancel")!.toUpperCase()),
        ),
        MaterialButton(
          onPressed: () => Navigator.pop(context, _newColor),
          child: Text(tr("Ok")!.toUpperCase()),
        ),
      ],
    );
  }
}

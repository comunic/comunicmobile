import 'package:flutter/material.dart';

/// Simple alert dialog
///
/// @author Pierre Hubert

Future<void> alert(BuildContext context, String? msg) async {
  await showDialog(context: context, builder: (c) => _AlertDialog(msg: msg!));
}

class _AlertDialog extends StatelessWidget {
  final String msg;

  const _AlertDialog({Key? key, required this.msg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Text(msg),
      actions: <Widget>[
        MaterialButton(
          child: Text("OK"),
          onPressed: () => Navigator.of(context).pop(),
        )
      ],
    );
  }
}

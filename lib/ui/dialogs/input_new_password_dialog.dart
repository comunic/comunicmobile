import 'package:comunic/ui/widgets/dialogs/auto_sized_dialog_content_widget.dart';
import 'package:comunic/ui/widgets/new_password_input_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Ask the user to enter a new password
///
/// @author Pierre HUBERT

/// Ask the user to enter a new password
Future<String?> showInputNewPassword({
  required BuildContext context,
  required UserInfoForPassword userInfo,
}) async {
  return await showDialog(
      context: context,
      builder: (c) => _InputNewPasswordDialog(
            userInfo: userInfo,
          ));
}

class _InputNewPasswordDialog extends StatefulWidget {
  final UserInfoForPassword userInfo;

  const _InputNewPasswordDialog({
    Key? key,
    required this.userInfo,
  })  : super(key: key);

  @override
  __InputNewPasswordDialogState createState() =>
      __InputNewPasswordDialogState();
}

class __InputNewPasswordDialogState extends State<_InputNewPasswordDialog> {
  final _controller1 = GlobalKey<NewPasswordInputWidgetState>();
  final _controller2 = TextEditingController();
  final _focusScopeNode = FocusScopeNode();

  String get _password => _controller1.currentState!.value;

  bool get _input1Valid =>
      _controller1.currentState != null && _controller1.currentState!.valid;

  bool get _input2Valid =>
      _controller1.currentState!.value == _controller2.text;

  bool get _isValid => _input1Valid && _input2Valid;

  void Function()? get _submitAction =>
      _isValid ? () => Navigator.of(context).pop(_password) : null;

  @override
  void dispose() {
    _focusScopeNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(tr("New password")!),
      content: AutoSizeDialogContentWidget(child: _buildContent()),
      actions: <Widget>[
        MaterialButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(tr("Cancel")!.toUpperCase()),
        ),
        MaterialButton(
          onPressed: _submitAction,
          child: Text(tr("Confirm")!.toUpperCase()),
        ),
      ],
    );
  }

  Widget _buildContent() {
    return FocusScope(
      node: _focusScopeNode,
      child: Column(
        children: <Widget>[
          // Input 1
          NewPasswordInputWidget(
            key: _controller1,
            user: widget.userInfo,
            label: tr("Your new password")!,
            textInputAction: TextInputAction.next,
            onSubmitted: () => _focusScopeNode.nextFocus(),
          ),

          // Input 2
          _buildPasswordField(
              controller: _controller2,
              label: tr("Confirm you new password"),
              errorText: _controller2.text.isNotEmpty && !_input2Valid
                  ? tr("This password is not the same as the other one!")
                  : null,
              textInputAction: TextInputAction.done,
              onSubmitted: _submitAction),
        ],
      ),
    );
  }

  Widget _buildPasswordField({
    required TextEditingController controller,
    required String? label,
    required String? errorText,
    required TextInputAction textInputAction,
    required void Function()? onSubmitted,
  }) {
    return TextFormField(
      textInputAction: textInputAction,
      controller: controller,
      onChanged: (s) => setState(() {}),
      onFieldSubmitted: (s) => onSubmitted!(),
      obscureText: true,
      decoration: InputDecoration(
        alignLabelWithHint: true,
        labelText: label,
        errorText: errorText,
      ),
    );
  }
}

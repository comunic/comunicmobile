import 'package:comunic/models/user.dart';
import 'package:comunic/ui/widgets/dialogs/cancel_dialog_button.dart';
import 'package:comunic/ui/widgets/dialogs/confirm_dialog_button.dart';
import 'package:comunic/ui/widgets/pick_user_widget.dart';
import 'package:comunic/ui/widgets/safe_state.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Pick user dialog
///
/// @author Pierre HUBERT

/// Ask the user to pick a user
///
/// Returns null if no user was selected
Future<int?> showPickUserDialog(BuildContext context) async {
  return await showDialog(context: context, builder: (c) => _PickUserDialog());
}

class _PickUserDialog extends StatefulWidget {
  @override
  __PickUserDialogState createState() => __PickUserDialogState();
}

class __PickUserDialogState extends SafeState<_PickUserDialog> {
  User? _user;

  bool get _isValid => _user != null;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(tr("Choose a user")!),
      content: _buildContent(),
      actions: <Widget>[
        CancelDialogButton(),
        ConfirmDialogButton(
          value: _isValid ? _user!.id : null,
          enabled: _isValid,
        ),
      ],
    );
  }

  Widget _buildContent() {
    final size = MediaQuery.of(context).size;
    print(size);
    return Container(
      width: size.width,
      height: size.height,
      child: Column(
        children: <Widget>[
          PickUserWidget(
            onSelectUser: (u) => setState(() => _user = u),
            label: tr("Search user...")!,
            onValueChange: (u) => setState(() => _user = null),
          ),
        ],
      ),
    );
  }
}

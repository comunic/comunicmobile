import 'package:comunic/ui/widgets/dialogs/auto_sized_dialog_content_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Build and show a dialog to offer to the user to choose between several
/// options
///
/// @author Pierre HUBERT

class MultiChoiceEntry<T> {
  final T id;
  final String title;
  final String? subtitle;
  final bool hidden;

  const MultiChoiceEntry({
    required this.id,
    required this.title,
    this.subtitle,
    this.hidden = false,
  }) : assert(id != null);

  bool get hasSubtitle => subtitle != null;
}

/// Show multiple choices dialog
Future<T?> showMultiChoicesDialog<T>({
  required BuildContext context,
  required List<MultiChoiceEntry<T>> choices,
  required T defaultChoice,
  required String? title,
}) async {
  return await showDialog<T>(
      context: context,
      builder: (c) => _MultiChoicesEntryDialog(
            title: title!,
            defaultChoice: defaultChoice,
            choices: choices,
          ));
}

class _MultiChoicesEntryDialog<T> extends StatefulWidget {
  final String title;
  final T defaultChoice;
  final List<MultiChoiceEntry<T>> choices;

  const _MultiChoicesEntryDialog({
    Key? key,
    required this.title,
    required this.defaultChoice,
    required this.choices,
  })  : assert(defaultChoice != null),
        super(key: key);

  @override
  __MultiChoicesEntryDialogState createState() =>
      __MultiChoicesEntryDialogState();
}

class __MultiChoicesEntryDialogState<T>
    extends State<_MultiChoicesEntryDialog<T>> {
  T? _currChoice;

  @override
  void initState() {
    super.initState();
    _currChoice = widget.defaultChoice;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title),
      content: AutoSizeDialogContentWidget(
        child: Column(
          children: widget.choices
              .where((f) => !f.hidden)
              .map((f) => ListTile(
                    leading: Radio<T>(
                      groupValue: _currChoice,
                      value: f.id,
                      onChanged: (v) => setState(() => _currChoice = v),
                    ),
                    title: Text(f.title),
                    subtitle: f.hasSubtitle ? Text(f.subtitle!) : null,
                  ))
              .toList(),
        ),
      ),
      actions: <Widget>[
        MaterialButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(tr("Cancel")!.toUpperCase()),
          textColor: Colors.red,
        ),
        MaterialButton(
          onPressed: () => Navigator.of(context).pop(_currChoice),
          child: Text(tr("Confirm")!.toUpperCase()),
          textColor: Colors.green,
        ),
      ],
    );
  }
}

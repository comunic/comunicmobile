import 'dart:math';

import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Screen dialog
///
/// @author Pierre Hubert

/// Show a screen dialog
///
/// This widget automatically adapt himself if we are in tablet
/// or in mobile mode
Future<T?> showScreenDialog<T>(BuildContext context, Widget screen) async {
  // TODO : add mobile support
  if (!isTablet(context)) throw Exception("Unsupported mode!");

  return await showDialog(
      context: context, builder: (c) => _ScreenDialog(body: screen));
}

class _ScreenDialog extends StatelessWidget {
  final Widget? body;

  const _ScreenDialog({Key? key, this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = min(500.0, size.width - 50);
    final height = min(800.0, size.height - 50);

    return AlertDialog(
      contentPadding: EdgeInsets.all(0.0),
      content: Container(
        width: width,
        height: height,
        child: body,
      ),
    );
  }
}

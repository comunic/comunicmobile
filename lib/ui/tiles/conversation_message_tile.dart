import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/models/conversation_message.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/widgets/conversation_file_tile.dart';
import 'package:comunic/ui/widgets/text_widget.dart';
import 'package:comunic/utils/clipboard_utils.dart';
import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Conversation message tile
///
/// @author Pierre HUBERT

enum _MenuChoices {
  COPY_URL,
  COPY_MESSAGE,
  DELETE,
  REQUEST_UPDATE_CONTENT,
  GET_STATS,
  REPORT
}

typedef OnRequestMessageStats = void Function(ConversationMessage);
typedef OnRequestMessageUpdate = void Function(ConversationMessage);
typedef OnRequestMessageDelete = void Function(ConversationMessage);

class ConversationMessageTile extends StatelessWidget {
  final ConversationMessage message;
  final User user;
  final OnRequestMessageStats onRequestMessageStats;
  final OnRequestMessageUpdate onRequestMessageUpdate;
  final OnRequestMessageDelete onRequestMessageDelete;
  final Function(ConversationMessage) onReportMessage;

  const ConversationMessageTile({
    Key? key,
    required this.message,
    required this.user,
    required this.onRequestMessageStats,
    required this.onRequestMessageUpdate,
    required this.onRequestMessageDelete,
    required this.onReportMessage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          RichText(
            text: TextSpan(
                text:
                    "${user.fullName} - ${formatDisplayDate(message.date, date: false)}",
                style: TextStyle(color: Colors.white, fontSize: 11),
                children: [
                  WidgetSpan(
                    child: PopupMenuButton<_MenuChoices>(
                      child: Icon(
                        Icons.more_vert,
                        color: Colors.white,
                        size: 14,
                      ),
                      onSelected: (v) => _menuOptionSelected(context, v),
                      itemBuilder: (c) => <PopupMenuItem<_MenuChoices>>[
                        PopupMenuItem(
                          enabled: (message.message.content ?? "") != "",
                          value: _MenuChoices.COPY_MESSAGE,
                          child: Text(tr("Copy message")!),
                        ),

                        PopupMenuItem(
                          enabled: message.file != null,
                          value: _MenuChoices.COPY_URL,
                          child: Text(tr("Copy URL")!),
                        ),

                        PopupMenuItem(
                          value: _MenuChoices.GET_STATS,
                          child: Text(tr("Statistics")!),
                        ),

                        // Update message content
                        PopupMenuItem(
                          enabled: message.isOwner &&
                              !message.message.isNull &&
                              message.message.content!.isNotEmpty,
                          value: _MenuChoices.REQUEST_UPDATE_CONTENT,
                          child: Text(tr("Update")!),
                        ),

                        // Delete the message
                        PopupMenuItem(
                          enabled: message.isOwner,
                          value: _MenuChoices.DELETE,
                          child: Text(tr("Delete")!),
                        ),

                        // Report the message
                        PopupMenuItem(
                          enabled: srvConfig!.isReportingEnabled &&
                              (!message.isOwner ||
                                  srvConfig!.reportPolicy!
                                      .canUserReportHisOwnContent),
                          value: _MenuChoices.REPORT,
                          child: Text(tr("Report abuse")!),
                        ),
                      ]..removeWhere((element) => !element.enabled),
                    ),
                  )
                ]),
          ),
          _buildMessageContent(),
        ],
      );

  Widget _buildMessageContent() {
    if (!message.hasFile)
      return TextWidget(
        content: message.message,
        textAlign: TextAlign.justify,
        style: TextStyle(color: Colors.white),
        linksColor: Colors.white,
      );

    return ConversationFileWidget(messageID: message.id!, file: message.file!);
  }

  /// Process menu choice
  void _menuOptionSelected(BuildContext context, _MenuChoices value) {
    switch (value) {
      case _MenuChoices.COPY_MESSAGE:
        copyToClipboard(context, message.message.content!);
        break;

      case _MenuChoices.COPY_URL:
        copyToClipboard(context, message.file!.url!);
        break;

      case _MenuChoices.GET_STATS:
        onRequestMessageStats(message);
        break;

      case _MenuChoices.REQUEST_UPDATE_CONTENT:
        onRequestMessageUpdate(message);
        break;

      case _MenuChoices.DELETE:
        onRequestMessageDelete(message);
        break;

      case _MenuChoices.REPORT:
        onReportMessage(message);
        break;
    }
  }
}

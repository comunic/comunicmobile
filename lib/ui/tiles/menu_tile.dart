import 'package:flutter/material.dart';

/// Menu tile
///
/// @author Pierre HUBERT

class MenuTile extends StatelessWidget {
  final String title;
  final GestureTapCallback? onTap;

  const MenuTile({required this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Divider(),
        InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(title),
          ),
        )
      ],
    );
  }
}

import 'package:comunic/models/user.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:flutter/material.dart';

/// Simple user tile
///
/// Basically this only shows the name of the user + its account image
///
/// @author Pierre HUBERT

typedef OnUserTap = void Function(User);

class SimpleUserTile extends StatelessWidget {
  final User user;
  final OnUserTap? onTap;
  final Widget? trailing;
  final String? subtitle;

  const SimpleUserTile({
    Key? key,
    required this.user,
    this.onTap,
    this.trailing,
    this.subtitle,
  })  : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap == null ? null : () => onTap!(user),
      leading: AccountImageWidget(
        user: user,
      ),
      title: Text(user.fullName),
      subtitle: subtitle == null ? null : Text(subtitle!),
      trailing: trailing,
    );
  }
}

import 'package:comunic/enums/post_visibility_level.dart';
import 'package:flutter/material.dart';

/// Post visibility level tile
///
/// @author Pierre HUBERT

class PostVisibilityLevelTile extends StatelessWidget {
  final PostVisibilityLevel level;
  final String title;
  final void Function(PostVisibilityLevel) onSelect;
  final bool visible;

  const PostVisibilityLevelTile({
    Key? key,
    required this.level,
    required this.title,
    required this.onSelect,
    this.visible = true,
  })  : super(key: key);

  @override
  Widget build(BuildContext context) {

    if(!visible)
      return Container();

    return ListTile(
      leading: Icon(PostVisibilityLevelsMapIcons[level]),
      title: Text(title),
      onTap: () => onSelect(level),
    );
  }
}

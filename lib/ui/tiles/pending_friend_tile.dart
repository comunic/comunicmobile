import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/models/friend.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/tiles/accepted_friend_tile.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Pending friend tile
///
/// @author Pierre HUBERT

typedef RespondFriendshipRequestCallback = void Function(Friend, bool);

class PendingFriendTile extends StatelessWidget {
  final Friend friend;
  final User user;
  final RespondFriendshipRequestCallback onRespond;
  final OnReportFriend onReport;

  const PendingFriendTile({
    Key? key,
    required this.friend,
    required this.user,
    required this.onRespond,
    required this.onReport,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: AccountImageWidget(
        user: user,
      ),
      isThreeLine: true,
      title: Text(user.fullName),
      subtitle: Container(
        height: 30.0,
        margin: EdgeInsets.only(top: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            ElevatedButton(
              child: Text(
                tr("Accept")!.toUpperCase(),
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.green)),
              onPressed: () => onRespond(friend, true),
            ),
            Container(
              width: 8.0,
            ),
            ElevatedButton(
              child: Text(
                tr("Reject")!.toUpperCase(),
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.red)),
              onPressed: () => onRespond(friend, false),
            ),

            // Report button
            srvConfig!.isReportingEnabled
                ? IconButton(
                    visualDensity: VisualDensity.compact,
                    onPressed: () => onReport(friend),
                    icon: Icon(
                      Icons.flag,
                      size: 15.0,
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}

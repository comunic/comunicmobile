import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/conversation_message.dart';
import 'package:flutter/material.dart';

/// Server conversation message list
///
/// @author Pierre hubert

class ServerConversationMessageTile extends StatelessWidget {
  final ConversationServerMessage message;
  final UsersList users;

  const ServerConversationMessageTile({
    Key? key,
    required this.message,
    required this.users,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        message.getText(users)!,
        style: TextStyle(
          fontStyle: FontStyle.italic,
          fontSize: 12,
        ),
      ),
    );
  }
}

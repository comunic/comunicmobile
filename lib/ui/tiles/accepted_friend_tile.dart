import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/models/friend.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/navigation_utils.dart';
import 'package:flutter/material.dart';

/// Accepted friend tile
///
/// @author Pierre HUBERT

enum _FriendMenuChoices {
  REMOVE,
  TOGGLE_FOLLOWING,
  PRIVATE_CONVERSATION,
  REPORT
}

typedef OnRequestDeleteFriend = void Function(Friend);
typedef OnSetFollowing = void Function(Friend, bool);
typedef OnOpenPrivateConversation = void Function(Friend);
typedef OnReportFriend = void Function(Friend);

class AcceptedFriendTile extends StatelessWidget {
  final Friend friend;
  final User user;
  final OnRequestDeleteFriend onRequestDelete;
  final OnSetFollowing onSetFollowing;
  final OnOpenPrivateConversation onOpenPrivateConversation;
  final OnReportFriend onReportFriend;

  const AcceptedFriendTile({
    Key? key,
    required this.friend,
    required this.user,
    required this.onRequestDelete,
    required this.onSetFollowing,
    required this.onOpenPrivateConversation,
    required this.onReportFriend,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => openUserPage(context: context, userID: user.id),
      leading: AccountImageWidget(user: user),
      title: Text(user.displayName),
      subtitle: friend.isConnected
          ? Text(
              tr(
                "Online",
              )!,
              style: TextStyle(color: Colors.green),
            )
          : Text(
              diffTimeFromNowToStr(friend.lastActive!)!,
            ),
      trailing: PopupMenuButton<_FriendMenuChoices>(
        itemBuilder: (c) => [
          //Open a private conversation
          PopupMenuItem(
            child: Text(tr("Private conversation")!),
            value: _FriendMenuChoices.PRIVATE_CONVERSATION,
          ),

          // Update following status
          PopupMenuItem(
            child: Row(
              children: <Widget>[
                Icon(friend.following
                    ? Icons.check_box
                    : Icons.check_box_outline_blank),
                Container(width: 10.0),
                Text(friend.following ? tr("Following")! : tr("Follow")!),
              ],
            ),
            value: _FriendMenuChoices.TOGGLE_FOLLOWING,
          ),

          // Remove the friend from the list
          PopupMenuItem(
            child: Text(tr("Remove")!),
            value: _FriendMenuChoices.REMOVE,
          ),
        ]..addAll(srvConfig!.isReportingEnabled
            ? [
                // Report user
                PopupMenuItem(
                  child: Text(tr("Report abuse")!),
                  value: _FriendMenuChoices.REPORT,
                ),
              ]
            : []),
        onSelected: _selectedMenuOption,
      ),
    );
  }

  void _selectedMenuOption(_FriendMenuChoices value) {
    switch (value) {
      // Open private conversation
      case _FriendMenuChoices.PRIVATE_CONVERSATION:
        onOpenPrivateConversation(friend);
        break;

      //Toggle following status
      case _FriendMenuChoices.TOGGLE_FOLLOWING:
        onSetFollowing(friend, !friend.following);
        break;

      //Delete friend
      case _FriendMenuChoices.REMOVE:
        onRequestDelete(friend);
        break;

      case _FriendMenuChoices.REPORT:
        onReportFriend(friend);
        break;
    }
  }
}

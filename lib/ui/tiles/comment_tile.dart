import 'package:comunic/helpers/server_config_helper.dart';
import 'package:comunic/models/comment.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/ui/widgets/like_widget.dart';
import 'package:comunic/ui/widgets/network_image_widget.dart';
import 'package:comunic/ui/widgets/text_widget.dart';
import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Single comment tile
///
/// @author Pierre HUBERT

enum _CommentAction { DELETE, UPDATE, REPORT }

class CommentTile extends StatelessWidget {
  final Comment comment;
  final User user;
  final void Function(Comment) onUpdateComment;
  final void Function(Comment) onDeleteComment;
  final void Function(Comment) onReportComment;

  const CommentTile({
    Key? key,
    required this.comment,
    required this.user,
    required this.onUpdateComment,
    required this.onDeleteComment,
    required this.onReportComment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: _buildAccountImageWidget(),
      title: Text(
        user.displayName,
      ),
      subtitle: _buildCommentContent(),
      trailing: _buildTrailing(),
    );
  }

  Widget _buildAccountImageWidget() {
    return AccountImageWidget(
      user: user,
    );
  }

  Widget _buildCommentContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        // Comment image
        Container(
          child: comment.hasImage
              ? NetworkImageWidget(
                  url: comment.imageURL!,
                  allowFullScreen: true,
                  height: 100.0,
                  width: null,
                )
              : null,
        ),

        // Comment text
        Container(
          child: comment.hasContent
              ? TextWidget(
                  content: comment.content,
                  style: TextStyle(
                      color: darkTheme() ? darkAccentColor : Colors.black),
                )
              : null,
        ),

        // Comment likes
        LikeWidget(likeElement: comment),
      ],
    );
  }

  Widget _buildTrailing() {
    return IntrinsicWidth(
      child: Row(
        children: [
          Text(
            diffTimeFromNowToStr(comment.timeSent)!,
            style: TextStyle(fontSize: 10.0),
          ),
          SizedBox(width: 5),
          PopupMenuButton<_CommentAction>(
            padding: EdgeInsets.all(0),
            child: InkWell(
              child: Icon(Icons.adaptive.more, size: 20),
            ),
            onSelected: _selectedMenuOption,
            itemBuilder: (c) => [
              // Update comment content
              PopupMenuItem(
                enabled: comment.isOwner,
                child: Text(tr("Update")!),
                value: _CommentAction.UPDATE,
              ),

              // Delete comment
              PopupMenuItem(
                enabled: comment.isOwner,
                child: Text(tr("Delete")!),
                value: _CommentAction.DELETE,
              ),
            ]..addAll(srvConfig!.isReportingEnabled &&
                    (!comment.isOwner ||
                        srvConfig!.reportPolicy!.canUserReportHisOwnContent)
                ? [
                    PopupMenuItem(
                      child: Text(tr("Report abuse")!),
                      value: _CommentAction.REPORT,
                    )
                  ]
                : []),
          )
        ],
      ),
    );
  }

  /// A menu option has been selected
  void _selectedMenuOption(_CommentAction value) {
    switch (value) {
      // Update comment content
      case _CommentAction.UPDATE:
        onUpdateComment(comment);
        break;

      // Delete comment
      case _CommentAction.DELETE:
        onDeleteComment(comment);
        break;

      // Report comment
      case _CommentAction.REPORT:
        onReportComment(comment);
        break;
    }
  }
}

import 'dart:collection';

import 'package:comunic/helpers/serialization/base_serialization_helper.dart';
import 'package:comunic/lists/conversation_messages_list.dart';
import 'package:comunic/models/conversation_message.dart';

/// Conversations serialization helper
///
/// @author Pierre Hubert

HashMap<int?, ConversationsMessagesSerializationHelper>? _instances;

class ConversationsMessagesSerializationHelper
    extends BaseSerializationHelper<ConversationMessage> {
  final int convID;

  ConversationsMessagesSerializationHelper._(int convID)
      : convID = convID;

  factory ConversationsMessagesSerializationHelper(int? convID) {
    if (_instances == null) _instances = HashMap();

    if (!_instances!.containsKey(convID))
      _instances![convID] = ConversationsMessagesSerializationHelper._(convID!);

    return _instances![convID]!;
  }

  @override
  ConversationMessage parse(Map<String, dynamic> m) =>
      ConversationMessage.fromJson(m);

  @override
  String get type => "conv-messages-$convID";

  Future<ConversationMessagesList> getList() async =>
      ConversationMessagesList()..addAll(await super.getList());

  Future<void> insertOrReplace(ConversationMessage msg) async =>
      await insertOrReplaceElement((t) => t.id == msg.id, msg);

  Future<void> remove(ConversationMessage msg) async =>
      await removeElement((t) => t.id == msg.id);

  /// Insert or replace a list of messages
  Future<void> insertOrReplaceAll(List<ConversationMessage> list) async {
    for (var message in list)
      await insertOrReplaceElement((t) => t.id == message.id, message);
  }
}

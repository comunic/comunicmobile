import 'package:comunic/helpers/serialization/base_serialization_helper.dart';
import 'package:comunic/lists/conversations_list.dart';
import 'package:comunic/models/conversation.dart';

/// Conversations serialization helper
///
/// @author Pierre Hubert

var _cache;

class ConversationsSerializationHelper
    extends BaseSerializationHelper<Conversation> {
  /// Singleton
  factory ConversationsSerializationHelper() {
    if (_cache == null) _cache = ConversationsSerializationHelper._();
    return _cache;
  }

  ConversationsSerializationHelper._();

  @override
  Conversation parse(Map<String, dynamic> m) => Conversation.fromJson(m);

  @override
  String get type => "conversations";

  Future<ConversationsList> getList() async =>
      ConversationsList()..addAll(await super.getList());

  /// Get a conversation
  Future<Conversation> get(int? id) => first((t) => t.id == id);
}

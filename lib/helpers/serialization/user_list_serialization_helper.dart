import 'package:comunic/helpers/serialization/base_serialization_helper.dart';
import 'package:comunic/models/user.dart';

/// User serialization helper
///
/// @author Pierre Hubert

UsersListSerialisationHelper? _singleton;

class UsersListSerialisationHelper extends BaseSerializationHelper<User> {
  UsersListSerialisationHelper._();

  factory UsersListSerialisationHelper() {
    if (_singleton == null) _singleton = UsersListSerialisationHelper._();

    return _singleton!;
  }

  @override
  String get type => "users-list";

  @override
  User parse(Map<String, dynamic> m) => User.fromJson(m);

  /// Remove a user by its ID
  Future<void> removeUserByID(int? userID) =>
      removeElement((t) => t.id == userID);
}

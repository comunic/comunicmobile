import 'package:comunic/utils/flutter_utils.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:version/version.dart';

/// Application version helper
///
/// @author Pierre Hubert

class VersionHelper {
  static PackageInfo? _info;

  static Future<void> ensureLoaded() async {
    if (!isWeb) _info = await PackageInfo.fromPlatform();
  }

  /// Get current version information
  static PackageInfo? get info => _info;

  /// Get current application version, in parsed format
  static Version get version => Version.parse(info!.version);
}

import 'package:comunic/helpers/groups_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/models/advanced_user_info.dart';
import 'package:comunic/models/api_request.dart';
import 'package:comunic/models/group.dart';

/// Forez groups helper
///
/// @author Pierre Hubert

class ForezGroupsHelper {
  static Future<List<Group>> getForezGroups() async {
    return (await APIRequest.withLogin("forez/get_groups").execWithThrow())
        .getArray()!
        .cast<Map<String, dynamic>>()
        .map(GroupsHelper.getGroupFromAPI)
        .toList();
  }

  /// Get advanced information about a Forez member
  ///
  /// This methods throws an exception in case of failure
  static Future<AdvancedUserInfo> getMemberInfo(int groupID, int userID) async {
    final response = await (APIRequest.withLogin("forez/get_member_info")
        .addInt("group", groupID)
        .addInt("user", userID)
        .execWithThrowGetObject());

    return UsersHelper.apiToAdvancedUserInfo(response);
  }
}

import 'package:comunic/models/api_request.dart';

/// Virtual directory helper
///
/// @author Pierre Hubert

enum VirtualDirectoryType { USER, GROUP, NONE }

class VirtualDirectoryResult {
  final VirtualDirectoryType type;
  final int? id;

  const VirtualDirectoryResult({
    required this.type,
    this.id,
  });
}

class VirtualDirectoryHelper {
  /// Find a virtual directory
  Future<VirtualDirectoryResult> find(String directory) async {
    final response = await APIRequest(
        uri: "virtualDirectory/find",
        needLogin: true,
        args: {"directory": directory}).exec();

    switch (response.code) {
      case 404:
        return VirtualDirectoryResult(type: VirtualDirectoryType.NONE);

      case 200:
        final id = response.getObject()["id"];
        final kind = response.getObject()["kind"];
        switch (kind) {
          case "user":
            return VirtualDirectoryResult(
                type: VirtualDirectoryType.USER, id: id);
          case "group":
            return VirtualDirectoryResult(
                type: VirtualDirectoryType.GROUP, id: id);

          default:
            throw Exception("Unsupported virtual directory kind: $kind");
        }

      default:
        throw new Exception("Could not get virtual directory!");
    }
  }
}

import 'package:comunic/helpers/conversations_helper.dart';
import 'package:comunic/helpers/friends_helper.dart';
import 'package:comunic/lists/memberships_list.dart';
import 'package:comunic/models/api_request.dart';
import 'package:comunic/models/membership.dart';

/// Web application helper
///
/// @author Pierre Hubert

class WebAppHelper {
  /// Fetch from the server the list of memberships of the user
  ///
  /// Throws in case of failure
  static Future<MembershipList> getMemberships() async {
    final response =
        (await APIRequest.withLogin("webApp/getMemberships").execWithThrow())
            .getArray()!;

    return MembershipList()
      ..addAll(response
          .cast<Map<String, dynamic>>()
          .map(_apiToMembership)
          .where((f) => f != null)
            ..toList());
  }

  /// Turn an API entry into a membership entry
  static Membership? _apiToMembership(Map<String, dynamic> entry) {
    switch (entry["type"]) {
      case "conversation":
        return Membership.conversation(
            ConversationsHelper.apiToConversation(entry["conv"]));

      case "friend":
        return Membership.friend(FriendsHelper.apiToFriend(entry["friend"]));

      case "group":
        return Membership.group(
            groupID: entry["id"], groupLastActive: entry["last_activity"]);

      default:
        print("Unknown membership type: ${entry["type"]}");
        return null;
    }
  }
}

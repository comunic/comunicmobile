import 'package:comunic/ui/dialogs/alert_dialog.dart';
import 'package:comunic/utils/flutter_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

/// Independent push notifications helper
///
/// @author Pierre Hubert

class IndependentPushNotificationsHelper {
  static const platform = const MethodChannel(
      "org.communiquons.comunic/independent-push-notifications-service");

  /// Check if a pre-configuration is required for an application
  static Future<bool> needPreConfiguration() async {
    return await platform.invokeMethod("needPreConfiguration");
  }

  /// Call this method to prepare the application for background refresh by
  /// requesting appropriate permissions, configuring battery optimization...
  ///
  /// Throws an Exception in case of failure
  static Future<void> preConfigure(BuildContext context) async {
    await alert(context,
        tr("Battery optimization settings will appear. To make push notifications service work better, please disable battery optimization for Comunic..."));

    await platform.invokeMethod("preConfigure");
  }

  /// Configure independent push notification services with a pull URL
  static Future<void> configure(String? wsURL) async {
    await platform.invokeMethod("configure", wsURL);
  }

  /// Disable independent push notifications service
  static Future<void> disable() async {
    if (isAndroid) await platform.invokeMethod("disable");
  }
}

import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/search_results_list.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/api_request.dart';
import 'package:comunic/models/search_result.dart';
import 'package:comunic/utils/api_utils.dart';

/// Search helper
///
/// @author Pierre HUBERT

class SearchHelper {
  /// Search for user. This method returns information about the target users
  ///
  /// Returns information about the target users or null if an error occurred
  Future<UsersList?> searchUser(String query) async {
    // Execute the query on the server
    final response = await APIRequest(
        uri: "user/search", needLogin: true, args: {"query": query}).exec();

    if (response.code != 200) return null;

    return await UsersHelper()
        .getUsersInfo(response.getArray()!.map((f) => cast<int>(f)).toList());
  }

  /// Perform a global search
  Future<SearchResultsList> globalSearch(String query) async {
    final result = await APIRequest(
        uri: "search/global", needLogin: true, args: {"query": query}).exec();

    result.assertOk();

    return SearchResultsList()..addAll(result.getArray()!.map((f) {
      switch (f["kind"]) {
        case "user":
          return SearchResult(id: f["id"], kind: SearchResultKind.USER);

        case "group":
          return SearchResult(id: f["id"], kind: SearchResultKind.GROUP);

        default:
          throw Exception("Unkown search kind: ${f["kind"]}");
      }
    }).toList());
  }
}

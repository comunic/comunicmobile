import 'package:comunic/helpers/database/friends_database_helper.dart';
import 'package:comunic/lists/friends_list.dart';
import 'package:comunic/models/api_request.dart';
import 'package:comunic/models/friend.dart';
import 'package:comunic/models/friend_status.dart';

/// Friends helper
///
/// @author Pierre HUBERT

class FriendsHelper {
  final FriendsDatabaseHelper _friendsDatabaseHelper = FriendsDatabaseHelper();

  /// Get the list of friends of the user from the server
  ///
  /// Returns the list of friends in case of success, or null if an error
  /// occurred
  Future<FriendsList?> _downloadList() async {
    final response = await APIRequest(
      uri: "friends/getList",
      needLogin: true,
      args: {
        "complete": "true",
      },
    ).exec();

    if (response.code != 200) return null;

    // Parse and return the list of friends
    FriendsList list = FriendsList()
      ..addAll(response
          .getArray()!
          .cast<Map<String, dynamic>>()
          .map(apiToFriend)
          .toList());

    // Save the list of friends
    _friendsDatabaseHelper.clearTable();
    _friendsDatabaseHelper.insertAll(list);

    return list;
  }

  /// Turn an API entry into a [Friend] object
  static Friend apiToFriend(Map<String, dynamic> row) {
    return Friend(
      id: row["ID_friend"],
      accepted: row["accepted"] == 1,
      lastActive: row["time_last_activity"],
      following: row["following"] == 1,
      canPostTexts: row["canPostTexts"],
    );
  }

  /// Get the list, either from an online or an offline source
  Future<FriendsList?> getList({required bool online}) async {
    if (online)
      return await _downloadList();
    else
      return FriendsList()..addAll(await _friendsDatabaseHelper.getAll());
  }

  /// Respond to friendship request
  Future<bool> respondRequest(int friendID, bool accept) async {
    final response = await APIRequest(
        uri: "friends/respondRequest",
        needLogin: true,
        args: {
          "friendID": friendID.toString(),
          "accept": accept.toString()
        }).exec();

    return response.isOK;
  }

  /// Update following status for a friend
  Future<bool> setFollowing(int friendID, bool follow) async {
    final response = await APIRequest(
        uri: "friends/setFollowing",
        needLogin: true,
        args: {
          "friendID": friendID.toString(),
          "follow": follow.toString()
        }).exec();

    return response.code == 200;
  }

  /// Remove a friend from the list
  Future<bool> removeFriend(int friendID) async {
    final response = await APIRequest(
        uri: "friends/remove",
        needLogin: true,
        args: {"friendID": friendID.toString()}).exec();

    return response.code == 200;
  }

  /// Get friendship status. Throw an exception in case of failure
  Future<FriendStatus> getFriendshipStatus(int userID) async {
    final response = await APIRequest(
      uri: "friends/getStatus",
      needLogin: true,
      args: {"friendID": userID.toString()},
    ).exec();

    if (response.code != 200)
      throw Exception("Could not get friendship status!");

    final obj = response.getObject();

    return FriendStatus(
      userID: userID,
      areFriend: obj["are_friend"],
      sentRequest: obj["sent_request"],
      receivedRequest: obj["received_request"],
      following: obj["following"],
    );
  }

  /// Get and return the list of friends of an other user
  ///
  /// Throws an Exception if could not get the list of friends
  Future<Set<int>> getOtherUserList(int userID) async {
    final response = await APIRequest(
      uri: "friends/get_user_list",
      needLogin: true,
      args: {"userID": userID.toString()},
    ).exec();

    if (response.code != 200)
      throw new Exception("Could not get the list of friends of this user!");

    return Set<int>.from(response.getArray()!);
  }

  /// Send a friendship request to a specified user
  Future<bool> sendRequest(int userID) async {
    return (await APIRequest(
      uri: "friends/sendRequest",
      needLogin: true,
      args: {"friendID": userID.toString()},
    ).exec())
        .isOK;
  }

  /// Cancel a friendship request
  Future<bool> cancelRequest(int userID) async {
    return (await APIRequest(
      uri: "friends/removeRequest",
      needLogin: true,
      args: {"friendID": userID.toString()},
    ).exec())
        .isOK;
  }
}

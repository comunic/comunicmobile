import 'package:comunic/enums/likes_type.dart';
import 'package:comunic/helpers/websocket_helper.dart';

/// Likes helper
///
/// @author Pierre HUBERT

const LikesAPIMap = {
  LikesType.USER: "user",
  LikesType.POST: "post",
  LikesType.COMMENT: "comment",
  LikesType.GROUP: "group",
};

class LikesHelper {
  /// Update liking status of an element
  Future<void> setLiking({
    required LikesType type,
    required bool like,
    required int id,
  }) async {
    return (await ws("likes/update", {
      "type": LikesAPIMap[type],
      "like": like.toString(),
      "id": id.toString(),
    }));
  }
}

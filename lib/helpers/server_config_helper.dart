import 'package:comunic/models/api_request.dart';
import 'package:comunic/models/server_config.dart';
import 'package:version/version.dart';

/// Server configuration helper
///
/// @author Pierre Hubert

class ServerConfigurationHelper {
  static ServerConfig? _config;

  /// Make sure the configuration has been correctly loaded
  static Future<void> ensureLoaded() async {
    if (_config != null) return;

    final response =
        (await APIRequest.withoutLogin("server/config").execWithThrow())
            .getObject();

    final banner = response["banner"];
    final pushNotificationsPolicy = response["push_notifications"];
    final passwordPolicy = response["password_policy"];
    final dataConservationPolicy = response["data_conservation_policy"];
    final conversationsPolicy = response["conversations_policy"];
    final accountInformationPolicy = response["account_info_policy"];
    final reportPolicy = response["report_policy"];

    _config = ServerConfig(
        minSupportedMobileVersion:
            Version.parse(response["min_supported_mobile_version"]),
        termsURL: response["terms_url"],
        privacyPolicyURL: response["privacy_policy_url"],
        contactEmail: response["contact_email"],
        playStoreURL: response["play_store_url"],
        androidDirectDownloadURL: response["android_direct_download_url"],
        banner: banner == null
            ? null
            : Banner(
                enabled: banner["enabled"],
                expire: banner["expire"],
                nature: BannerNatureExt.fromStr(banner["nature"]),
                message: Map<String, dynamic>.from(banner["message"])
                    .map((key, value) => MapEntry(key, value.toString())),
                link: banner["link"]),
        notificationsPolicy: NotificationsPolicy(
          hasFirebase: pushNotificationsPolicy["has_firebase"],
          hasIndependent: pushNotificationsPolicy["has_independent"],
        ),
        passwordPolicy: PasswordPolicy(
          allowMailInPassword: passwordPolicy["allow_email_in_password"],
          allowNameInPassword: passwordPolicy["allow_name_in_password"],
          minPasswordLength: passwordPolicy["min_password_length"],
          minNumberUpperCaseLetters:
              passwordPolicy["min_number_upper_case_letters"],
          minNumberLowerCaseLetters:
              passwordPolicy["min_number_lower_case_letters"],
          minNumberDigits: passwordPolicy["min_number_digits"],
          minNumberSpecialCharacters:
              passwordPolicy["min_number_special_characters"],
          minCategoriesPresence: passwordPolicy["min_categories_presence"],
        ),
        dataConservationPolicy: ServerDataConservationPolicy(
          minInactiveAccountLifetime:
              dataConservationPolicy["min_inactive_account_lifetime"],
          minNotificationLifetime:
              dataConservationPolicy["min_notification_lifetime"],
          minCommentsLifetime: dataConservationPolicy["min_comments_lifetime"],
          minPostsLifetime: dataConservationPolicy["min_posts_lifetime"],
          minConversationMessagesLifetime:
              dataConservationPolicy["min_conversation_messages_lifetime"],
          minLikesLifetime: dataConservationPolicy["min_likes_lifetime"],
        ),
        conversationsPolicy: ConversationsPolicy(
          maxConversationNameLen:
              conversationsPolicy["max_conversation_name_len"],
          minMessageLen: conversationsPolicy["min_message_len"],
          maxMessageLen: conversationsPolicy["max_message_len"],
          allowedFilesType:
              conversationsPolicy["allowed_files_type"].cast<String>(),
          filesMaxSize: conversationsPolicy["files_max_size"],
          writingEventInterval: conversationsPolicy["writing_event_interval"],
          writingEventLifetime: conversationsPolicy["writing_event_lifetime"],
          maxMessageImageWidth: conversationsPolicy["max_message_image_width"],
          maxMessageImageHeight:
              conversationsPolicy["max_message_image_height"],
          maxThumbnailWidth: conversationsPolicy["max_thumbnail_width"],
          maxThumbnailHeight: conversationsPolicy["max_thumbnail_height"],
          maxLogoWidth: conversationsPolicy["max_logo_width"],
          maxLogoHeight: conversationsPolicy["max_logo_height"],
        ),
        accountInformationPolicy: AccountInformationPolicy(
          minFirstNameLength: accountInformationPolicy["min_first_name_length"],
          maxFirstNameLength: accountInformationPolicy["max_first_name_length"],
          minLastNameLength: accountInformationPolicy["min_last_name_length"],
          maxLastNameLength: accountInformationPolicy["max_last_name_length"],
          maxLocationLength: accountInformationPolicy["max_location_length"],
        ),
        reportPolicy: reportPolicy == null
            ? null
            : ReportPolicy(
                causes: List.from(reportPolicy["causes"]
                    .map((cause) => ReportCause(
                        id: cause["id"],
                        label: new Map<String, String>.from(cause["label"])))
                    .toList()),
                maxCommentLength: reportPolicy["max_comment_length"],
                canUserReportHisOwnContent:
                    reportPolicy["can_user_report_his_own_content"],
              ));
  }

  /// Get current server configuration, throwing if it is not loaded yet
  static ServerConfig? get config {
    if (_config == null)
      throw Exception(
          "Trying to access server configuration but it is not loaded yet!");

    return _config;
  }
}

/// Shortcut for server configuration
ServerConfig? get srvConfig => ServerConfigurationHelper.config;

bool get showBanner => srvConfig!.banner != null && srvConfig!.banner!.visible;

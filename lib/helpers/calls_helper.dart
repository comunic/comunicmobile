import 'dart:convert';

import 'package:comunic/helpers/websocket_helper.dart';
import 'package:comunic/lists/call_members_list.dart';
import 'package:comunic/models/call_config.dart';
import 'package:comunic/models/call_member.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';

/// Calls helper
///
/// @author Pierre Hubert

class CallsHelper {
  /// Join a call
  static Future<void> join(int convID) async =>
      await ws("calls/join", {"convID": convID});

  /// Leave a call
  static Future<void> leave(int convID) async =>
      await ws("calls/leave", {"convID": convID});

  /// Get calls configuration
  static Future<CallConfig> getConfig() async {
    final response = await ws("calls/config", {});
    return CallConfig(
      iceServers: response["iceServers"].cast<String>(),
    );
  }

  /// Get current call members
  static Future<CallMembersList> getMembers(int callID) async =>
      CallMembersList()
        ..addAll((await ws("calls/members", {"callID": callID}))
            .map((f) => CallMember(
                  userID: f["userID"],
                  status: f["ready"] ? MemberStatus.READY : MemberStatus.JOINED,
                ))
            .toList()
            .cast<CallMember>());

  /// Request an offer to access another peer's stream
  static Future<void> requestOffer(int callID, int? peerID) async =>
      await ws("calls/request_offer", {"callID": callID, "peerID": peerID});

  /// Send a Session Description message to the server
  static Future<void> sendSessionDescription(
          int callID, int? peerID, RTCSessionDescription sdp) async =>
      await ws("calls/signal", {
        "callID": callID,
        "peerID": peerID,
        "type": "SDP",
        "data": jsonEncode(sdp.toMap())
      });

  /// Send an IceCandidate
  static Future<void> sendIceCandidate(
          int callID, int? peerID, RTCIceCandidate candidate) async =>
      await ws("calls/signal", {
        "callID": callID,
        "peerID": peerID,
        "type": "CANDIDATE",
        "data": jsonEncode(candidate.toMap())
      });

  /// Mark ourselves as ready to stream to other peers
  static Future<void> markPeerReady(int callID) async =>
      await ws("calls/mark_ready", {"callID": callID});

  /// Notify other peers that we stopped streaming
  ///
  /// This method never throw
  static Future<void> notifyStoppedStreaming(int callID) async {
    try {
      await ws("calls/stop_streaming", {"callID": callID});
    } catch (e, stack) {
      print("$e\n$stack");
    }
  }
}

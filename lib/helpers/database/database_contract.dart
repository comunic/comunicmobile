/// Database contract
///
/// @author Pierre HUBERT

/// Main information
class DatabaseContract {
  static const DATABASE_VERSION = 2;
  static const DATABASE_FILE_NAME = "database.sqlite";
}

/// Base table contract
abstract class BaseTableContract {
  static const C_ID = "id";
}

/// Friends table contract
abstract class FriendsListTableContract {
  static const TABLE_NAME = "friends";
  static const C_ID = BaseTableContract.C_ID;
  static const C_ACCEPTED = "accepted";
  static const C_LAST_ACTIVE = "last_active";
  static const C_FOLLOWING = "following";
  static const C_CAN_POST_TEXTS = "can_post_texts";
}

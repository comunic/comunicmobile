import 'package:comunic/helpers/database/database_contract.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

/// Database Helper
///
/// Manage the connection to the local SQLite database
///
/// @author Pierre HUBERT

abstract class DatabaseHelper {
  static Database? _db;

  /// Open the database
  static Future<void> open() async {
    if (_db != null && _db!.isOpen) return;

    var databasePath = await getDatabasesPath();
    _db = await openDatabase(
        join(databasePath, DatabaseContract.DATABASE_FILE_NAME),
        version: DatabaseContract.DATABASE_VERSION,
        onUpgrade: _performDatabaseUpdate,
        onCreate: _initializeDatabase);
  }

  /// Get a database instance
  static Future<Database?> get() async {
    await open();
    return _db;
  }

  /// Perform database update
  ///
  /// Currently : delete all the database tables and initialize it again
  static Future<void> _performDatabaseUpdate(
      Database db, int oldVersion, int newVersion) async {
    // Drop friends list table
    await db
        .execute("DROP TABLE IF EXISTS ${FriendsListTableContract.TABLE_NAME}");

    // Initialize database again
    await _initializeDatabase(db, newVersion);
  }

  /// Initialize the database
  static Future<void> _initializeDatabase(Database db, int version) async {
    // Friends list table
    await db.execute("CREATE TABLE ${FriendsListTableContract.TABLE_NAME} ("
        "${FriendsListTableContract.C_ID} INTEGER PRIMARY KEY, "
        "${FriendsListTableContract.C_ACCEPTED} INTEGER, "
        "${FriendsListTableContract.C_LAST_ACTIVE} INTEGER, "
        "${FriendsListTableContract.C_FOLLOWING} INTEGER, "
        "${FriendsListTableContract.C_CAN_POST_TEXTS} INTEGER"
        ")");
  }
}

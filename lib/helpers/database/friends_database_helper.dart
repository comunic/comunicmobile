import 'package:comunic/helpers/database/database_contract.dart';
import 'package:comunic/helpers/database/model_database_helper.dart';
import 'package:comunic/models/friend.dart';

/// Friends database helper
///
/// @author Pierre HUBERT

class FriendsDatabaseHelper extends ModelDatabaseHelper<Friend> {
  @override
  Friend initializeFromMap(Map<String, dynamic> map) => Friend.fromMap(map);

  @override
  String tableName() => FriendsListTableContract.TABLE_NAME;
}
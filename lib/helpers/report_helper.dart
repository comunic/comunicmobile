import 'package:comunic/enums/report_target_type.dart';
import 'package:comunic/models/api_request.dart';
import 'package:comunic/models/report_target.dart';
import 'package:comunic/models/server_config.dart';

/// Reports Helper
///
/// @author Pierre Hubert

enum ReportResult {
  Success,
  ErrorAlreadyReported,
  Error,
}

class ReportHelper {
  /// Send a new report to the server
  static Future<ReportResult> sendReport({
    required ReportCause cause,
    required ReportTarget target,
    required String comment,
  }) async {
    final response = await APIRequest.withLogin("reports/create", args: {
      "cause": cause.id,
      "target_type": target.type.apiId,
      "target_id": target.id.toString(),
      "comment": comment
    }).exec();

    if (response.isOK) return ReportResult.Success;

    print("Failed to send report: ${response.content}");

    if (response.code == 409) return ReportResult.ErrorAlreadyReported;

    return ReportResult.Error;
  }
}

import 'package:comunic/enums/user_page_visibility.dart';
import 'package:comunic/models/account_image_settings.dart';
import 'package:comunic/models/api_request.dart';
import 'package:comunic/models/data_conservation_policy_settings.dart';
import 'package:comunic/models/general_settings.dart';
import 'package:comunic/models/new_emoji.dart';
import 'package:comunic/models/notifications_settings.dart';
import 'package:comunic/models/security_settings.dart';

import '../models/api_request.dart';

/// Settings helper
///
/// @author Pierre Hubert

const _APIAccountImageVisibilityAPILevels = {
  "open": AccountImageVisibilityLevels.EVERYONE,
  "public": AccountImageVisibilityLevels.COMUNIC_USERS,
  "friends": AccountImageVisibilityLevels.FRIENDS_ONLY,
};

class SettingsHelper {
  /// Get & return general user settings
  static Future<GeneralSettings> getGeneralSettings() async {
    final response =
        (await APIRequest(uri: "settings/get_general", needLogin: true).exec())
            .assertOk()
            .getObject();

    return GeneralSettings(
      email: response["email"],
      firstName: response["firstName"],
      lastName: response["lastName"],
      pageVisibility: response["is_open"]
          ? UserPageVisibility.OPEN
          : response["is_public"]
              ? UserPageVisibility.PUBLIC
              : UserPageVisibility.PRIVATE,
      allowComments: response["allow_comments"],
      allowPostsFromFriends: response["allow_posts_from_friends"],
      allowComunicEmails: response["allow_comunic_mails"],
      publicFriendsList: response["public_friends_list"],
      publicEmail: response["public_email"],
      virtualDirectory: response["virtual_directory"],
      personalWebsite: response["personnal_website"],
      publicNote: response["publicNote"],
      location: response["location"],
    );
  }

  /// Apply new general settings
  static Future<void> updateGeneralSettings(GeneralSettings settings) async =>
      await APIRequest.withLogin("settings/set_general", args: {
        "firstName": settings.firstName,
        "lastName": settings.lastName,
        "allow_comunic_mails": settings.allowComunicEmails ? "true" : "false",
        "isPublic": settings.pageVisibility != UserPageVisibility.PRIVATE
            ? "true"
            : "false",
        "isOpen": settings.pageVisibility == UserPageVisibility.OPEN
            ? "true"
            : "false",
        "allowComments": settings.allowComments ? "true" : "false",
        "allowPostsFromFriends":
            settings.allowPostsFromFriends ? "true" : "false",
        "publicFriendsList": settings.publicFriendsList ? "true" : "false",
        "personnalWebsite": settings.personalWebsite,
        "virtualDirectory": settings.virtualDirectory,
        "publicNote": settings.publicNote,
      })
          .addBool("email_public", settings.publicEmail)
          .addString("location", settings.location ?? "")
          .execWithThrow();

  /// Check out whether a virtual directory is available for a user or not
  ///
  /// Throws in case of failure
  static Future<void> checkUserDirectoryAvailability(String dir) async =>
      (await APIRequest(
              uri: "settings/check_user_directory_availability",
              needLogin: true,
              args: {"directory": dir}).exec())
          .assertOk();

  /// Get & return account image settings
  static Future<AccountImageSettings> getAccountImageSettings() async {
    final response =
        (await APIRequest(uri: "settings/get_account_image", needLogin: true)
                .exec())
            .assertOk()
            .getObject();

    return AccountImageSettings(
        hasImage: response["has_image"],
        imageURL: response["image_url"],
        visibility:
            _APIAccountImageVisibilityAPILevels[response["visibility"]]!);
  }

  /// Upload a new account image
  static Future<void> uploadAccountImage(BytesFile newImage) async =>
      await APIRequest(uri: "settings/upload_account_image", needLogin: true)
          .addBytesFile("picture", newImage)
          .execWithFilesAndThrow();

  /// Upload a new account image from memory
  static Future<bool> uploadAccountImageFromMemory(List<int> bytes) async =>
      (await APIRequest(uri: "settings/upload_account_image", needLogin: true)
              .addBytesFile("picture", BytesFile("accountImage.png", bytes))
              .execWithFiles())
          .isOK;

  /// Change account image visibility level
  static Future<bool> setAccountImageVisibilityLevel(
          AccountImageVisibilityLevels level) async =>
      (await APIRequest(
                  uri: "settings/set_account_image_visibility", needLogin: true)
              .addString(
                  "visibility",

                  // Find appropriate visibility level
                  _APIAccountImageVisibilityAPILevels.entries
                      .firstWhere((f) => f.value == level)
                      .key)
              .exec())
          .isOK;

  /// Delete user account image
  static Future<bool> deleteAccountImage() async =>
      (await APIRequest(uri: "settings/delete_account_image", needLogin: true)
              .exec())
          .isOK;

  /// Upload a new custom emoji
  static Future<void> uploadNewCustomEmoji(NewEmoji newEmoji) async =>
      await APIRequest(
              uri: "settings/upload_custom_emoji",
              needLogin: true,
              args: {"shortcut": newEmoji.shortcut})
          .addBytesFile("image", newEmoji.image)
          .execWithFilesAndThrow();

  /// Delete a custom emoji
  ///
  /// Throws in case of failure
  static Future<void> deleteCustomEmoji(int? emojiID) async =>
      (await APIRequest(uri: "settings/delete_custom_emoji", needLogin: true)
              .addInt("emojiID", emojiID)
              .exec())
          .assertOk();

  /// Check current user password
  ///
  /// Throws in case of failure
  static Future<void> checkUserPassword(String password) async =>
      await APIRequest(uri: "settings/check_password", needLogin: true)
          .addString("password", password)
          .execWithThrow();

  /// Change user password
  ///
  /// Throws in case of failure
  static Future<void> changePassword(
          String oldPassword, String newPassword) async =>
      await APIRequest(uri: "settings/update_password", needLogin: true)
          .addString("oldPassword", oldPassword)
          .addString("newPassword", newPassword)
          .execWithThrow();

  /// Retrieve security settings of the user
  ///
  /// This method throws in case of failure
  static Future<SecuritySettings> getSecuritySettings(String password) async {
    final response =
        (await APIRequest(uri: "settings/get_security", needLogin: true)
                .addString("password", password)
                .execWithThrow())
            .getObject();

    return SecuritySettings(
      securityQuestion1: response["security_question_1"],
      securityAnswer1: response["security_answer_1"],
      securityQuestion2: response["security_question_2"],
      securityAnswer2: response["security_answer_2"],
    );
  }

  /// Apply new security settings to the user
  ///
  /// Throws in case of failure
  static Future<void> setSecuritySettings(
      String password, SecuritySettings newSettings) async {
    await APIRequest(uri: "settings/set_security", needLogin: true)
        .addString("password", password)
        .addString("security_question_1", newSettings.securityQuestion1)
        .addString("security_answer_1", newSettings.securityAnswer1)
        .addString("security_question_2", newSettings.securityQuestion2)
        .addString("security_answer_2", newSettings.securityAnswer2)
        .execWithThrow();
  }

  /// Get account data conservation policy settings
  ///
  /// Throws in case of failure
  static Future<DataConservationPolicySettings>
      getDataConservationPolicy() async {
    final response =
        (await APIRequest.withLogin("settings/get_data_conservation_policy")
                .execWithThrow())
            .getObject();

    return DataConservationPolicySettings(
        inactiveAccountLifeTime: response["inactive_account_lifetime"],
        notificationLifetime: response["notification_lifetime"],
        commentsLifetime: response["comments_lifetime"],
        postsLifetime: response["posts_lifetime"],
        conversationMessagesLifetime:
            response["conversation_messages_lifetime"],
        likesLifetime: response["likes_lifetime"]);
  }

  /// Apply new data conservation policy settings
  ///
  /// Throws in case of failure
  static Future<void> setDataConservationPolicy(
      String? password, DataConservationPolicySettings newSettings) async {
    await APIRequest(
            uri: "settings/set_data_conservation_policy", needLogin: true)
        .addString("password", password)
        .addInt("inactive_account_lifetime",
            newSettings.inactiveAccountLifeTime ?? 0)
        .addInt("notification_lifetime", newSettings.notificationLifetime ?? 0)
        .addInt("comments_lifetime", newSettings.commentsLifetime ?? 0)
        .addInt("posts_lifetime", newSettings.postsLifetime ?? 0)
        .addInt("conversation_messages_lifetime",
            newSettings.conversationMessagesLifetime ?? 0)
        .addInt("likes_lifetime", newSettings.likesLifetime ?? 0)
        .execWithThrow();
  }

  /// Get notifications settings
  ///
  /// Throws in case of failure
  static Future<NotificationsSettings> getNotificationsSettings() async {
    final response = await APIRequest.withLogin("settings/get_notifications")
        .execWithThrowGetObject();

    return NotificationsSettings(
      allowConversations: response["allow_conversations"],
      allowNotificationsSound: response["allow_notifications_sound"],
    );
  }

  /// Apply new notifications settings
  ///
  /// Throws in case of failure
  static Future<void> setNotificationsSettings(
          NotificationsSettings settings) async =>
      await APIRequest.withLogin("settings/set_notifications")
          .addBool(
              "allow_notifications_sound", settings.allowNotificationsSound)
          .addBool("allow_conversations", settings.allowConversations)
          .execWithThrow();
}

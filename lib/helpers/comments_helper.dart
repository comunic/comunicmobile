import 'package:comunic/models/api_request.dart';
import 'package:comunic/models/comment.dart';
import 'package:comunic/models/displayed_content.dart';
import 'package:comunic/models/new_comment.dart';

/// Comments helper
///
/// @author Pierre HUBERT

class CommentsHelper {
  /// Send a new comment
  ///
  /// Returns 0 or below in case of failure, the ID of the comment else
  Future<int> createComment(NewComment comment) async {
    final request = APIRequest(uri: "comments/create", needLogin: true, args: {
      "postID": comment.postID.toString(),
      "content": comment.hasContent ? comment.content : "",
    });

    if (comment.hasImage) request.addBytesFile("image", comment.image);

    final response = await request.execWithFiles();

    if (response.code != 200) return -1;

    return response.getObject()["commentID"];
  }

  /// Get a single comment from the server, specified by its [id]
  Future<Comment?> getSingle(int id) async {
    final response = await APIRequest(
        uri: "comments/get_single",
        needLogin: true,
        args: {"commentID": id.toString()}).exec();

    if (response.code != 200) return null;

    return apiToComment(response.getObject());
  }

  /// Update comment content
  Future<bool> updateContent(int id, String? newContent) async {
    return (await APIRequest(uri: "comments/edit", needLogin: true, args: {
      "commentID": id.toString(),
      "content": newContent,
    }).exec())
        .isOK;
  }

  /// Attempt to delete a comment
  Future<bool> delete(int id) async {
    return (await APIRequest(
          uri: "comments/delete",
          needLogin: true,
          args: {"commentID": id.toString()},
        ).exec())
            .code ==
        200;
  }

  /// Turn an API entry into a [Comment] object
  static Comment apiToComment(Map<String, dynamic> entry) {
    return Comment(
      id: entry["ID"],
      userID: entry["userID"],
      postID: entry["postID"],
      timeSent: entry["time_sent"],
      content: DisplayedString(entry["content"]),
      imageURL: entry["img_url"],
      likes: entry["likes"],
      userLike: entry["userlike"],
    );
  }
}

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

/// Firebase messaging helper
///
/// @author Pierre Hubert

bool _isFirebaseReady = false;

class FirebaseMessagingHelper {
  /// Pre configure notifications
  static Future<void> preConfigure() async {
    if (!_isFirebaseReady) await Firebase.initializeApp();
    _isFirebaseReady = true;

    final settings = await FirebaseMessaging.instance.requestPermission();

    if (settings.authorizationStatus != AuthorizationStatus.authorized)
      throw new Exception("The user did not authorize notifications!");
  }

  /// Get a Firebase access token
  static Future<String?> getToken() async {
    return await FirebaseMessaging.instance.getToken();
  }
}

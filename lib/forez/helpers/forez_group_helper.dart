import 'package:comunic/helpers/groups_helper.dart';
import 'package:comunic/helpers/preferences_helper.dart';
import 'package:comunic/models/advanced_group_info.dart';

/// Forez group helper
///
/// Contains the ID of the currently selected Forez group
///
/// @author Pierre Hubert

AdvancedGroupInfo? _forezGroup;

class ForezGroupHelper {
  static Future<void> setId(int groupID) async {
    (await PreferencesHelper.getInstance())
        .setInt(PreferencesKeyList.FOREZ_GROUP, groupID);
  }

  static Future<int?> getId() async {
    return (await PreferencesHelper.getInstance())
        .getInt(PreferencesKeyList.FOREZ_GROUP);
  }

  static Future<void> refreshInfo() async {
    final res = await GroupsHelper().getAdvancedInfo(await getId());
    assert(res.status == GetAdvancedInfoStatus.SUCCESS);
    _forezGroup = res.info;
  }

  static AdvancedGroupInfo? getGroup() => _forezGroup;
}

AdvancedGroupInfo? get forezGroup => ForezGroupHelper.getGroup();

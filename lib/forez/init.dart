import 'package:comunic/forez/helpers/forez_group_helper.dart';
import 'package:comunic/forez/tour/forez_tour_builder.dart';
import 'package:comunic/forez/ui/routes/forez_route.dart';
import 'package:comunic/models/config.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Forez initialization
///
/// @author Pierre Hubert

class ForezConfig extends Config {
  ForezConfig({
    required String apiServerName,
    required String apiServerUri,
    required bool apiServerSecure,
    required String clientName,
  }) : super(
          apiServerName: apiServerName,
          apiServerUri: apiServerUri,
          apiServerSecure: apiServerSecure,
          clientName: clientName,
          splashBackgroundColor: Colors.green.shade900,
          primaryColor: Colors.green,
          primaryColorDark: Colors.green.shade900,
          appName: "#Forez",
          appQuickDescription: tr("Events organisation in Forez plain"),
          unreadConversationColor: Colors.green.shade900,
          defaultConversationColor: Colors.green.shade900,
          toursEntriesBuilder: buildTour,
          additionalLoading: () => ForezGroupHelper.refreshInfo(),
          mainRouteBuilder: (c, k) => ForezRoute(key: k),
        );
}

import 'package:comunic/forez/init.dart';
import 'package:comunic/main.dart';
import 'package:comunic/models/config.dart';

/// Forez online configuration
///
/// @author Pierre Hubert

void main() {
  Config.set(ForezConfig(
    apiServerName: "api.communiquons.org",
    apiServerUri: "/",
    apiServerSecure: true,
    clientName: "ForezMobile",
  ));

  subMain();
}

import 'package:comunic/forez/helpers/forez_group_helper.dart';
import 'package:comunic/helpers/forez_groups_helper.dart';
import 'package:comunic/helpers/groups_helper.dart';
import 'package:comunic/models/group.dart';
import 'package:comunic/ui/dialogs/alert_dialog.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/tour/presentation_pane.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/log_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Ask the user to join a Forez group
///
/// @author Pierre Hubert

class JoinForezGroupPane extends PresentationPane {
  JoinForezGroupPane({
    required Function() onUpdated,
    required GlobalKey<JoinGroupPaneBodyState>? key,
  }) : super(
          icon: Icons.login,
          title: tr("Join a Forez group")!,
          child: (c) => _JoinGroupPaneBody(
            key: key,
            onUpdated: onUpdated,
          ),
          canGoNext: key?.currentState?.canGoNext ?? false,
          onTapNext: (c) => key!.currentState!.validateChoice(),
        );
}

class _JoinGroupPaneBody extends StatefulWidget {
  final Function() onUpdated;

  const _JoinGroupPaneBody({
    Key? key,
    required this.onUpdated,
  })  : super(key: key);

  @override
  JoinGroupPaneBodyState createState() => JoinGroupPaneBodyState();
}

class JoinGroupPaneBodyState extends State<_JoinGroupPaneBody> {
  final _key = GlobalKey<AsyncScreenWidgetState>();

  late List<Group> _groups;
  int? _currChoice;

  bool get canGoNext => _currChoice != null && _currChoice! > 0;

  Group get _currGroup => _groups.firstWhere((e) => e.id == _currChoice);

  Future<void> _load() async {
    _groups = await ForezGroupsHelper.getForezGroups();
    _currChoice = _groups[0].id;

    widget.onUpdated();
  }

  @override
  Widget build(BuildContext context) => AsyncScreenWidget(
      key: _key,
      onReload: _load,
      onBuild: onBuild,
      errorMessage: tr("Failed to load the list of Forez groups!")!);

  Widget onBuild() => ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 300),
        child: Column(
          children: [
            Text(tr("Please choose now the Forez group you want to join...")!),
          ]..addAll(_groups.map((e) => RadioListTile(
                value: e.id,
                groupValue: _currChoice,
                onChanged: (dynamic v) => setState(() => _currChoice = e.id),
                title: Text(e.name),
                subtitle: Text(e.membershipText),
              ))),
        ),
      );

  Future<bool> validateChoice() async {
    try {
      switch (_currGroup.membershipLevel) {
        case GroupMembershipLevel.VISITOR:
          if (!await GroupsHelper.sendRequest(_currGroup.id))
            throw Exception("Failed to send group membership request!");
          break;

        case GroupMembershipLevel.INVITED:
          if (!await GroupsHelper.respondInvitation(_currGroup.id, true))
            throw Exception(
                "Failed to respond to group membership invitation!");
          break;

        case GroupMembershipLevel.PENDING:
          break;

        case GroupMembershipLevel.ADMINISTRATOR:
        case GroupMembershipLevel.MODERATOR:
        case GroupMembershipLevel.MEMBER:
          break;
      }

      // Check if the user can not access yet the group
      if ((_currGroup.membershipLevel == GroupMembershipLevel.VISITOR &&
              _currGroup.registrationLevel != GroupRegistrationLevel.OPEN) ||
          _currGroup.membershipLevel == GroupMembershipLevel.PENDING) {
        await alert(context,
            "${tr("You can not access this group yet, please wait for a member of the group to accept your request.")}\n${tr("Hopefully this will not be too long.")}\n${tr("Please check back soon!")}");

        _key.currentState!.refresh();

        return false;
      }

      await ForezGroupHelper.setId(_currGroup.id);
      return true;
    } catch (e, s) {
      logError(e, s);
      snack(context, tr("Failed to register to group!")!);
      _key.currentState!.refresh();
      return false;
    }
  }
}

import 'package:comunic/forez/helpers/forez_group_helper.dart';
import 'package:comunic/forez/tour/join_group_pane.dart';
import 'package:comunic/ui/routes/forez_presence_settings_route.dart';
import 'package:comunic/ui/routes/tour_route.dart';
import 'package:comunic/ui/widgets/tour/account_image_tour_pane.dart';
import 'package:comunic/ui/widgets/tour/first_pane.dart';
import 'package:comunic/ui/widgets/tour/last_pane.dart';
import 'package:comunic/ui/widgets/tour/presentation_pane.dart';
import 'package:comunic/ui/widgets/tour/tour_notifications_pane.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:flutter/material.dart';

/// Forez tour builder
///
/// Handles the presentation tour for the Forez flavor application
///
/// @author Pierre Hubert

const _JOIN_GROUP_KEY_ID = 1;

List<Widget> buildTour(TourRouteState state) {
  if (!state.pubKeys.containsKey(_JOIN_GROUP_KEY_ID))
    state.pubKeys[_JOIN_GROUP_KEY_ID] = GlobalKey<JoinGroupPaneBodyState>();

  return [
    FirstTourPane(
      msgOne: tr(
          "Welcome to #Forez, the central application dedicated to events planning in the Forez plain!"),
      msgTwo: tr("Let's first join a Forez group!"),
    ),
    JoinForezGroupPane(
      key: state.pubKeys[_JOIN_GROUP_KEY_ID] as GlobalKey<JoinGroupPaneBodyState>?,
      onUpdated: () => state.rebuild(),
    ),
    FirstTourPane(
      msgOne: tr("Great, you are now a member of a Forez group!"),
      msgTwo: tr(
          "Before we continue, you must be aware that #Forez is now based on Comunic, so you are using Comunic in fact..."),
    ),
    AccountImageTourPane(
      user: state.currUser,
      onUpdated: (ctx) => state.setStateKeepCurrentIndex(ctx),
    ),
    TourNotificationsPane(
      pushNotificationsKey: state.pushNotificationsKey,
      onConfigured: state.rebuild,
      onChanged: state.rebuild,
      visible: !state.areNotificationsConfigured,
    ),

    // Forez specific features
    PresentationPane(
      icon: Icons.calendar_today,
      title: tr("Presence in Forez")!,
      text: tr(
          "Easily specify the days you are in Forez plain, so that everyone can know it!"),
      actionTitle: tr("Do it now!"),
      onActionTap: (context) async =>
          showPresenceSettingsRoute(context, await ForezGroupHelper.getId()),
    ),

    // Chat pane
    PresentationPane(
      icon: Icons.question_answer,
      title: tr("Conversations")!,
      text: tr(
          "#Forez now integrates the conversation system of Comunic, so you have access both to public and private conversations!"),
    ),

    LastTourPane(),
  ];
}

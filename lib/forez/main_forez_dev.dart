import 'dart:io';

import 'package:comunic/forez/init.dart';
import 'package:comunic/main.dart';
import 'package:comunic/models/config.dart';

/// Forez development configuration
///
/// @author Pierre Hubert

/// Fix HTTPS issue
class MyHttpOverride extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (cert, host, port) {
        return host == "devweb.local"; // Forcefully trust local website
      };
  }
}

void main() {
  Config.set(ForezConfig(
    apiServerName: "192.168.1.9:3000",
    apiServerUri: "/",
    apiServerSecure: false,
    clientName: "ForezFlutter",
  ));

  HttpOverrides.global = new MyHttpOverride();

  subMain();
}

import 'package:comunic/forez/helpers/forez_group_helper.dart';
import 'package:comunic/forez/ui/routes/search_users.dart';
import 'package:comunic/helpers/groups_helper.dart';
import 'package:comunic/helpers/users_helper.dart';
import 'package:comunic/lists/group_members_list.dart';
import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/group_membership.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/utils/account_utils.dart';
import 'package:comunic/utils/conversations_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/log_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Forez directory screen
///
/// @author Pierre Hubert

enum _PopupMenuActions {
  PRIVATE_CONVERSATION,
  CANCEL_INVITATION,
  ACCEPT_REQUEST,
  REJECT_REQUEST
}

class ForezDirectoryScreen extends StatefulWidget {
  @override
  _ForezDirectoryScreenState createState() => _ForezDirectoryScreenState();
}

class _ForezDirectoryScreenState extends State<ForezDirectoryScreen> {
  final _key = GlobalKey<AsyncScreenWidgetState>();

  late UsersList _users;
  late GroupMembersList _members;

  Future<void> _load() async {
    _members = await GroupsHelper.getMembersList(forezGroup!.id);
    _users = await UsersHelper().getListWithThrow(_members.usersID);
  }

  @override
  Widget build(BuildContext context) => Stack(
        children: [
          AsyncScreenWidget(
            onReload: _load,
            onBuild: onBuild,
            errorMessage: tr("Failed to load members list!")!,
            key: _key,
          ),
          Positioned(
            child: FloatingActionButton(
              onPressed: _doSearch,
              child: Icon(Icons.search),
              backgroundColor: Colors.green,
            ),
            right: 20,
            bottom: 20,
          ),
        ],
      );

  Widget onBuild() => ListView.builder(
        itemBuilder: (c, i) => _ForezMemberTile(
          member: _members[i],
          user: _users.getUser(_members[i].userID),
          onTap: (user) => _openUserProfile(user),
          selectedAction: _doPopupMenuAction,
        ),
        itemCount: _members.length,
      );

  void _doPopupMenuAction(_PopupMenuActions action, User user) async {
    try {
      switch (action) {
        case _PopupMenuActions.PRIVATE_CONVERSATION:
          openPrivateConversation(context, user.id);
          break;

        case _PopupMenuActions.CANCEL_INVITATION:
          if (!await showConfirmDialog(
              context: context,
              message: tr(
                  "Do you really want to cancel the invitation sent to %u%?",
                  args: {"u": user.fullName}))) return;

          await GroupsHelper.cancelInvitation(forezGroup!.id, user.id);
          _key.currentState!.refresh();
          break;

        case _PopupMenuActions.ACCEPT_REQUEST:
          await GroupsHelper.respondRequest(forezGroup!.id, user.id, true);
          _key.currentState!.refresh();
          break;

        case _PopupMenuActions.REJECT_REQUEST:
          if (!await showConfirmDialog(
              context: context,
              message: tr(
                  "Do you really want to reject the request of %u% to join the Forez group?",
                  args: {"u": user.fullName}))) return;

          await GroupsHelper.respondRequest(forezGroup!.id, user.id, false);
          _key.currentState!.refresh();
          break;
      }
    } catch (e, s) {
      logError(e, s);
      snack(context, tr("Error while processing action!")!);
    }
  }

  void _doSearch() async {
    final user = await searchUser(
        context,
        UsersList()
          ..addAll(_users.where((u) =>
              _members.where((m) => m.userID == u.id).first.isAtLeastMember)));

    if (user != null) _openUserProfile(user);
  }

  void _openUserProfile(User user) =>
      MainController.of(context)!.openUserPage(user.id);
}

class _ForezMemberTile extends StatelessWidget {
  final User user;
  final GroupMembership member;
  final Function(_PopupMenuActions, User) selectedAction;
  final Function(User) onTap;

  const _ForezMemberTile({
    Key? key,
    required this.user,
    required this.member,
    required this.selectedAction,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => ListTile(
        leading: AccountImageWidget(user: user),
        title: Text(user.fullName),
        subtitle: Text(member.membershipText),
        trailing: !member.isAtLeastMember && forezGroup!.isAtLeastModerator
            ? (member.isInvited
                ? _buildInvitedButton()
                : _buildRequestedButton())
            : _buildConversationButton(),
        onTap: member.isAtLeastMember ? () => onTap(user) : null,
      );

  Widget? _buildConversationButton() => user.id == userID()
      ? null
      : IconButton(
          icon: Icon(Icons.message),
          onPressed: () =>
              selectedAction(_PopupMenuActions.PRIVATE_CONVERSATION, user));

  Widget _buildInvitedButton() => _MembershipButton(
        user: user,
        action: _PopupMenuActions.CANCEL_INVITATION,
        onTap: selectedAction,
        color: Colors.redAccent,
        icon: Icons.close,
      );

  Widget _buildRequestedButton() => IntrinsicWidth(
        child: Row(
          children: [
            _MembershipButton(
              user: user,
              action: _PopupMenuActions.ACCEPT_REQUEST,
              onTap: selectedAction,
              color: Colors.green,
              icon: Icons.check,
            ),
            SizedBox(width: 5),
            _MembershipButton(
              user: user,
              action: _PopupMenuActions.REJECT_REQUEST,
              onTap: selectedAction,
              color: Colors.redAccent,
              icon: Icons.close,
            ),
          ],
        ),
      );
}

class _MembershipButton extends StatelessWidget {
  final User user;
  final _PopupMenuActions action;
  final Function(_PopupMenuActions, User) onTap;
  final Color color;
  final IconData icon;

  const _MembershipButton({
    Key? key,
    required this.user,
    required this.action,
    required this.onTap,
    required this.color,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => ElevatedButton(
        onPressed: () => onTap(action, user),
        child: Icon(icon),
        style: ButtonStyle(
            visualDensity: VisualDensity.compact,
            backgroundColor: MaterialStateProperty.all(color)),
      );
}

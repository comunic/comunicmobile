import 'package:comunic/lists/users_list.dart';
import 'package:comunic/models/user.dart';
import 'package:comunic/ui/widgets/account_image_widget.dart';
import 'package:flutter/material.dart';

/// Search for users
///
/// @author Pierre Hubert

Future<User?> searchUser(BuildContext context, UsersList users) async {
  return await showSearch<User?>(
      context: context, delegate: _SearchDelegate(users));
}

class _SearchDelegate extends SearchDelegate<User?> {
  final UsersList _usersList;

  _SearchDelegate(this._usersList);

  @override
  List<Widget>? buildActions(BuildContext context) => null;

  @override
  Widget buildLeading(BuildContext context) => IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => this.close(context, null),
      );

  @override
  Widget buildSuggestions(BuildContext context) {
    final List<User> list = _usersList
        .where((element) =>
            element.fullName.toLowerCase().contains(query.toLowerCase()))
        .toList();

    return ListView.builder(
        itemCount: list.length,
        itemBuilder: (c, i) {
          final e = list[i];
          return ListTile(
            title: Text(e.fullName),
            leading: AccountImageWidget(user: e),
            onTap: () {
              query = e.fullName;
              close(context, e);
            },
          );
        });
  }

  @override
  Widget buildResults(BuildContext context) => Container();
}

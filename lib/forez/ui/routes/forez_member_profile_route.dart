import 'package:cached_network_image/cached_network_image.dart';
import 'package:comunic/forez/helpers/forez_group_helper.dart';
import 'package:comunic/helpers/forez_groups_helper.dart';
import 'package:comunic/helpers/forez_presence_helper.dart';
import 'package:comunic/lists/forez_presences_set.dart';
import 'package:comunic/models/advanced_user_info.dart';
import 'package:comunic/models/displayed_content.dart';
import 'package:comunic/ui/widgets/async_screen_widget.dart';
import 'package:comunic/ui/widgets/copy_icon.dart';
import 'package:comunic/ui/widgets/forez_presence_calendar_widget.dart';
import 'package:comunic/ui/widgets/text_widget.dart';
import 'package:comunic/utils/account_utils.dart';
import 'package:comunic/utils/conversations_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

/// Show information about a Forez member
///
/// @author Pierre Hubert

class ForezMemberProfileRoute extends StatefulWidget {
  final int userID;

  const ForezMemberProfileRoute({
    Key? key,
    required this.userID,
  }) : super(key: key);

  @override
  _ForezMemberProfileRouteState createState() =>
      _ForezMemberProfileRouteState();
}

class _ForezMemberProfileRouteState extends State<ForezMemberProfileRoute> {
  final double _appBarHeight = 256.0;

  final _key = GlobalKey<AsyncScreenWidgetState>();

  late AdvancedUserInfo _user;
  late PresenceSet _presence;

  Future<void> _load() async {
    _user =
        await ForezGroupsHelper.getMemberInfo(forezGroup!.id, widget.userID);
    _presence =
        await ForezPresenceHelper.getForUser(forezGroup!.id, widget.userID);
  }

  @override
  Widget build(BuildContext context) => AsyncScreenWidget(
      key: _key,
      onReload: _load,
      onBuild: _buildProfile,
      loadingWidget: _buildLoading(),
      errorWidget: _buildError(),
      errorMessage: tr(
          "Failed to load user information, maybe it is not a Forez member yet?")!);

  Widget _buildLoading() => Scaffold(
        appBar: AppBar(
          title: Text(tr("Loading...")!),
        ),
        body: buildCenteredProgressBar(),
      );

  Widget _buildError() => Scaffold(
        appBar: AppBar(
          title: Text(tr("Error")!),
        ),
        body: buildErrorCard(
            tr("Failed to load user information, maybe it is not a Forez member yet?"),
            actions: [
              MaterialButton(
                onPressed: () => _key.currentState!.refresh(),
                child: Text(tr("Try again")!),
                textColor: Colors.white,
              )
            ]),
      );

  Widget _buildProfile() => Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            _buildAppBar(),
            _buildList(),
          ],
        ),
      );

  Widget _buildAppBar() => SliverAppBar(
        expandedHeight: _appBarHeight,
        pinned: true,
        flexibleSpace: FlexibleSpaceBar(
          title: Text(_user.fullName),
          background: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              CachedNetworkImage(
                fit: BoxFit.cover,
                imageUrl: _user.accountImageURL,
                height: _appBarHeight,
              ),
              // This gradient ensures that the toolbar icons are distinct
              // against the background image.
              const DecoratedBox(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment(0.0, -1.0),
                    end: Alignment(0.0, -0.4),
                    colors: <Color>[Color(0x60000000), Color(0x00000000)],
                  ),
                ),
              ),
            ],
          ),
        ),
        actions: _user.id == userID()
            ? null
            : [
                IconButton(
                    icon: Icon(Icons.message),
                    onPressed: () => openPrivateConversation(context, _user.id))
              ],
      );

  Widget _buildList() => SliverList(
          delegate: SliverChildListDelegate(<Widget>[
        // Public note
        !_user.hasPublicNote
            ? Container()
            : ListTile(
                leading: Icon(Icons.note),
                title: TextWidget(content: DisplayedString(_user.publicNote)),
                subtitle: Text(tr("Note")!),
              ),

        // Email address
        !_user.hasEmailAddress
            ? Container()
            : ListTile(
                leading: Icon(Icons.email),
                title: Text(_user.emailAddress!),
                subtitle: Text(tr("Email address")!),
                trailing: CopyIcon(_user.emailAddress!),
              ),

        // Location
        !_user.hasLocation
            ? Container()
            : ListTile(
                leading: Icon(Icons.location_on),
                title: Text(_user.location!),
                subtitle: Text(tr("Location")!),
                trailing: CopyIcon(_user.location!),
              ),

        // Website
        !_user.hasPersonalWebsite
            ? Container()
            : ListTile(
                leading: Icon(Icons.link),
                title: Text(_user.personalWebsite),
                subtitle: Text(tr("Website")!),
                trailing: IconButton(
                  icon: Icon(Icons.open_in_new),
                  onPressed: () => launchUrlString(_user.personalWebsite),
                ),
              ),

        Divider(),
        ListTile(
          leading: Icon(Icons.calendar_today),
          title: Text(tr("Presence in Forez")!),
          subtitle: Text(_presence.containsDate(DateTime.now())
              ? tr("Present today")!
              : tr("Absent")!),
        ),
        PresenceCalendarWidget(presenceSet: _presence),
        Divider(),
      ]));
}

/// Check password reset token result
///
/// @author Pierre Hubert

class ResCheckPasswordToken {
  final String firstName;
  final String lastName;
  final String email;

  const ResCheckPasswordToken({
    required this.firstName,
    required this.lastName,
    required this.email,
  });
}

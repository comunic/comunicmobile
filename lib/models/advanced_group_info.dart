import 'package:comunic/enums/likes_type.dart';
import 'package:comunic/models/conversation.dart';
import 'package:comunic/models/like_element.dart';

import 'group.dart';

/// Advanced group information
///
/// @author Pierre Hubert

class AdvancedGroupInfo extends Group implements LikeElement {
  bool? isMembersListPublic;
  final int? timeCreate;
  String description;
  String url;
  int likes;
  bool userLike;
  List<Conversation>? conversations;
  bool isForezGroup;

  AdvancedGroupInfo({
    required int id,
    required String name,
    required String iconURL,
    required int numberMembers,
    required GroupMembershipLevel membershipLevel,
    required GroupVisibilityLevel visibilityLevel,
    required GroupRegistrationLevel registrationLevel,
    required GroupPostCreationLevel postCreationLevel,
    required String virtualDirectory,
    required bool following,
    required this.isMembersListPublic,
    required this.timeCreate,
    required this.description,
    required this.url,
    required this.likes,
    required this.userLike,
    required this.conversations,
    required this.isForezGroup,
  }) : super(
            id: id,
            name: name,
            iconURL: iconURL,
            numberMembers: numberMembers,
            membershipLevel: membershipLevel,
            visibilityLevel: visibilityLevel,
            registrationLevel: registrationLevel,
            postCreationLevel: postCreationLevel,
            virtualDirectory: virtualDirectory,
            following: following);

  @override
  LikesType likeType = LikesType.GROUP;

  get hasURL => url.isNotEmpty && url != "null";

  get hasDescription => description.isNotEmpty && description != "null";
}

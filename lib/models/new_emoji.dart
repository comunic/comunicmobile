import 'api_request.dart';

/// New emoji information
///
/// @author Pierre HUBERT

class NewEmoji {
  final String shortcut;
  final BytesFile image;

  const NewEmoji({
    required this.shortcut,
    required this.image,
  });
}

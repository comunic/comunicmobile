/// Single search result
///
/// @author Pierre Hubert

enum SearchResultKind { USER, GROUP }

class SearchResult {
  final int id;
  final SearchResultKind kind;

  SearchResult({
    required this.id,
    required this.kind,
  });
}

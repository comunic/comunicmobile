import 'package:comunic/enums/likes_type.dart';
import 'package:comunic/enums/post_kind.dart';
import 'package:comunic/enums/post_visibility_level.dart';
import 'package:comunic/enums/user_access_levels.dart';
import 'package:comunic/lists/comments_list.dart';
import 'package:comunic/models/displayed_content.dart';
import 'package:comunic/models/like_element.dart';
import 'package:comunic/models/survey.dart';
import 'package:comunic/utils/account_utils.dart' as account;

/// Single post information
///
/// @author Pierre HUBERT

class Post implements LikeElement {
  final int id;
  final int userID;
  final int? userPageID;
  final int? groupID;
  final int timeSent;
  DisplayedString content;
  PostVisibilityLevel visibilityLevel;
  final PostKind kind;
  final int? fileSize;
  final String? fileType;
  final String? filePath;
  final String? fileURL;
  final int? timeEnd;
  final String? linkURL;
  final String? linkTitle;
  final String? linkDescription;
  final String? linkImage;
  int likes;
  bool userLike;
  final UserAccessLevels access;
  final CommentsList? comments;
  Survey? survey;

  Post(
      {required this.id,
      required this.userID,
      required this.userPageID,
      required this.groupID,
      required this.timeSent,
      required this.content,
      required this.visibilityLevel,
      required this.kind,
      required this.fileSize,
      required this.fileType,
      required this.filePath,
      required this.fileURL,
      required this.timeEnd,
      required this.linkURL,
      required this.linkTitle,
      required this.linkDescription,
      required this.linkImage,
      required this.likes,
      required this.userLike,
      required this.access,
      required this.comments,
      required this.survey})
      : assert(userPageID != 0 || groupID != 0),
        assert(kind != PostKind.COUNTDOWN || timeEnd != null),
        assert(kind != PostKind.SURVEY || survey != null);

  bool get isGroupPost => groupID != null && groupID! > 0;

  bool get hasContent => !content.isNull && !content.isEmpty;

  bool get hasComments => comments != null;

  bool get canUpdate => access == UserAccessLevels.FULL;

  bool get hasLinkImage => linkImage != null;

  bool get canDelete =>
      access == UserAccessLevels.FULL ||
      access == UserAccessLevels.INTERMEDIATE;

  bool get isOwner => userID == account.userID();

  @override
  LikesType get likeType => LikesType.POST;
}

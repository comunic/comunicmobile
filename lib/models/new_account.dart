/// New account information container
///
/// @author Pierre HUBERT

class NewAccount {
  final String firstName;
  final String lastName;
  final String email;
  final String password;

  NewAccount({
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.password,
  });
}

/// Single survey choice
///
/// @author Pierre HUBERT

class SurveyChoice {
  final int id;
  final String name;
  int responses;

  SurveyChoice({
    required this.id,
    required this.name,
    required this.responses,
  });
}

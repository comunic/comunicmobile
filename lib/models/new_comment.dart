import 'api_request.dart';

/// New comment information
///
/// @author Pierre HUBERT

class NewComment {
  final int postID;
  final String content;
  final BytesFile? image;

  const NewComment({
    required this.postID,
    required this.content,
    required this.image,
  });

  bool get hasContent => content.length > 0;

  bool get hasImage => image != null;
}

import 'package:comunic/enums/report_target_type.dart';

class ReportTarget {
  final ReportTargetType type;
  final int id;

  const ReportTarget(this.type, this.id);
}

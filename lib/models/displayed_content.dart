import 'package:comunic/utils/ui_utils.dart';

/// Optimized colons Emoji-parsed string
///
/// @author Pierre Hubert

class DisplayedString {
  String? _string;
  String? _parseCache;

  DisplayedString(this._string);

  int get length => _string!.length;

  bool get isEmpty => _string!.isEmpty;

  bool get isNull => _string == null;

  String? get content => _string;

  set content(String? content) {
    _string = content;
    _parseCache = null;
  }

  @override
  String toString() {
    return _string!;
  }

  String? get parsedString {
    if (_parseCache == null) {
      _parseCache = parseEmojies(this._string!);
    }

    return _parseCache;
  }
}

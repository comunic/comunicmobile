import 'package:comunic/enums/likes_type.dart';

/// Element that can be liked by the user
///
/// @author Pierre HUBERT

abstract class LikeElement {
  LikesType get likeType;

  int get id;

  late bool userLike;
  late int likes;
}

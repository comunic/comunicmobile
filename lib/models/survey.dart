import 'package:comunic/models/survey_choice.dart';
import 'package:comunic/utils/account_utils.dart' as account;

/// Survey information
///
/// @author Pierre HUBERT

class Survey {
  final int id;
  final int userID;
  final int postID;
  final int creationTime;
  final String question;
  int userChoice;
  final Set<SurveyChoice> choices;
  bool allowNewChoicesCreation;

  Survey({
    required this.id,
    required this.userID,
    required this.postID,
    required this.creationTime,
    required this.question,
    required this.userChoice,
    required this.choices,
    required this.allowNewChoicesCreation,
  }) : assert(choices.length > 0);

  bool get hasResponded => this.userChoice > 0;

  bool get hasResponses =>
      this.choices.where((f) => f.responses > 0).length > 0;

  bool get canBlockNewChoicesCreation =>
      allowNewChoicesCreation && account.userID() == this.userID;

  SurveyChoice? get userResponse {
    if (!hasResponded) return null;

    return choices.firstWhere((e) => e.id == userChoice);
  }

  void cancelUserResponse() {
    if (hasResponded) userResponse!.responses--;
    userChoice = 0;
  }

  void setUserResponse(SurveyChoice choice) {
    cancelUserResponse();
    userChoice = choice.id;
    choice.responses++;
  }
}

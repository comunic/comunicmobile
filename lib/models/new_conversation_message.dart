import 'package:comunic/models/api_request.dart';

/// New conversation message model
///
/// This model is used to transfer a conversation message to send in the application
///
/// @author Pierre HUBERT

class NewConversationMessage {
  final int conversationID;
  final String? message;
  final BytesFile? file;
  final BytesFile? thumbnail;

  NewConversationMessage({
    required this.conversationID,
    required this.message,
    this.file,
    this.thumbnail,
  }) : assert(file != null || message != null);

  bool get hasMessage => message != null;

  bool get hasFile => file != null;

  bool get hasThumbnail => thumbnail != null;
}

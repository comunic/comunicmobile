/// Count the number of unread notifications
///
/// @author Pierre Hubert

class CountUnreadNotifications {
  int? notifications;
  int? conversations;

  CountUnreadNotifications({
    required int this.notifications,
    required int this.conversations,
  });
}



/// Application settings
///
/// @author Pierre Hubert

class ApplicationPreferences {
  bool enableDarkMode;
  bool forceMobileMode;
  bool showPerformancesOverlay;

  ApplicationPreferences({
    required this.enableDarkMode,
    required this.forceMobileMode,
    required this.showPerformancesOverlay,
  }) ;
}

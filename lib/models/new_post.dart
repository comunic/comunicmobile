import 'package:comunic/enums/post_kind.dart';
import 'package:comunic/enums/post_target.dart';
import 'package:comunic/enums/post_visibility_level.dart';

import 'api_request.dart';

/// New post information
///
/// @author Pierre HUBERT

class NewSurvey {
  final String question;
  final Set<String> answers;
  final bool allowNewChoicesCreation;

  const NewSurvey({
    required this.question,
    required this.answers,
    required this.allowNewChoicesCreation,
  }) : assert(answers.length > 1);
}

class NewPost {
  final PostTarget target;
  final int targetID;
  final PostVisibilityLevel visibility;
  final String content;
  final BytesFile? image;
  final String? url;
  final List<int>? pdf;
  final PostKind kind;
  final DateTime? timeEnd;
  final NewSurvey? survey;
  final String? youtubeId;

  const NewPost({
    required this.target,
    required this.targetID,
    required this.visibility,
    required this.content,
    required this.kind,
    required this.image,
    required this.url,
    required this.pdf,
    required this.timeEnd,
    required this.survey,
    required this.youtubeId,
  })  : assert(kind != PostKind.TEXT || content.length > 3),
        assert(kind != PostKind.IMAGE || image != null),
        assert(kind != PostKind.WEB_LINK || url != null),
        assert(kind != PostKind.PDF || pdf != null),
        assert(kind != PostKind.COUNTDOWN || timeEnd != null),
        assert(kind != PostKind.SURVEY || survey != null),
        assert(kind != PostKind.YOUTUBE || youtubeId != null);
}

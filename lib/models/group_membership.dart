import 'package:comunic/models/group.dart';

/// Group membership information
///
/// @author Pierre Hubert

class GroupMembership {
  final int userID;
  final int groupID;
  final int timeCreate;
  final GroupMembershipLevel level;

  const GroupMembership({
    required this.userID,
    required this.groupID,
    required this.timeCreate,
    required this.level,
  });

  String get membershipText => membershipToText(level);

  bool get isAtLeastMember =>
      level == GroupMembershipLevel.ADMINISTRATOR ||
      level == GroupMembershipLevel.MODERATOR ||
      level == GroupMembershipLevel.MEMBER;

  bool get isPending => level == GroupMembershipLevel.PENDING;

  bool get isInvited => level == GroupMembershipLevel.INVITED;
}

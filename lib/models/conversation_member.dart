/// Conversation member
///
/// @author Pierre Hubert

class ConversationMember {
  final int userID;
  final int lastMessageSeen;
  final int lastAccessTime;
  final bool following;
  final bool isAdmin;

  const ConversationMember({
    required this.userID,
    required this.lastMessageSeen,
    required this.lastAccessTime,
    required this.following,
    required this.isAdmin,
  });

  Map<String, dynamic> toJson() => {
        'userID': userID,
        'lastMessageSeen': lastMessageSeen,
        'lastAccessTime': lastAccessTime,
        'following': following,
        'isAdmin': isAdmin,
      };

  ConversationMember.fromJSON(Map<String, dynamic> json)
      : userID = json["userID"],
        lastMessageSeen = json["lastMessageSeen"],
        lastAccessTime = json["lastAccessTime"],
        following = json["following"],
        isAdmin = json["isAdmin"];
}

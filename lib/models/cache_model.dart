import 'package:comunic/helpers/database/database_contract.dart';

/// Cache base model
///
/// @author Pierre HUBERT

abstract class CacheModel {
  final int id;

  const CacheModel({required this.id});

  /// Initialize a CacheModel from a map
  CacheModel.fromMap(Map<String, dynamic> map)
      : id = map[BaseTableContract.C_ID];

  /// Convert the object to a map
  Map<String, dynamic> toMap();
}

/// Security settings of the user
///
/// @author Pierre HUBERT

class SecuritySettings {
  final String securityQuestion1;
  final String securityAnswer1;
  final String securityQuestion2;
  final String securityAnswer2;

  const SecuritySettings({
    required this.securityQuestion1,
    required this.securityAnswer1,
    required this.securityQuestion2,
    required this.securityAnswer2,
  });
}

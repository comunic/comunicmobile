import 'package:flutter/cupertino.dart';

/// New conversation information
///
/// @author Pierre Hubert

class NewConversation {
  final String name;
  final List<int?> members;
  final bool follow;
  final bool canEveryoneAddMembers;
  final Color? color;

  const NewConversation({
    required this.name,
    required this.members,
    required this.follow,
    required this.canEveryoneAddMembers,
    required this.color,
  })  :
        assert(members.length > 0);
}

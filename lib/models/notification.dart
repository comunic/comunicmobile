/// Notification model
///
/// @author Pierre HUBERT

enum NotificationElementType {
  USER_PAGE,
  GROUP_PAGE,
  CONVERSATION,
  CONVERSATION_MESSAGE,
  POST,
  POST_TEXT,
  POST_IMAGE,
  POST_YOUTUBE,
  POST_WEBLINK,
  POST_PDF,
  POST_TIMER,
  POST_SURVEY,
  COMMENT,
  FRIENDSHIP_REQUEST,
  GROUP_MEMBERSHIP,
}

enum NotificationType {
  COMMENT_CREATED,
  SENT_FRIEND_REQUEST,
  ACCEPTED_FRIEND_REQUEST,
  REJECTED_FRIEND_REQUEST,
  ELEM_CREATED,
  ELEM_UPDATED,
  SENT_GROUP_MEMBERSHIP_INVITATION,
  ACCEPTED_GROUP_MEMBERSHIP_INVITATION,
  REJECTED_GROUP_MEMBERSHIP_INVITATION,
  SENT_GROUP_MEMBERSHIP_REQUEST,
  ACCEPTED_GROUP_MEMBERSHIP_REQUEST,
  REJECTED_GROUP_MEMBERSHIP_REQUEST,
}

class Notification {
  final int id;
  final int timeCreate;
  final bool seen;
  final int fromUser;
  final int onElemId;
  final NotificationElementType onElemType;
  final NotificationType type;
  final int? fromContainerId;
  final NotificationElementType? fromContainerType;

  const Notification({
    required this.id,
    required this.timeCreate,
    required this.seen,
    required this.fromUser,
    required this.onElemId,
    required this.onElemType,
    required this.type,
    required this.fromContainerId,
    required this.fromContainerType,
  });
}

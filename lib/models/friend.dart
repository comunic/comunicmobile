import 'package:comunic/helpers/database/database_contract.dart';
import 'package:comunic/models/cache_model.dart';
import 'package:comunic/utils/date_utils.dart';

/// Single user Friend information
///
/// @author Pierre HUBERT

class Friend extends CacheModel implements Comparable<Friend> {
  bool accepted;
  final int? lastActive;
  final bool following;
  final bool canPostTexts;

  Friend({
    required int id,
    required this.accepted,
    required int this.lastActive,
    required this.following,
    required this.canPostTexts,
  }) : super(id: id);

  /// Check out whether friend is connected or not
  bool get isConnected => time() - 30 < lastActive!;

  @override
  int compareTo(Friend other) => other.lastActive!.compareTo(lastActive!);

  @override
  Map<String, dynamic> toMap() => {
        FriendsListTableContract.C_ID: id.toString(),
        FriendsListTableContract.C_ACCEPTED: accepted ? 1 : 0,
        FriendsListTableContract.C_LAST_ACTIVE: lastActive,
        FriendsListTableContract.C_FOLLOWING: following ? 1 : 0,
        FriendsListTableContract.C_CAN_POST_TEXTS: canPostTexts ? 1 : 0
      };

  Friend.fromMap(Map<String, dynamic> map)
      : accepted = map[FriendsListTableContract.C_ACCEPTED] == 1,
        lastActive = map[FriendsListTableContract.C_LAST_ACTIVE],
        following = map[FriendsListTableContract.C_FOLLOWING] == 1,
        canPostTexts = map[FriendsListTableContract.C_CAN_POST_TEXTS] == 1,
        super.fromMap(map);
}

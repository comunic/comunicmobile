import 'package:flutter/widgets.dart';

/// Conversation settings update
///
/// @author Pierre Hubert

class NewConversationsSettings {
  final int convID;
  final bool following;
  final bool isComplete;
  final String name;
  final bool? canEveryoneAddMembers;
  final Color? color;

  const NewConversationsSettings({
    required this.convID,
    required this.following,
    required this.isComplete,
    required this.name,
    required this.canEveryoneAddMembers,
    required this.color,
  })  : assert(convID > 0),
        assert(!isComplete || canEveryoneAddMembers != null);
}

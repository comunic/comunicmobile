import 'package:comunic/enums/likes_type.dart';
import 'package:comunic/models/displayed_content.dart';
import 'package:comunic/models/like_element.dart';
import 'package:comunic/utils/account_utils.dart' as account;

/// Comments
///
/// Contains information about a single comment
///
/// @author Pierre HUBERT

class Comment implements LikeElement {
  final int id;
  final int userID;
  final int postID;
  final int timeSent;
  DisplayedString content;
  final String? imageURL;
  int likes;
  bool userLike;

  Comment({
    required this.id,
    required this.userID,
    required this.postID,
    required this.timeSent,
    required this.content,
    required this.imageURL,
    required this.likes,
    required this.userLike,
  });

  bool get hasContent => !content.isNull && content.length > 0;

  bool get hasImage => imageURL != null;

  bool get isOwner => userID == account.userID();

  @override
  LikesType get likeType => LikesType.COMMENT;
}

import 'package:comunic/ui/routes/tour_route.dart';
import 'package:flutter/material.dart';

/// Application configuration model
///
/// @author Pierre HUBERT

const defaultColor = Color(0xFF1A237E);

/// Configuration class
class Config {
  final String apiServerName;
  final String apiServerUri;
  final bool apiServerSecure;
  final String clientName;

  // Theme customization
  final Color splashBackgroundColor;
  final Color? primaryColor;
  final Color? primaryColorDark;
  final String appName;
  final String? appQuickDescription;
  final Color? unreadConversationColor;
  final Color? defaultConversationColor;

  // Entries for the welcome tour
  final TourEntriesBuilder? toursEntriesBuilder;

  // Custom initialization
  final Future<void> Function()? additionalLoading;

  // Custom main application route
  final Widget Function(BuildContext, GlobalKey)? mainRouteBuilder;

  const Config({
    required this.apiServerName,
    required this.apiServerUri,
    required this.apiServerSecure,
    required this.clientName,
    this.splashBackgroundColor = defaultColor,
    this.primaryColor,
    this.primaryColorDark,
    this.appName = "Comunic",
    this.appQuickDescription,
    this.defaultConversationColor,
    this.unreadConversationColor,
    this.toursEntriesBuilder,
    this.additionalLoading,
    this.mainRouteBuilder,
  });

  /// Get and set static configuration
  static Config? _config;

  static Config? get() {
    return _config;
  }

  static void set(Config conf) {
    _config = conf;
  }
}

/// Get the current configuration of the application
Config config() {
  return Config.get()!;
}

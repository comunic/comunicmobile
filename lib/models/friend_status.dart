/// Simple friendship status
///
/// @author Pierre HUBERT

class FriendStatus {
  final int userID;
  final bool areFriend;
  final bool sentRequest;
  final bool receivedRequest;
  final bool following;

  const FriendStatus({
    required this.userID,
    required this.areFriend,
    required this.sentRequest,
    required this.receivedRequest,
    required this.following,
  });

  bool get noRequestExchanged => !areFriend && !sentRequest && !receivedRequest;
}

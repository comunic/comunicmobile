import 'dart:convert';

/// API response
///
/// @author Pierre HUBERT

class APIResponse {
  final int code;
  final String? content;

  const APIResponse(this.code, this.content);

  List<dynamic>? getArray() => jsonDecode(this.content!);

  Map<String, dynamic> getObject() => jsonDecode(this.content!);

  /// Check if the request is successful or not
  bool get isOK => code == 200;

  /// Make sure the request succeed, or throw an exception else
  APIResponse assertOk() {

    if(!this.isOK)
      throw Exception("Request failed with status $code -> $content");

    return this;
  }
}

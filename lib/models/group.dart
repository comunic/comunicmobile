import 'package:comunic/utils/intl_utils.dart';

/// Group information
///
/// @author Pierre HUBERT

enum GroupMembershipLevel {
  ADMINISTRATOR,
  MODERATOR,
  MEMBER,
  INVITED,
  PENDING,
  VISITOR
}

String membershipToText(GroupMembershipLevel level) {
  switch (level) {
    case GroupMembershipLevel.ADMINISTRATOR:
      return tr("Administrator")!;
    case GroupMembershipLevel.MODERATOR:
      return tr("Moderator")!;
    case GroupMembershipLevel.MEMBER:
      return tr("Member")!;
    case GroupMembershipLevel.INVITED:
      return tr("Invited")!;
    case GroupMembershipLevel.PENDING:
      return tr("Requested")!;
    case GroupMembershipLevel.VISITOR:
      return tr("Visitor")!;
  }
}

enum GroupVisibilityLevel { OPEN, PRIVATE, SECRETE }

enum GroupRegistrationLevel { OPEN, MODERATED, CLOSED }

enum GroupPostCreationLevel { MODERATORS, MEMBERS }

class Group implements Comparable<Group> {
  final int id;
  String name;
  final String iconURL;
  final int numberMembers;
  GroupMembershipLevel membershipLevel;
  GroupVisibilityLevel visibilityLevel;
  GroupRegistrationLevel registrationLevel;
  GroupPostCreationLevel postCreationLevel;
  String virtualDirectory;
  bool following;

  Group({
    required this.id,
    required this.name,
    required this.iconURL,
    required this.numberMembers,
    required this.membershipLevel,
    required this.visibilityLevel,
    required this.registrationLevel,
    required this.postCreationLevel,
    required this.virtualDirectory,
    required this.following,
  });

  get displayName => this.name;

  bool get isAtLeastMember =>
      membershipLevel == GroupMembershipLevel.ADMINISTRATOR ||
      membershipLevel == GroupMembershipLevel.MODERATOR ||
      membershipLevel == GroupMembershipLevel.MEMBER;

  bool get isAdmin => membershipLevel == GroupMembershipLevel.ADMINISTRATOR;

  bool get isAtLeastModerator =>
      membershipLevel == GroupMembershipLevel.ADMINISTRATOR ||
      membershipLevel == GroupMembershipLevel.MODERATOR;

  bool get canCreatePost =>
      membershipLevel == GroupMembershipLevel.ADMINISTRATOR ||
      membershipLevel == GroupMembershipLevel.MODERATOR ||
      (membershipLevel == GroupMembershipLevel.MEMBER &&
          postCreationLevel == GroupPostCreationLevel.MEMBERS);

  String get membershipText => membershipToText(membershipLevel);

  @override
  int compareTo(Group other) => id.compareTo(other.id);
}

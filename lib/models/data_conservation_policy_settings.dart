/// Data conservation policy settings
///
/// @author Pierre Hubert

class DataConservationPolicySettings {
  int? inactiveAccountLifeTime;
  int? notificationLifetime;
  int? commentsLifetime;
  int? postsLifetime;
  int? conversationMessagesLifetime;
  int? likesLifetime;

  DataConservationPolicySettings({
    this.inactiveAccountLifeTime,
    this.notificationLifetime,
    this.commentsLifetime,
    this.postsLifetime,
    this.conversationMessagesLifetime,
    this.likesLifetime,
  });
}

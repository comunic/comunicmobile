/// Single custom emoji information
///
/// @author Pierre Hubert

class CustomEmoji {
  final int? id;
  final int? userID;
  final String? shortcut;
  final String? url;

  const CustomEmoji({
    required int this.id,
    required int this.userID,
    required String this.shortcut,
    required String this.url,
  });

  Map<String, dynamic> toMap() => {
        "id": id,
        "userID": userID,
        "shortcut": shortcut,
        "url": url,
      };

  CustomEmoji.fromMap(Map<String, dynamic> map)
      : id = map["id"],
        userID = map["userID"],
        shortcut = map["shortcut"],
        url = map["url"];
}

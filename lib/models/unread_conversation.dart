import 'package:comunic/models/conversation.dart';
import 'package:comunic/models/conversation_message.dart';

/// Unread conversation information
///
/// @author Pierre Hubert

class UnreadConversation {
  final Conversation conv;
  final ConversationMessage message;

  const UnreadConversation({
    required this.conv,
    required this.message,
  });
}

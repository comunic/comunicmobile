/// Call configuration
///
/// @author Pierre Hubert

class CallConfig {
  final List<String> iceServers;

  const CallConfig({
    required this.iceServers,
  });

  /// Turn this call configuration into the right for the WebRTC plugin
  Map<String, dynamic> get pluginConfig => {
        "iceServers": iceServers.map((f) => {"url": f}).toList(),
        "sdpSemantics": "unified-plan",
      };
}

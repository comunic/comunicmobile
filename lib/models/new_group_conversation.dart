import 'package:comunic/models/group.dart';

/// This class contains information about a conversation linked to a group
/// to create
///
/// @author Pierre Hubert

class NewGroupConversation {
  final int groupID;
  final String name;
  final GroupMembershipLevel minMembershipLevel;

  const NewGroupConversation({
    required this.groupID,
    required this.name,
    required this.minMembershipLevel,
  });
}

/// Account image settings
///
/// @author Pierre Hubert

enum AccountImageVisibilityLevels { EVERYONE, COMUNIC_USERS, FRIENDS_ONLY }

class AccountImageSettings {
  final bool hasImage;
  final String imageURL;
  final AccountImageVisibilityLevels visibility;

  const AccountImageSettings({
    required this.hasImage,
    required this.imageURL,
    required this.visibility,
  });
}

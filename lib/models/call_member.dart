import 'package:flutter_webrtc/flutter_webrtc.dart';

/// Single call member information
///
/// @author Pierre Hubert

enum MemberStatus { JOINED, READY }

class CallMember {
  final int userID;
  MemberStatus status;
  MediaStream? stream;

  CallMember({
    required this.userID,
    this.status = MemberStatus.JOINED,
  });

  bool get hasVideoStream =>
      stream != null && stream!.getVideoTracks().length > 0;
}

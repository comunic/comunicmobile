import 'package:comunic/enums/user_page_visibility.dart';

/// General settings
///
/// @author Pierre Hubert

class GeneralSettings {
  final String email;
  String firstName;
  String lastName;
  UserPageVisibility pageVisibility;
  bool allowComments;
  bool allowPostsFromFriends;
  bool allowComunicEmails;
  bool publicFriendsList;
  bool publicEmail;
  String virtualDirectory;
  String personalWebsite;
  String publicNote;
  String? location;

  GeneralSettings({
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.pageVisibility,
    required this.allowComments,
    required this.allowPostsFromFriends,
    required this.allowComunicEmails,
    required this.publicFriendsList,
    required this.publicEmail,
    required this.virtualDirectory,
    required this.personalWebsite,
    required this.publicNote,
    required this.location,
  });
}

import 'package:comunic/utils/date_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:version/version.dart';

/// Server static configuration
///
/// @author Pierre Hubert

class NotificationsPolicy {
  final bool hasFirebase;
  final bool hasIndependent;

  const NotificationsPolicy({
    required this.hasFirebase,
    required this.hasIndependent,
  });
}

class PasswordPolicy {
  final bool allowMailInPassword;
  final bool allowNameInPassword;
  final int minPasswordLength;
  final int minNumberUpperCaseLetters;
  final int minNumberLowerCaseLetters;
  final int minNumberDigits;
  final int minNumberSpecialCharacters;
  final int minCategoriesPresence;

  const PasswordPolicy({
    required this.allowMailInPassword,
    required this.allowNameInPassword,
    required this.minPasswordLength,
    required this.minNumberUpperCaseLetters,
    required this.minNumberLowerCaseLetters,
    required this.minNumberDigits,
    required this.minNumberSpecialCharacters,
    required this.minCategoriesPresence,
  });
}

class ServerDataConservationPolicy {
  final int minInactiveAccountLifetime;
  final int minNotificationLifetime;
  final int minCommentsLifetime;
  final int minPostsLifetime;
  final int minConversationMessagesLifetime;
  final int minLikesLifetime;

  const ServerDataConservationPolicy({
    required this.minInactiveAccountLifetime,
    required this.minNotificationLifetime,
    required this.minCommentsLifetime,
    required this.minPostsLifetime,
    required this.minConversationMessagesLifetime,
    required this.minLikesLifetime,
  });
}

class ConversationsPolicy {
  final int maxConversationNameLen;
  final int minMessageLen;
  final int maxMessageLen;
  final List<String> allowedFilesType;
  final int filesMaxSize;
  final int writingEventInterval;
  final int writingEventLifetime;
  final int maxMessageImageWidth;
  final int maxMessageImageHeight;
  final int maxThumbnailWidth;
  final int maxThumbnailHeight;
  final int maxLogoWidth;
  final int maxLogoHeight;

  const ConversationsPolicy({
    required this.maxConversationNameLen,
    required this.minMessageLen,
    required this.maxMessageLen,
    required this.allowedFilesType,
    required this.filesMaxSize,
    required this.writingEventInterval,
    required this.writingEventLifetime,
    required this.maxMessageImageWidth,
    required this.maxMessageImageHeight,
    required this.maxThumbnailWidth,
    required this.maxThumbnailHeight,
    required this.maxLogoWidth,
    required this.maxLogoHeight,
  });
}

class AccountInformationPolicy {
  final int minFirstNameLength;
  final int maxFirstNameLength;
  final int minLastNameLength;
  final int maxLastNameLength;
  final int maxLocationLength;

  const AccountInformationPolicy({
    required this.minFirstNameLength,
    required this.maxFirstNameLength,
    required this.minLastNameLength,
    required this.maxLastNameLength,
    required this.maxLocationLength,
  });
}

enum BannerNature { Information, Warning, Success }

extension BannerNatureExt on BannerNature {
  static BannerNature fromStr(String? s) {
    switch (s) {
      case "information":
        return BannerNature.Information;
      case "success":
        return BannerNature.Success;
      case "warning":
      default:
        return BannerNature.Warning;
    }
  }
}

class Banner {
  final bool enabled;
  final int? expire;
  final BannerNature nature;
  final Map<String, String> message;
  final String? link;

  const Banner({
    required this.enabled,
    required this.expire,
    required this.nature,
    required this.message,
    required this.link,
  });

  bool get visible => enabled && (expire == null || expire! > time());
}

class ReportCause {
  final String id;
  final Map<String, String> label;

  const ReportCause({required this.id, required this.label});

  String get localeLabel =>
      label.containsKey(shortLang) ? label[shortLang]! : label["en"]!;
}

class ReportPolicy {
  final List<ReportCause> causes;
  final int maxCommentLength;
  final bool canUserReportHisOwnContent;

  const ReportPolicy({
    required this.causes,
    required this.maxCommentLength,
    required this.canUserReportHisOwnContent,
  });
}

class ServerConfig {
  final Version minSupportedMobileVersion;
  final String termsURL;
  final String privacyPolicyURL;
  final String contactEmail;
  final String playStoreURL;
  final String androidDirectDownloadURL;
  final Banner? banner;
  final NotificationsPolicy notificationsPolicy;
  final PasswordPolicy passwordPolicy;
  final ServerDataConservationPolicy dataConservationPolicy;
  final ConversationsPolicy conversationsPolicy;
  final AccountInformationPolicy accountInformationPolicy;
  final ReportPolicy? reportPolicy;

  const ServerConfig({
    required this.minSupportedMobileVersion,
    required this.termsURL,
    required this.privacyPolicyURL,
    required this.contactEmail,
    required this.playStoreURL,
    required this.androidDirectDownloadURL,
    required this.banner,
    required this.notificationsPolicy,
    required this.passwordPolicy,
    required this.dataConservationPolicy,
    required this.conversationsPolicy,
    required this.accountInformationPolicy,
    required this.reportPolicy,
  });

  bool get isReportingEnabled => this.reportPolicy != null;
}



/// Authentication details
///
/// @author Pierre HUBERT

class AuthenticationDetails {
  final String email;
  final String password;

  const AuthenticationDetails({required this.email, required this.password});
}

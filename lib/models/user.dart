import 'dart:convert';

import 'package:comunic/enums/user_page_visibility.dart';
import 'package:comunic/helpers/serialization/base_serialization_helper.dart';
import 'package:comunic/lists/custom_emojies_list.dart';
import 'package:comunic/utils/account_utils.dart';
import 'package:comunic/utils/ui_utils.dart';

/// Single user information
///
/// @author Pierre HUBERT

class User implements SerializableElement<User> {
  final int id;
  final String firstName;
  final String lastName;
  final UserPageVisibility pageVisibility;
  final String? virtualDirectory;
  final String accountImageURL;
  final CustomEmojiesList customEmojies;

  const User({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.pageVisibility,
    required this.virtualDirectory,
    required this.accountImageURL,
    required this.customEmojies,
  }) : assert(id > 0);

  /// Get user full name
  String get fullName => firstName + " " + lastName;

  /// Get user display name
  String get displayName => htmlDecodeCharacters(fullName);

  bool get hasVirtualDirectory =>
      virtualDirectory != null && virtualDirectory!.length > 0;

  bool get isCurrentUser => id == userID();

  Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "pageVisibility": pageVisibility.toString(),
        "virtualDirectory": virtualDirectory,
        "accountImageURL": accountImageURL,
        "customEmojies": customEmojies.toSerializableList()
      };

  User.fromJson(Map<String, dynamic> map)
      : this.id = map["id"],
        this.firstName = map["firstName"],
        this.lastName = map["lastName"],
        this.pageVisibility = UserPageVisibility.values.firstWhere(
            (element) => element.toString() == map["pageVisibility"]),
        this.virtualDirectory = map["virtualDirectory"],
        this.accountImageURL = map["accountImageURL"],
        this.customEmojies = CustomEmojiesList.fromSerializedList(
            jsonDecode(map["customEmojies"]));

  @override
  int compareTo(User other) => id.compareTo(other.id);
}

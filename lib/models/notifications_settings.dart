/// Notifications settings
///
/// @author Pierre Hubert

class NotificationsSettings {
  bool allowConversations;
  bool allowNotificationsSound;

  NotificationsSettings({
    required this.allowConversations,
    required this.allowNotificationsSound,
  });
}

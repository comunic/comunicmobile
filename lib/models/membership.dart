import 'package:comunic/models/conversation.dart';
import 'package:comunic/models/friend.dart';

/// Membership information
///
/// @author Pierre Hubert

enum MembershipType { FRIEND, GROUP, CONVERSATION }

class Membership {
  final MembershipType type;
  final Conversation? conversation;
  final Friend? friend;
  final int? groupID;
  final int? groupLastActive;

  Membership.conversation(Conversation this.conversation)
      : type = MembershipType.CONVERSATION,
        friend = null,
        groupID = null,
        groupLastActive = null;

  Membership.friend(Friend this.friend)
      : type = MembershipType.FRIEND,
        conversation = null,
        groupID = null,
        groupLastActive = null;

  Membership.group(
      {required int this.groupID, required int this.groupLastActive})
      : type = MembershipType.GROUP,
        conversation = null,
        friend = null;

  int? get lastActive {
    switch (type) {
      case MembershipType.FRIEND:
        return friend!.lastActive;
      case MembershipType.GROUP:
        return groupLastActive;
      case MembershipType.CONVERSATION:
        return conversation!.lastActivity;
      default:
        throw Exception("Unreachable statment!");
    }
  }
}

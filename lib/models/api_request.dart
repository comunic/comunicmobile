import 'dart:io';

import 'package:comunic/helpers/api_helper.dart';
import 'package:comunic/models/api_response.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';

/// API Request model
///
/// Contains all the information associated to an API request
///
/// @author Pierre HUBERT

class BytesFile {
  final String filename;
  final List<int>? bytes;
  final MediaType? type;

  const BytesFile(
    this.filename,
    this.bytes, {
    this.type,
  });
}

class APIRequest {
  final String uri;
  final bool needLogin;
  ProgressCallback? progressCallback;
  CancelToken? cancelToken;
  Map<String, String?>? args;
  Map<String, File> files = Map();
  Map<String, BytesFile?> bytesFiles = Map();

  APIRequest({required this.uri, this.needLogin = false, this.args}) {
    if (this.args == null) this.args = Map();
  }

  APIRequest.withLogin(this.uri, {this.args}) : needLogin = true {
    if (args == null) this.args = Map();
  }

  APIRequest.withoutLogin(this.uri, {this.args}) : needLogin = false {
    if (args == null) this.args = Map();
  }

  APIRequest addString(String name, String? value) {
    args![name] = value;
    return this;
  }

  APIRequest addInt(String name, int? value) {
    args![name] = value.toString();
    return this;
  }

  APIRequest addBool(String name, bool value) {
    args![name] = value ? "true" : "false";
    return this;
  }

  APIRequest addFile(String name, File file) {
    files[name] = file;
    return this;
  }

  APIRequest addBytesFile(String name, BytesFile? file) {
    this.bytesFiles[name] = file;
    return this;
  }

  void addArgs(Map<String, String> newArgs) => args!.addAll(newArgs);

  /// Execute the request
  Future<APIResponse> exec() async => APIHelper().exec(this);

  /// Execute the request, throws an exception in case of failure
  Future<APIResponse> execWithThrow() async => (await exec()).assertOk();

  /// Execute the request, throws an exception in case of failure
  Future<Map<String, dynamic>> execWithThrowGetObject() async =>
      (await execWithThrow()).getObject();

  /// Execute the request with files
  Future<APIResponse> execWithFiles() async => APIHelper().execWithFiles(this);

  /// Execute the request with files to send & throws in case of failure
  Future<APIResponse> execWithFilesAndThrow() async =>
      (await execWithFiles()).assertOk();
}

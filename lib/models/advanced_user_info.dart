import 'package:comunic/enums/likes_type.dart';
import 'package:comunic/enums/user_page_visibility.dart';
import 'package:comunic/lists/custom_emojies_list.dart';
import 'package:comunic/models/like_element.dart';
import 'package:comunic/models/user.dart';

/// Advanced user information
///
/// @author Pierre HUBERT

class AdvancedUserInfo extends User implements LikeElement {
  final String? emailAddress;
  final String publicNote;
  final bool canPostTexts;
  final bool isFriendsListPublic;
  final int numberFriends;
  final int accountCreationTime;
  final String personalWebsite;
  final String? location;
  bool userLike;
  int likes;

  AdvancedUserInfo({
    required int id,
    required String firstName,
    required String lastName,
    required UserPageVisibility pageVisibility,
    required String? virtualDirectory,
    required String accountImageURL,
    required CustomEmojiesList customEmojies,
    required this.emailAddress,
    required this.publicNote,
    required this.canPostTexts,
    required this.isFriendsListPublic,
    required this.numberFriends,
    required this.accountCreationTime,
    required this.personalWebsite,
    required this.location,
    required this.userLike,
    required this.likes,
  }) : super(
            id: id,
            firstName: firstName,
            lastName: lastName,
            pageVisibility: pageVisibility,
            virtualDirectory: virtualDirectory,
            accountImageURL: accountImageURL,
            customEmojies: customEmojies);

  bool get hasPublicNote => publicNote.isNotEmpty;

  bool get hasPersonalWebsite => personalWebsite.isNotEmpty;

  bool get hasEmailAddress => emailAddress != null && emailAddress!.isNotEmpty;

  bool get hasLocation => location != null && location!.isNotEmpty;

  @override
  LikesType get likeType => LikesType.USER;
}

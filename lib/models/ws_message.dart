import 'dart:convert';

/// WebSocket message
///
/// @author Pierre Hubert

class WsMessage {
  final String id;
  final String title;
  final dynamic data;

  const WsMessage({
    required this.id,
    required this.title,
    required this.data,
  }) : assert(title.length > 0);

  /// Construct a message from a JSON document (messages coming from the server)
  static WsMessage fromJSON(final Map<dynamic, dynamic> m) {
    return WsMessage(id: m["id"] ?? "", title: m["title"], data: m["data"]);
  }

  /// Turn a message into a JSON object to send it to the API
  String toJSON() => jsonEncode({
        "id": id,
        "title": title,
        "data": data,
      });

  bool get hasId => this.id.isNotEmpty;
}

/// Single presence information
///
/// @author Pierre Hubert

class Presence {
  final int userID;
  final int year;
  final int month;
  final int day;

  const Presence({
    required this.userID,
    required this.year,
    required this.month,
    required this.day,
  });

  Presence.fromDateTime(DateTime dt, this.userID)
      : year = dt.year,
        month = dt.month,
        day = dt.day;
}

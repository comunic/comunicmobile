import 'package:comunic/lists/base_set.dart';
import 'package:comunic/models/forez_presence.dart';

/// Forez presence set
///
/// @author Pierre Hubert

class PresenceSet extends BaseSet<Presence> {
  /// Get the presence of a specific user
  PresenceSet getForUser(int? userID) =>
      PresenceSet()..addAll(where((element) => element.userID == userID));

  bool containsDate(DateTime dt) => any(
        (element) =>
            element.year == dt.year &&
            element.month == dt.month &&
            element.day == dt.day,
      );

  void removeDate(DateTime dt) => removeWhere(
        (element) =>
            element.year == dt.year &&
            element.month == dt.month &&
            element.day == dt.day,
      );

  void toggleDate(DateTime dt, int? userID) {
    if (containsDate(dt))
      removeDate(dt);
    else
      add(Presence.fromDateTime(dt, userID!));
  }

  int countAtDate(DateTime dt) => where(
        (element) =>
            element.year == dt.year &&
            element.month == dt.month &&
            element.day == dt.day,
      ).length;

  /// Get the list of users present at a specified date
  List<int> getUsersAtDate(DateTime dt) => where(
        (element) =>
            element.year == dt.year &&
            element.month == dt.month &&
            element.day == dt.day,
      ).map((e) => e.userID).toList();

  /// Get the ID of all the users referenced in this set
  Set<int> get usersID => map((element) => element.userID).toSet();
}

import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/conversation_message.dart';

/// Conversations messages list
///
/// @author Pierre HUBERT

class ConversationMessagesList extends AbstractList<ConversationMessage> {
  /// Get the list of the users ID who own a message in this list
  Set<int?> getUsersID() {
    final Set<int?> users = Set();

    for (ConversationMessage message in this) users.addAll(message.usersID);

    return users;
  }

  /// Get the ID of the last message present in this list
  int? get lastMessageID {
    int? lastMessageID = 0;
    for (ConversationMessage message in this)
      if (message.id! > lastMessageID!) lastMessageID = message.id;
    return lastMessageID;
  }

  /// Get the ID of the first message present in this list
  int? get firstMessageID {
    int? firstMessageID = this[0].id;
    for (ConversationMessage message in this)
      if (message.id! < firstMessageID!) firstMessageID = message.id;
    return firstMessageID;
  }

  /// Replace a message by another one (if any)
  void replace(ConversationMessage msg) {
    final index = this.indexWhere((t) => t.id == msg.id);
    if (index >= 0) this[index] = msg;
  }

  /// Remove a message from this list
  void removeMsg(int? id) => removeWhere((f) => f.id == id);
}

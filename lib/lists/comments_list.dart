import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/comment.dart';

/// Comments list
///
/// Contains the list of comments for a post
///
/// @author Pierre HUBERT

class CommentsList extends AbstractList<Comment> {
  /// Get the list of users in this comments, as a set
  Set<int> get usersID => map((f) => f.userID).toSet();
}

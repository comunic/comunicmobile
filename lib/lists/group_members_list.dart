import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/group_membership.dart';

/// Group members list
///
/// @author Pierre Hubert

class GroupMembersList extends AbstractList<GroupMembership> {
  /// Get the list of users in this set
  Set<int> get usersID => map((f) => f.userID).toSet();
}

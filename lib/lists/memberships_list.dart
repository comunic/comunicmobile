import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/membership.dart';

/// Memberships list
///
/// @author Pierre Hubert

class MembershipList extends AbstractList<Membership?> {
  /// Get the IDs of all the users included in some way in this list
  Set<int?> get usersId {
    final s = Set<int?>();

    forEach((m) {
      switch (m!.type) {
        case MembershipType.FRIEND:
          s.add(m.friend!.id);
          break;
        case MembershipType.GROUP:
          break;
        case MembershipType.CONVERSATION:
          s.addAll(m.conversation!.membersID);
          break;
      }
    });

    return s;
  }

  /// Get the ID of the groups included in this list
  Set<int?> get groupsId => where((f) => f!.type == MembershipType.GROUP)
      .map((f) => f!.groupID)
      .toSet();

  /// Remove a friend membership from the list
  void removeFriend(int friendID) => remove(firstWhere(
      (f) => f!.type == MembershipType.FRIEND && f.friend!.id == friendID));

  /// Get the list of conversations of a group
  Set<Membership?> getGroupConversations(int groupID) => where((element) =>
      element!.type == MembershipType.CONVERSATION &&
      element.conversation!.groupID == groupID).toSet();
}

import 'dart:collection';

/// Abstract list
///
/// @author Pierre HUBERT

class AbstractList<E> extends ListBase<E> {
  final _list = <E>[];

  int get length => _list.length;

  set length(int l) => _list.length = l;

  @override
  E operator [](int index) => _list[index];

  @override
  void operator []=(int index, E value) => _list[index] = value;

  @override
  void add(E element) => _list.add(element);
}

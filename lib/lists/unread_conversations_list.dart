import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/unread_conversation.dart';

/// List of unread conversations
///
/// @author Pierre Hubert

class UnreadConversationsList extends AbstractList<UnreadConversation> {
  /// Get the ID of the users included in this list
  Set<int?> get usersID {
    final set = Set<int?>();
    forEach((element) {
      set.addAll(element.conv.membersID);
      set.addAll(element.message.usersID);
    });
    return set;
  }

  /// Get the ID of the groups references ind this list
  Set<int?> get groupsID {
    final set = Set<int?>();
    forEach((element) {
      if (element.conv.isGroupConversation) set.add(element.conv.groupID);
    });
    return set;
  }
}

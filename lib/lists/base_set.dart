import 'dart:collection';

/// Base set
///
/// @author pierre Hubert

class BaseSet<T> extends SetBase<T> {
  final _set = new Set<T>();

  @override
  bool add(T value) => _set.add(value);

  @override
  bool contains(Object? element) => _set.contains(element);

  @override
  Iterator<T> get iterator => _set.iterator;

  @override
  int get length => _set.length;

  @override
  T? lookup(Object? element) => _set.lookup(element);

  @override
  bool remove(Object? value) => _set.remove(value);

  @override
  Set<T> toSet() => _set.toSet();
}

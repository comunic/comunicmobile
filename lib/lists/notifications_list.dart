import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/notification.dart';

/// Notifications list
///
/// @author Pierre HUBERT

class NotificationsList extends AbstractList<Notification> {
  /// Get the ID of all the users related to the notifications
  /// included in the list
  Set<int?> get usersIds {
    final list = Set<int?>();

    forEach((n) {
      list.add(n.fromUser);

      if (n.onElemType == NotificationElementType.FRIENDSHIP_REQUEST ||
          n.onElemType == NotificationElementType.USER_PAGE)
        list.add(n.onElemId);

      if (n.fromContainerType == NotificationElementType.FRIENDSHIP_REQUEST ||
          n.fromContainerType == NotificationElementType.USER_PAGE)
        list.add(n.fromContainerId);
    });

    return list;
  }

  /// Get the ID of all the groups related to the notifications
  /// included in the list
  Set<int?> get groupsIds {
    final list = Set<int?>();

    forEach((n) {
      if (n.onElemType == NotificationElementType.GROUP_PAGE)
        list.add(n.onElemId);

      if (n.fromContainerType == NotificationElementType.GROUP_PAGE)
        list.add(n.fromContainerId);

      if (n.onElemType == NotificationElementType.GROUP_MEMBERSHIP)
        list.add(n.onElemId);
    });

    return list;
  }
}

import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/call_member.dart';

/// Call members list
///
/// @author Pierre Hubert

class CallMembersList extends AbstractList<CallMember> {
  /// Get the IDs of the users in this list
  Set<int> get usersID => this.map((f) => f.userID).toSet();

  /// Remove a specific member from this list
  void removeUser(int? userID) => this.removeWhere((f) => f.userID == userID);

  /// Get the connection of a specific user
  CallMember getUser(int? userID) => this.firstWhere((f) => f.userID == userID);

  /// Extract ready peers from this list
  CallMembersList get readyPeers =>
      CallMembersList()..addAll(where((f) => f.status == MemberStatus.READY));
}

import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/friend.dart';

/// List of friends of the user
///
/// @author Pierre HUBERT

class FriendsList extends AbstractList<Friend> {
  /// Get the ID of all the friends of the current user
  List<int> get usersId => map((f) => f.id).toList();
}

import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/custom_emoji.dart';

/// Custom emojies list
///
/// @author Pierre HUBERT

class CustomEmojiesList extends AbstractList<CustomEmoji> {
  /// Check if an emoji, identified by its shortcut, is present in this list
  bool hasShortcut(String shortcut) => any((f) => f.shortcut == shortcut);

  /// Serialize this list
  List<Map<String, dynamic>> toSerializableList() =>
      map((f) => f.toMap()).toList();

  /// Un-serialize this list
  static CustomEmojiesList fromSerializedList(List<dynamic> list) =>
      CustomEmojiesList()
        ..addAll(list.map((f) => CustomEmoji.fromMap(f)).toList());
}

import 'dart:collection';

import 'package:comunic/models/group.dart';

/// Groups list
///
/// @author Pierre HUBERT

class GroupsList extends MapBase<int?, Group> {
  final Map<int?, Group?> _groups = Map();

  @override
  Group? operator [](Object? key) => _groups[key];

  @override
  void operator []=(int? key, Group? value) => _groups[key] = value;

  @override
  void clear() => _groups.clear();

  @override
  Iterable<int?> get keys => _groups.keys;

  @override
  Group? remove(Object? key) => _groups.remove(key);

  Group? getGroup(int? id) => this[id];
}

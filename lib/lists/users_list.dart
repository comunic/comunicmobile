import 'dart:collection';

import 'package:comunic/models/user.dart';

/// Users list
///
/// @author Pierre HUBERT

class UsersList extends ListBase<User> {
  List<User> _list = [];

  int get length => _list.length;

  set length(l) => _list.length = l;

  @override
  User operator [](int index) {
    return _list[index];
  }

  @override
  void operator []=(int index, User value) {
    _list[index] = value;
  }

  /// Find a user with a specific ID
  User getUser(int? userID) {
    for (int i = 0; i < this.length; i++)
      if (this[i].id == userID) return this[i];

    throw "User not found in the list!";
  }

  /// Check if the user is included in this list or not
  bool hasUser(int? userID) => any((f) => f.id == userID);

  /// Get the list of users ID present in this list
  List<int?> get usersID => List.generate(length, (i) => this[i].id);

  @override
  void add(User element) => _list.add(element);
}

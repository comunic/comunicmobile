import 'dart:collection';

import 'package:comunic/models/conversation.dart';

/// Conversations list
///
/// @author Pierre HUBERT

class ConversationsList extends ListBase<Conversation> {
  final List<Conversation> _list = [];

  set length(l) => _list.length = l;

  int get length => _list.length;

  @override
  Conversation operator [](int index) => _list[index];

  @override
  void operator []=(int index, Conversation value) => _list[index] = value;

  /// Get the entire lists of users ID in this list
  Set<int> get allUsersID {
    final Set<int> list = Set();
    forEach((c) => c.members!.forEach((member) => list.add(member.userID)));
    return list;
  }

  /// Get the entire lists of groups ID in this list
  Set<int?> get allGroupsID => where((element) => element.isGroupConversation)
      .map((e) => e.groupID)
      .toSet();

  @override
  void add(Conversation element) => _list.add(element);
}

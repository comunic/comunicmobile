import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/search_result.dart';

/// List of result of a global search on the database
///
/// @author Pierre Hubert

class SearchResultsList extends AbstractList<SearchResult> {
  /// Get the list of users included in this search result
  Set<int> get usersId => this
      .where((f) => f.kind == SearchResultKind.USER)
      .map((f) => f.id)
      .toSet();

  /// Get the list of groups included in this search result
  Set<int> get groupsId => this
      .where((f) => f.kind == SearchResultKind.GROUP)
      .map((f) => f.id)
      .toSet();
}

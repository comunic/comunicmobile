import 'package:comunic/lists/abstract_list.dart';
import 'package:comunic/models/post.dart';

/// Posts List
///
/// Contains method to easily process a list of posts
///
/// @author Pierre HUBERT

class PostsList extends AbstractList<Post> {
  // Get the list of users ID in this set
  Set<int?> get usersID {
    Set<int?> set = Set();

    forEach((p) {
      set.add(p.userID);

      if (p.userPageID != null && p.userPageID! > 0) set.add(p.userPageID);

      if (p.comments != null) set.addAll(p.comments!.usersID);
    });

    return set;
  }

  /// Get the list of groups in this list of posts
  Set<int?> get groupsID {
    Set<int?> set = Set();

    forEach((p) {
      if (p.isGroupPost) set.add(p.groupID);
    });

    return set;
  }

  /// Get the ID of the oldest post of this list. Returns 0 if the list is empty
  int get oldestID {
    if (isEmpty) return 0;

    return this.elementAt(length - 1).id;
  }
}

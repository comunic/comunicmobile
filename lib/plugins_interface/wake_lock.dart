import 'package:wakelock/wakelock.dart';

/// Wake lock plugin interface
///
/// @author Pierre Hubert

/// Interface for requesting device wake locking
Future<void> setWakeLock(bool enabled) async =>
    await Wakelock.toggle(enable: enabled);

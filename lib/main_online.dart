import 'package:comunic/main.dart';
import 'package:comunic/models/config.dart';

/// Online (communiquons.org) build configuration for the project
///
/// @author Pierre HUBERT

void main() {
  Config.set(Config(
    apiServerName: "api.communiquons.org",
    apiServerUri: "/",
    apiServerSecure: true,
    clientName: "ComunicFlutter",
  ));

  subMain();
}

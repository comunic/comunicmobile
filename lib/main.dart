import 'package:comunic/helpers/account_helper.dart';
import 'package:comunic/helpers/database/database_helper.dart';
import 'package:comunic/helpers/preferences_helper.dart';
import 'package:comunic/helpers/serialization/user_list_serialization_helper.dart';
import 'package:comunic/helpers/version_helper.dart';
import 'package:comunic/models/config.dart';
import 'package:comunic/ui/widgets/init_widget.dart';
import 'package:comunic/utils/flutter_utils.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// Main file of the application
///
/// @author Pierre HUBERT

void subMain() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Load package information
  await VersionHelper.ensureLoaded();

  // Connect to database
  if (!isWeb) {
    await DatabaseHelper.open();

    if (await Connectivity().checkConnectivity() != ConnectivityResult.none)
      await UsersListSerialisationHelper().removeAll();
  }

  // Get current system language
  await initTranslations();

  // Check if the user is currently signed in
  await AccountHelper().signedIn();

  runApp(ComunicApplication(
    preferences: await PreferencesHelper.getInstance(),
  ));
}

class ComunicApplication extends StatefulWidget {
  final PreferencesHelper preferences;

  const ComunicApplication({
    Key? key,
    required this.preferences,
  })  : super(key: key);

  @override
  ComunicApplicationState createState() => ComunicApplicationState();
}

class ComunicApplicationState extends State<ComunicApplication> {
  /// Use this method to force the application to rebuild
  void refresh() => setState(() {});

  @override
  Widget build(BuildContext context) {
    final prefs = widget.preferences.preferences;

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: InitializeWidget(),
      theme: (prefs.enableDarkMode ? ThemeData.dark() : ThemeData.fallback())
          .copyWith(
              primaryColor: config().primaryColor,
              primaryColorDark: config().primaryColorDark,
              appBarTheme: AppBarTheme(
                backgroundColor: config().primaryColor,
                systemOverlayStyle: SystemUiOverlayStyle.light,
              )),
      showPerformanceOverlay: prefs.showPerformancesOverlay,
    );
  }
}

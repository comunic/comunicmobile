import 'package:comunic/helpers/account_helper.dart';

/// Account utilities
///
/// @author Pierre HUBERT

/// Get the ID of the current user
int? userID() {
  return AccountHelper.getCurrentUserID();
}
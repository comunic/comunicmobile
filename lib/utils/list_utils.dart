/// List utilities
///
/// @author Pierre HUBERT

/// Transform a list of dynamic thins into something a list of ints
List<int> listToIntList(List<dynamic> srcList) {
  List<int> list = [];

  srcList.forEach((e) {
    list.add(int.parse(e));
  });

  return list;
}

/// Find the list of missing elements of a [testList] from a [srcList]
List<T> findMissingFromList<T>(List<T> srcList, List<T> testList) {
  List<T> dest = [];

  testList.forEach((f) {
    if (!srcList.contains(f) && !dest.contains(f)) dest.add(f);
  });

  return dest;
}

/// Find the list of missing elements of a [testList] from a [srcList]
Set<T> findMissingFromSet<T>(Set<T> srcList, Set<T> testList) {
  Set<T> dest = Set();

  testList.forEach((f) {
    if (!srcList.contains(f)) dest.add(f);
  });

  return dest;
}

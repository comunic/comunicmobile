/// Map utilities
///
/// @author Pierre HUBERT

/// Invert the values of a map with their keys
Map<V, K> invertMap<K, V>(Map<K, V> m) => m.map((k, v) => MapEntry(v, k));

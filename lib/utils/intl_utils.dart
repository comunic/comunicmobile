import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl_standalone.dart';

/// Internationalization utilities
///
/// @author Pierre HUBERT

String? _currLang;
late Map<String, Map<String, String>> translations;

/// Initialize translations system
///
/// This method currently fetch the current user language
Future<void> initTranslations() async {
  _currLang = await findSystemLocale();
  await initializeDateFormatting(_currLang, null);

  // Load the list of translations
  final list = Map<String, String>.from(
      jsonDecode(await rootBundle.loadString("assets/langs.json")));

  translations = new Map();
  for (final entry in list.entries) {
    translations[entry.key] = Map<String, String>.from(jsonDecode(
        await rootBundle.loadString("assets/langs/${entry.value}.json")));
  }
}

/// Translate a string
///
/// Translate a given [string] into the current language, if available
///
/// Then apply the list of [args] to the string, each argument name is
/// surrounded by '%'
String? tr(String? string, {Map<String, String?>? args}) {
  // Check if a translation is available
  if (_currLang != null &&
      translations.containsKey(_currLang) &&
      translations[_currLang!]!.containsKey(string))
    string = translations[_currLang!]![string!];

  //Apply arguments
  if (args != null)
    args.forEach((key, value) => string = string!.replaceAll("%$key%", value!));

  return string;
}

/// Get current lang, in format aa_BB
String get lang => _currLang != null ? _currLang! : "en_US";


/// Get short lang format, in format aa
String get shortLang => lang.split("_")[0];

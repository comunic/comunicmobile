import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:random_string/random_string.dart';

import '../models/api_request.dart';
import '../ui/dialogs/pick_file_dialog.dart';

/// Files utilities
///
/// @author Pierre HUBERT

/// Ask the user to choose an image, either from the gallery or using the camera
///
/// Throws an exception null in case of failure
Future<BytesFile?> pickImage(BuildContext context,
    {CropAspectRatio? aspectRatio}) async {
  return await showPickFileDialog(
    context: context,
    allowedMimeTypes: ["image/png", "image/jpeg", "image/gif"],
    imageMaxHeight: 10000,
    imageMaxWidth: 10000,
    aspectRatio: aspectRatio,
  );
}

/// Generate a new temporary file
///
/// Throws in case of failure
Future<File> generateTemporaryFile() async {
  final tempDir = await getTemporaryDirectory();
  return File(path.join(tempDir.path, randomString(15, from: 65, to: 90)));
}

/// Check if a mime type maps to an image or not
bool isImage(String mimeType) => mimeType.startsWith("image/");

/// Check if a mime type maps to a video or not
bool isVideo(String mimeType) => mimeType.startsWith("video/mp4");

/// Check if a mime type maps to an audio file or not
bool isAudio(String mimeType) => mimeType.startsWith("audio/mpeg");

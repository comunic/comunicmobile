/// API utilities
///
/// @author Pierre Hubert

/// Casting helper
T? cast<T>(dynamic val) => val is T ? val : null;

/// Turn null and "null" into ""
String nullToEmpty(String? input) =>
    input == null || input == "null" ? "" : input;

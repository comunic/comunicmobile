/// Log utilities
///
/// @author Pierre Hubert

void logError(dynamic e, StackTrace s) {
  if (e is Exception) {
    print("Exception: $e\n$s");
  } else {
    print("Error: $e\n$s");
  }
}

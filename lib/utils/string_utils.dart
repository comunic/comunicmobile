/// String utilities
/// 
/// @author Pierre Hubert

String reduceString(String str, int maxLen) =>
  str.length <= maxLen ? str : str.substring(0, maxLen - 3) + "...";

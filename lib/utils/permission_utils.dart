import 'package:comunic/utils/log_utils.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

/// Permission utilities
///
/// @author Pierre Hubert

/// Request a permission, if it has not been granted yet
Future<bool> requestPermission(
    BuildContext context, Permission permission) async {
  try {
    if (await permission.isPermanentlyDenied) {
      return false;
    }

    if (await permission.isGranted) return true;

    if (await permission.request() != PermissionStatus.granted) return false;

    return true;
  } catch (e, s) {
    logError(e, s);
    return false;
  }
}

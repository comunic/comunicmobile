/// Input utilities
///
/// @author Pierre HUBERT

/// Check out whether a password is valid or not
bool legacyValidatePassword(String s) => s.length > 3;


/// Check out whether a given email address is valid or not
///
/// Taken from https://medium.com/@nitishk72/form-validation-in-flutter-d762fbc9212c
bool validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern as String);
  return regex.hasMatch(value);
}

/// Check out whether a given string is a valid URL or not
bool validateUrl(String url) {
  //Initial check
  if (!url.startsWith("http://") && !url.startsWith("https://")) return false;

  try {
    final uri = Uri.parse(url);
    return uri.hasScheme &&
        uri.hasAuthority &&
        uri.port != 0 &&
        uri.path.length != 0;
  } catch (e) {
    return false;
  }
}

/// Validate directory reference
bool validateDirectoryReference(String ref) =>
    RegExp(r'@[a-zA-Z0-9]+').hasMatch(ref);

/// Validated a shortcut
bool validateShortcut(String shortcut) =>
    RegExp(r'^:[a-zA-Z0-9]+:$').hasMatch(shortcut);

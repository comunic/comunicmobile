import 'dart:io';

/// Flutter utilities
///
/// @author Pierre Hubert
import 'package:flutter/foundation.dart' show kIsWeb;

bool get isWeb => kIsWeb;

bool get isAndroid => !isWeb && Platform.isAndroid;

bool get isIOS => !isWeb && Platform.isIOS;
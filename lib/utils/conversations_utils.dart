import 'package:comunic/helpers/conversations_helper.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Conversations utilities
///
/// @author Pierre HUBERT

/// Open a private conversation with a given [userID]
Future<bool> openPrivateConversation(BuildContext context, int? userID) async {
  try {
    final convID = await ConversationsHelper().getPrivate(userID);

    // Open the conversation
    MainController.of(context)!.openConversationById(convID);

    return true;
  } catch (e, s) {
    print("Failed to find private conversation! $e => $s");
    showSimpleSnack(context, tr("Could not find a private conversation!")!);
    return false;
  }
}

import 'dart:ui';

/// Color utilities
///
/// @author Pierre Hubert

String colorToHex(Color? color) {
  if (color == null) return "";

  return (color.red.toRadixString(16).padLeft(2, '0') +
          color.green.toRadixString(16).padLeft(2, '0') +
          color.blue.toRadixString(16).padLeft(2, '0'))
      .toUpperCase();
}

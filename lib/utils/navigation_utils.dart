import 'package:comunic/helpers/virtual_directory_helper.dart';
import 'package:comunic/ui/routes/main_route/main_route.dart';
import 'package:comunic/ui/routes/single_post_route.dart';
import 'package:comunic/utils/intl_utils.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/material.dart';

/// Navigation utilities
///
/// @author Pierre HUBERT

/// Pop a page
void popPage(BuildContext context) {
  MainController.of(context)!.popPage();
}

/// Open the page of a user
void openUserPage({required int userID, required BuildContext context}) {
  MainController.of(context)!.openUserPage(userID);
}

/// Open a post in full screen
void openPostFullScreen(int postID, BuildContext context) {
  MainController.of(context)!.push(SinglePostRoute(postID: postID));
}

/// Open a virtual directory
void openVirtualDirectory(BuildContext context, String directory) async {
  if (directory.startsWith("@")) directory = directory.substring(1);

  try {
    final result = await VirtualDirectoryHelper().find(directory);

    switch (result.type) {
      case VirtualDirectoryType.USER:
        openUserPage(context: context, userID: result.id!);
        break;

      case VirtualDirectoryType.GROUP:
        MainController.of(context)!.openGroup(result.id!);
        break;

      case VirtualDirectoryType.NONE:
        await showDialog(
            context: context,
            builder: (c) => AlertDialog(
                  title: Text(tr("Error")!),
                  content: Text(tr("Could not find related resource!")!),
                  actions: <Widget>[
                    MaterialButton(
                      child: Text(tr("OK")!),
                      onPressed: () => Navigator.of(c).pop(),
                    )
                  ],
                ));
        break;
    }
  } catch (e, stack) {
    print(e);
    print(stack);
    showSimpleSnack(context, tr("Could not search virtual directory!")!);
  }
}

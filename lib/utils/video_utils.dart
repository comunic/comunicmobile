import 'dart:io';

import 'package:comunic/models/api_request.dart';
import 'package:comunic/utils/log_utils.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import 'files_utils.dart';

/// Video utilities
///
/// @author Pierre Hubert

/// Generate a thumbnail for a video. In case of failure, return null
Future<BytesFile?> generateVideoThumbnail({
  required BytesFile videoFile,
  required int maxWidth,
}) async {
  File? file;

  try {
    file = await generateTemporaryFile();

    await file.writeAsBytes(videoFile.bytes!);

    return BytesFile(
      "thumb.png",
      await VideoThumbnail.thumbnailData(
          video: file.absolute.path, maxWidth: maxWidth),
    );
  } catch (e, s) {
    logError(e, s);
    return null;
  } finally {
    if (file != null && await file.exists()) await file.delete();
  }
}

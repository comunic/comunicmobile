import 'package:comunic/utils/intl_utils.dart';
import 'package:intl/intl.dart';

/// Date utilities
///
/// @author Pierre HUBERT

/// Get current timestamp
int time() {
  return (DateTime.now().millisecondsSinceEpoch / 1000).floor();
}

String? diffTimeToStr(int amount) {
  if (amount < 0) amount = 0;

  // Seconds
  if (amount < 60) return tr("%secs%s", args: {"secs": amount.toString()});

  // Minutes
  if (amount < 60 * 60)
    return tr("%mins% min", args: {"mins": (amount / 60).floor().toString()});

  // Hours
  if (amount < 60 * 60 * 24)
    return tr("%hours% h",
        args: {"hours": (amount / (60 * 60)).floor().toString()});

  // Days
  if (amount < 60 * 60 * 24 * 30)
    return tr("%days%d",
        args: {"days": (amount / (60 * 60 * 24)).floor().toString()});

  // Months
  if (amount < 60 * 60 * 24 * 365) {
    final months = (amount / (60 * 60 * 24 * 30)).floor();
    return months == 1
        ? tr("1 month")
        : tr("%months% months", args: {"months": months.toString()});
  }

  // Years
  final years = (amount / (60 * 60 * 24 * 365)).floor();
  return years == 1
      ? tr("1 year")
      : tr("%years% years", args: {"years": years.toString()});
}

String? diffTimeFromNowToStr(int date) {
  return diffTimeToStr(time() - date);
}

/// Return properly formatted date and time
String dateTimeToString(DateTime time) {
  return "${time.day}:${time.month}:${time.year} ${time.hour}:${time.minute}";
}

/// Format a [Duration] in the form "MM:SS"
String formatDuration(Duration d) =>
    "${d.inMinutes < 10 ? "0" + d.inMinutes.toString() : d.inMinutes.toString()}:${d.inSeconds % 60 < 10 ? "0" + (d.inSeconds % 60).toString() : (d.inSeconds % 60).toString()}";

/// Compare two [DateTime] on their date
bool isSameDate(DateTime one, DateTime? other) {
  if (other == null) return false;
  return one.year == other.year &&
      one.month == other.month &&
      one.day == other.day;
}

/// Format a date to make it easily shown
String formatDisplayDate(DateTime dt, {bool date = true, bool time = true}) {
  final format = Intl(Intl.systemLocale).date();
  if (date) {
    format.add_EEEE();
    format.add_yMMMMd();
  }
  if (time) format.add_jm();
  return format.format(dt);
}
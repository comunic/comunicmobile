import 'package:clipboard/clipboard.dart';
import 'package:comunic/utils/ui_utils.dart';
import 'package:flutter/cupertino.dart';

import 'intl_utils.dart';

/// Clipboard utilities
///
/// @author Pierre Hubert

void copyToClipboard(BuildContext context, String content) {
  FlutterClipboard.copy(content);
  snack(context, tr("'%1%' copied to clipboard!", args: {"1": content})!);
}

#!/bin/sh
DEST="/home/pierre/Desktop"

make stable_release_split_per_abi && \
	mv build/app/outputs/flutter-apk/app-armeabi-v7a-stable-release.apk "$DEST" && \
	mv build/app/outputs/flutter-apk/app-arm64-v8a-stable-release.apk "$DEST" && \
	mv build/app/outputs/flutter-apk/app-x86_64-stable-release.apk "$DEST" && \
	mv build/app/outputs/mapping/stableRelease/mapping.txt "$DEST" && \
	make stable_release && \
	mv build/app/outputs/flutter-apk/app-stable-release.apk "$DEST"

echo "Done. Artifacts copied to $DEST"

